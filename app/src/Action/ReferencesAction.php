<?php
namespace App\Action;

use Slim\Views\Twig;
use Psr\Log\LoggerInterface;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

final class ReferencesAction
{
    private $view;
    private $logger;
    private $config;

    public function __construct(Twig $view, LoggerInterface $logger, $client_session)
    {
    	$this->view = $view;
        $this->logger = $logger;
        $this->config = $client_session->getConfig();
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     *
     * @SWG\Get(
     *   path="/references/address-types",
     *   summary="Papi's Address Type References",
     *   operationId="getAddressTypeReference",
     *   produces={"application/json"},
     *	 tags={"References"},
     *   @SWG\Response(
     *   	response=200,
     *   	description="Array of Papi's Address Types."
     *   )
     * )
     */
    
    
    public function address_types(Request $request, Response $response, $args)
    {
    	$this->logger->debug("References Address Types Action");
    
    	$response = $response->withHeader('Content-Type', 'application/json');
    
    	$address_types = \App\Models\AddressTypes::where('active', '=', true)->get();
    	
    	if( $address_types )
    		echo $address_types->toJson();
    	else
    		echo '[]';
    	
    	return $response;
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     *
     * @SWG\Get(
     *   path="/references/countries",
     *   summary="World Countries References",
     *   operationId="getCountriesReference",
     *   produces={"application/json"},
     *	 tags={"References"},
     *   @SWG\Response(
     *   	response=200,
     *   	description="Array of World Countries."
     *   )
     * )
     */
    
    
    public function countries(Request $request, Response $response, $args)
    {
    	$this->logger->debug("References Countries Action");
    
    	$response = $response->withHeader('Content-Type', 'application/json');
    
    	echo file_get_contents($this->config['protocol'] . $this->config['domain'] . '/references/countries.json');
    
    	return $response;
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     *
     * @SWG\Get(
     *   path="/references/months",
     *   summary="Months of Year References",
     *   operationId="getMonthsReference",
     *   produces={"application/json"},
     *	 tags={"References"},
     *   @SWG\Response(
     *   	response=200,
     *   	description="Array of Months of Year."
     *   )
     * )
     */
    
    
    public function months(Request $request, Response $response, $args)
    {
    	$this->logger->debug("References Months Action");
    
    	$response = $response->withHeader('Content-Type', 'application/json');
    
    	echo file_get_contents($this->config['protocol'] . $this->config['domain'] . '/references/months.json');
    
    	return $response;
    }
    
    /**
     * @return \Illuminate\Http\JsonResponse
     *
     * @SWG\Get(
     *   path="/references/payment-methods",
     *   summary="Papi's Payment Methods References",
     *   operationId="getPaymentMethodsReference",
     *   produces={"application/json"},
     *	 tags={"References"},
     *   @SWG\Response(
     *   	response=200,
     *   	description="Array of Papi's Payment Methods."
     *   )
     * )
     */
    
    
    public function payment_methods(Request $request, Response $response, $args)
    {
    	$this->logger->debug("References Payment Methods Action");
    
    	$response = $response->withHeader('Content-Type', 'application/json');
    
    	$payment_methods = \App\Models\PaymentMethods::where('active', '=', true)->get();
    	
    	if( $payment_methods )
    		echo $payment_methods->toJson();
    	else
    		echo '[]';;
    	
    	return $response;
    }
    
    /**
     * @return \Illuminate\Http\JsonResponse
     *
     * @SWG\Get(
     *   path="/references/shipping-methods",
     *   summary="Papi's Shipping Methods References",
     *   operationId="getShippingMethodsReference",
     *   produces={"application/json"},
     *	 tags={"References"},
     *   @SWG\Response(
     *   	response=200,
     *   	description="Array of Papi's Shipping Methods."
     *   )
     * )
     */
    
    
    public function shipping_methods(Request $request, Response $response, $args)
    {
    	$this->logger->debug("References Shipping Methods Action");
    
    	$response = $response->withHeader('Content-Type', 'application/json');
    
    	$shipping_methods = \App\Models\ShippingMethods::where('active', '=', true)->get();
    	
    	if( $shipping_methods )
    		echo $shipping_methods->toJson();
    	else
    		echo '[]';

    	return $response;
    }
    
    /**
     * @return \Illuminate\Http\JsonResponse
     *
     * @SWG\Get(
     *   path="/references/states",
     *   summary="US States References",
     *   operationId="getStatesReference",
     *   produces={"application/json"},
     *	 tags={"References"},
     *   @SWG\Response(
     *   	response=200,
     *   	description="Array of US States."
     *   )
     * )
     */
    

    public function states(Request $request, Response $response, $args)
    {
        $this->logger->debug("References States Action");
        
        $response = $response->withHeader('Content-Type', 'application/json');
        
        echo file_get_contents($this->config['protocol'] . $this->config['domain'] . '/references/states.json');

        return $response;
    }
       
}
