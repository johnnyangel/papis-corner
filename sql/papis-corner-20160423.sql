-- MySQL dump 10.13  Distrib 5.7.9, for linux-glibc2.5 (x86_64)
--
-- Host: localhost    Database: papis-corner
-- ------------------------------------------------------
-- Server version	5.6.27

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `account`
--

DROP TABLE IF EXISTS `account`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `account` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`,`user_id`),
  KEY `fk_account_user1_idx` (`user_id`),
  CONSTRAINT `fk_account_user1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `account`
--

LOCK TABLES `account` WRITE;
/*!40000 ALTER TABLE `account` DISABLE KEYS */;
INSERT INTO `account` VALUES (1,1,'2016-04-07 17:30:00','2016-04-07 17:30:00',NULL),(2,2,'2016-04-07 17:30:00','2016-04-07 17:30:00',NULL);
/*!40000 ALTER TABLE `account` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `address`
--

DROP TABLE IF EXISTS `address`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `address` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_id` int(11) NOT NULL,
  `type` enum('Billing','Shipping') DEFAULT NULL,
  `recipient` varchar(128) DEFAULT NULL,
  `address_1` varchar(128) DEFAULT NULL,
  `address_2` varchar(128) DEFAULT NULL,
  `city` varchar(128) DEFAULT NULL,
  `state` varchar(2) DEFAULT NULL,
  `postal_code` varchar(128) DEFAULT NULL,
  `territory` varchar(128) DEFAULT NULL,
  `country` varchar(128) DEFAULT NULL,
  `lat` decimal(10,8) DEFAULT NULL,
  `long` decimal(11,8) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`,`account_id`),
  KEY `fk_address_account1_idx` (`account_id`),
  CONSTRAINT `fk_address_account1` FOREIGN KEY (`account_id`) REFERENCES `account` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `address`
--

LOCK TABLES `address` WRITE;
/*!40000 ALTER TABLE `address` DISABLE KEYS */;
/*!40000 ALTER TABLE `address` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `address_types`
--

DROP TABLE IF EXISTS `address_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `address_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `active` tinyint(1) DEFAULT '1',
  `key` varchar(45) DEFAULT NULL,
  `value` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `address_types`
--

LOCK TABLES `address_types` WRITE;
/*!40000 ALTER TABLE `address_types` DISABLE KEYS */;
INSERT INTO `address_types` VALUES (1,1,'Billing','My Address'),(2,1,'Shipping','Ship To Address');
/*!40000 ALTER TABLE `address_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `catalog`
--

DROP TABLE IF EXISTS `catalog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `catalog` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(150) DEFAULT NULL,
  `discription` varchar(500) DEFAULT NULL,
  `path` varchar(250) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `catalog`
--

LOCK TABLES `catalog` WRITE;
/*!40000 ALTER TABLE `catalog` DISABLE KEYS */;
INSERT INTO `catalog` VALUES (1,'The Naomi Cordero Collection','Home furnishing art work for all. Canvas, pencil portriats that will brighten and add texture to your personal space.','/media/catalog/naomi-cordero','2016-01-17 14:44:56','2016-01-17 14:44:56',NULL);
/*!40000 ALTER TABLE `catalog` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `notification`
--

DROP TABLE IF EXISTS `notification`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `notification` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `type` enum('SYSTEM','ACCOUNT') DEFAULT NULL,
  `html` text,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_notification_user1_idx` (`user_id`),
  CONSTRAINT `fk_notification_user1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `notification`
--

LOCK TABLES `notification` WRITE;
/*!40000 ALTER TABLE `notification` DISABLE KEYS */;
/*!40000 ALTER TABLE `notification` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `payment_method`
--

DROP TABLE IF EXISTS `payment_method`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `payment_method` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_id` int(11) NOT NULL,
  `type` enum('PAYPAL','CREDIT CARD') DEFAULT NULL,
  `card_number` varchar(100) DEFAULT NULL,
  `expiration_date` varchar(4) DEFAULT NULL,
  `csv` varchar(3) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`,`account_id`),
  KEY `fk_payment_method_account1_idx` (`account_id`),
  CONSTRAINT `fk_payment_method_account1` FOREIGN KEY (`account_id`) REFERENCES `account` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `payment_method`
--

LOCK TABLES `payment_method` WRITE;
/*!40000 ALTER TABLE `payment_method` DISABLE KEYS */;
/*!40000 ALTER TABLE `payment_method` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `payment_methods`
--

DROP TABLE IF EXISTS `payment_methods`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `payment_methods` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `active` tinyint(1) DEFAULT '1',
  `key` varchar(45) DEFAULT NULL,
  `value` varchar(45) DEFAULT NULL,
  `logo` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `payment_methods`
--

LOCK TABLES `payment_methods` WRITE;
/*!40000 ALTER TABLE `payment_methods` DISABLE KEYS */;
INSERT INTO `payment_methods` VALUES (1,1,'PAYPAL','Paypal','/images/paypal.png'),(2,1,'VISA','Visa','/images/visa.png'),(3,1,'MASTERCARD','Master Card','/images/master-card.png'),(4,1,'DISCOVER','Discover','/images/discover.png'),(5,1,'AMEX','American Express','/images/amex.png');
/*!40000 ALTER TABLE `payment_methods` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permissions`
--

DROP TABLE IF EXISTS `permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `permission` enum('MASTER','SLAVE') DEFAULT 'SLAVE',
  PRIMARY KEY (`id`,`user_id`),
  KEY `fk_permissions_user1_idx` (`user_id`),
  CONSTRAINT `fk_permissions_user1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permissions`
--

LOCK TABLES `permissions` WRITE;
/*!40000 ALTER TABLE `permissions` DISABLE KEYS */;
INSERT INTO `permissions` VALUES (1,1,'MASTER'),(2,2,'SLAVE'),(3,1,'SLAVE');
/*!40000 ALTER TABLE `permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product`
--

DROP TABLE IF EXISTS `product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `guid` varbinary(18) NOT NULL,
  `key` varchar(150) NOT NULL,
  `catalog_id` int(11) NOT NULL,
  `status` enum('PENDING','AVAILABLE','ON_HOLD','SOLD_OUT') DEFAULT 'PENDING',
  `name` varchar(150) DEFAULT NULL,
  `caption` varchar(200) DEFAULT NULL,
  `description` varchar(500) DEFAULT NULL,
  `path` varchar(250) DEFAULT NULL,
  `price` decimal(10,2) DEFAULT '50.00',
  `provided_by` varchar(45) DEFAULT NULL,
  `sort_order` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`,`guid`,`catalog_id`,`key`),
  KEY `fk_product_catalog1_idx` (`catalog_id`),
  CONSTRAINT `fk_product_catalog1` FOREIGN KEY (`catalog_id`) REFERENCES `catalog` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product`
--

LOCK TABLES `product` WRITE;
/*!40000 ALTER TABLE `product` DISABLE KEYS */;
INSERT INTO `product` VALUES (1,'��0���','Pottery-and-Fruit',1,'AVAILABLE','Pottery and Fruit','Canvas oil painting of clay pitcher and bowl of fruit.',NULL,'/media/catalog/naomi-cordero/products/art-work/images',50.00,'Naomi Cordero',NULL,'2016-01-18 02:47:41','2016-04-23 17:13:10',NULL),(2,'�u�\0-���','Lighthouse-on-the-Coastal-Waterway',1,'AVAILABLE','Lighthouse on the Coastal Waterway','Canvas oil painting of lighthouse on a sunny day.',NULL,'/media/catalog/naomi-cordero/products/art-work/images',50.00,'Naomi Cordero',NULL,'2016-01-18 04:52:21','2016-04-21 23:53:39',NULL),(3,'����z�T&','Birds-on-a-Branch',1,'AVAILABLE','Birds on a Branch','Pencil drawing of two tropical birds on a branch.',NULL,'/media/catalog/naomi-cordero/products/art-work/images',50.00,'Naomi Cordero',NULL,'2016-01-18 05:10:23','2016-04-23 05:51:28',NULL),(4,'��5a5h�1Z�','Bird-on-a-Limb',1,'AVAILABLE','Bird on a Limb','Pencil drawing of a wild turkey perched on a branch.',NULL,'/media/catalog/naomi-cordero/products/art-work/images',50.00,'Naomi Cordero',NULL,'2016-01-18 09:51:18','2016-04-23 05:53:24',NULL),(5,'XP+�C;S�+R?','Bird-in-a-Bush',1,'AVAILABLE','Bird in a Bush','Pencil drawing of black and yellow bird in a flowering bush.',NULL,'/media/catalog/naomi-cordero/products/art-work/images',50.00,'Naomi Cordero',NULL,'2016-01-18 10:01:17','2016-04-23 17:06:01',NULL),(6,'S��|���','Birds-in-Berry-Branches',1,'AVAILABLE','Birds in Berry Branches','Pencil drawing of a flock of birds on a berry branches.',NULL,'/media/catalog/naomi-cordero/products/art-work/images',50.00,'Naomi Cordero',NULL,'2016-01-18 10:08:15','2016-04-23 17:09:17',NULL),(8,'L��(�4kK�','King-Of-Man',1,'AVAILABLE','King Of Man','Pencil drawing of My Jesus of Nazareth.',NULL,'/media/catalog/naomi-cordero/products/art-work/images',50.00,'Naomi Cordero',NULL,'2016-01-30 23:02:59','2016-04-05 02:52:13',NULL),(9,'b���\nWD�V','',1,'PENDING',NULL,NULL,NULL,NULL,50.00,NULL,NULL,'2016-01-30 23:40:23','2016-01-30 23:40:23',NULL),(10,'4/L+�M�9\n^�','',1,'PENDING',NULL,NULL,NULL,NULL,50.00,NULL,NULL,'2016-01-30 23:49:11','2016-01-30 23:49:11',NULL),(11,'��Dyg�;�','',1,'PENDING',NULL,NULL,NULL,NULL,50.00,NULL,NULL,'2016-01-31 00:00:34','2016-01-31 00:00:34',NULL),(12,'�f�4���','',1,'PENDING',NULL,NULL,NULL,NULL,50.00,NULL,NULL,'2016-01-31 00:01:02','2016-01-31 00:01:02',NULL),(13,'�\'8��R�^','',1,'PENDING',NULL,NULL,NULL,NULL,50.00,NULL,NULL,'2016-01-31 00:01:18','2016-01-31 00:01:18',NULL),(14,'S�g��[�}.','',1,'PENDING',NULL,NULL,NULL,NULL,50.00,NULL,NULL,'2016-01-31 00:20:19','2016-01-31 00:20:19',NULL),(15,'���I{s}','',1,'PENDING',NULL,NULL,NULL,NULL,50.00,NULL,NULL,'2016-01-31 00:20:39','2016-01-31 00:20:39',NULL),(16,' ���F5�\�','',1,'PENDING',NULL,NULL,NULL,NULL,50.00,NULL,NULL,'2016-01-31 22:13:34','2016-01-31 22:13:34',NULL),(17,'Q�\'����e','',1,'PENDING',NULL,NULL,NULL,NULL,50.00,NULL,NULL,'2016-02-01 01:23:17','2016-02-01 01:23:17',NULL);
/*!40000 ALTER TABLE `product` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product_has_search_tags`
--

DROP TABLE IF EXISTS `product_has_search_tags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_has_search_tags` (
  `product_id` int(11) NOT NULL,
  `product_guid` varbinary(18) NOT NULL,
  `product_catalog_id` int(11) NOT NULL,
  `search_tags_id` int(11) NOT NULL,
  PRIMARY KEY (`product_id`,`product_guid`,`product_catalog_id`,`search_tags_id`),
  KEY `fk_product_has_search_tags_search_tags1_idx` (`search_tags_id`),
  KEY `fk_product_has_search_tags_product1_idx` (`product_id`,`product_guid`,`product_catalog_id`),
  CONSTRAINT `fk_product_has_search_tags_product1` FOREIGN KEY (`product_id`, `product_guid`, `product_catalog_id`) REFERENCES `product` (`id`, `guid`, `catalog_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_product_has_search_tags_search_tags1` FOREIGN KEY (`search_tags_id`) REFERENCES `search_tags` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_has_search_tags`
--

LOCK TABLES `product_has_search_tags` WRITE;
/*!40000 ALTER TABLE `product_has_search_tags` DISABLE KEYS */;
/*!40000 ALTER TABLE `product_has_search_tags` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product_image`
--

DROP TABLE IF EXISTS `product_image`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_image` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `product_guid` varbinary(18) NOT NULL,
  `type` enum('LARGE','MEDIUM','SMALL','TINY') DEFAULT NULL,
  `filename` varchar(250) DEFAULT NULL,
  `backdrop_style` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`,`product_id`,`product_guid`),
  KEY `fk_product_image_product1_idx` (`product_id`,`product_guid`),
  CONSTRAINT `fk_product_image_product1` FOREIGN KEY (`product_id`, `product_guid`) REFERENCES `product` (`id`, `guid`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=65 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_image`
--

LOCK TABLES `product_image` WRITE;
/*!40000 ALTER TABLE `product_image` DISABLE KEYS */;
INSERT INTO `product_image` VALUES (1,1,'��0���','LARGE','pottery-and-fruit-naomi-cordero-1024x778.png','color-pastel-red'),(2,1,'��0���','MEDIUM','pottery-and-fruit-naomi-cordero-870x400.png','color-pastel-red'),(3,1,'��0���','SMALL','pottery-and-fruit-naomi-cordero-300x400.png','color-pastel-red'),(4,2,'�u�\0-���','LARGE','lighthouse-redroof-house-naomi-cordero-1024x795.png','color-pastel-dark-red'),(5,2,'�u�\0-���','MEDIUM','lighthouse-redroof-house-naomi-cordero-870x400.png','color-pastel-dark-red'),(6,2,'�u�\0-���','SMALL','lighthouse-redroof-house-naomi-cordero-300x400.png','color-pastel-dark-red'),(7,3,'����z�T&','LARGE','birds-on-branch-naomi-cordero-1024x1324.png','color-pastel-brown'),(8,3,'����z�T&','MEDIUM','birds-on-branch-naomi-cordero-870x400.png','color-pastel-brown'),(9,3,'����z�T&','SMALL','birds-on-branch-naomi-cordero-300x400.png','color-pastel-brown'),(10,4,'��5a5h�1Z�','LARGE','bird-on-a-limb-naomi-cordero-1024x1326.png','color-pastel-orange'),(11,4,'��5a5h�1Z�','MEDIUM','bird-on-a-limb-naomi-cordero-870x400.png','color-pastel-orange'),(12,4,'��5a5h�1Z�','SMALL','bird-on-a-limb-naomi-cordero-300x400.png','color-pastel-orange'),(13,5,'XP+�C;S�+R?','LARGE','bird-in-a-bush-naomi-cordero-1024x1326.png','color-pastel-blue'),(14,5,'XP+�C;S�+R?','MEDIUM','bird-in-a-bush-naomi-cordero-870x400.png','color-pastel-blue'),(15,5,'XP+�C;S�+R?','SMALL','bird-in-a-bush-naomi-cordero-300x400.png','color-pastel-blue'),(16,6,'S��|���','LARGE','birds-in-berry-branches-naomi-cordero-1024x1366.png','color-pastel-yellow'),(17,6,'S��|���','MEDIUM','birds-in-berry-branches-naomi-cordero-870x400.png','color-pastel-yellow'),(18,6,'S��|���','SMALL','birds-in-berry-branches-naomi-cordero-300x400.png','color-pastel-yellow'),(19,1,'��0���','TINY','pottery-and-fruit-naomi-cordero-100x100.png','color-pastel-red'),(20,2,'�u�\0-���','TINY','lighthouse-redroof-house-naomi-cordero-100x100.png','color-pastel-dark-red'),(21,3,'����z�T&','TINY','birds-on-branch-naomi-cordero-100x100.png','color-pastel-brown'),(22,4,'��5a5h�1Z�','TINY','bird-on-a-limb-naomi-cordero-100x100.png','color-pastel-orange'),(23,5,'XP+�C;S�+R?','TINY','bird-in-a-bush-naomi-cordero-100x100.png','color-pastel-blue'),(24,6,'S��|���','TINY','birds-in-berry-branches-naomi-cordero-100x100.png','color-pastel-yellow'),(25,8,'L��(�4kK�','LARGE','king-of-man-naomi-cordero-1024x1345.png','color-pastel-dark-purple'),(26,8,'L��(�4kK�','MEDIUM','king-of-man-naomi-cordero-870x400.png','color-pastel-dark-purple'),(27,8,'L��(�4kK�','SMALL','king-of-man-naomi-cordero-300x400.png','color-pastel-dark-purple'),(28,8,'L��(�4kK�','TINY','king-of-man-naomi-cordero-100x100.png','color-pastel-dark-purple'),(29,9,'b���\nWD�V','LARGE',NULL,NULL),(30,9,'b���\nWD�V','MEDIUM',NULL,NULL),(31,9,'b���\nWD�V','SMALL',NULL,NULL),(32,9,'b���\nWD�V','TINY',NULL,NULL),(33,10,'4/L+�M�9\n^�','LARGE',NULL,NULL),(34,10,'4/L+�M�9\n^�','MEDIUM',NULL,NULL),(35,10,'4/L+�M�9\n^�','SMALL',NULL,NULL),(36,10,'4/L+�M�9\n^�','TINY',NULL,NULL),(37,11,'��Dyg�;�','LARGE',NULL,NULL),(38,11,'��Dyg�;�','MEDIUM',NULL,NULL),(39,11,'��Dyg�;�','SMALL',NULL,NULL),(40,11,'��Dyg�;�','TINY',NULL,NULL),(41,12,'�f�4���','LARGE',NULL,NULL),(42,12,'�f�4���','MEDIUM',NULL,NULL),(43,12,'�f�4���','SMALL',NULL,NULL),(44,12,'�f�4���','TINY',NULL,NULL),(45,13,'�\'8��R�^','LARGE',NULL,NULL),(46,13,'�\'8��R�^','MEDIUM',NULL,NULL),(47,13,'�\'8��R�^','SMALL',NULL,NULL),(48,13,'�\'8��R�^','TINY',NULL,NULL),(49,14,'S�g��[�}.','LARGE',NULL,NULL),(50,14,'S�g��[�}.','MEDIUM',NULL,NULL),(51,14,'S�g��[�}.','SMALL',NULL,NULL),(52,14,'S�g��[�}.','TINY',NULL,NULL),(53,15,'���I{s}','LARGE',NULL,NULL),(54,15,'���I{s}','MEDIUM',NULL,NULL),(55,15,'���I{s}','SMALL',NULL,NULL),(56,15,'���I{s}','TINY',NULL,NULL),(57,16,' ���F5�\�','LARGE',NULL,NULL),(58,16,' ���F5�\�','MEDIUM',NULL,NULL),(59,16,' ���F5�\�','SMALL',NULL,NULL),(60,16,' ���F5�\�','TINY',NULL,NULL),(61,17,'Q�\'����e','LARGE',NULL,NULL),(62,17,'Q�\'����e','MEDIUM',NULL,NULL),(63,17,'Q�\'����e','SMALL',NULL,NULL),(64,17,'Q�\'����e','TINY',NULL,NULL);
/*!40000 ALTER TABLE `product_image` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `profile_picture`
--

DROP TABLE IF EXISTS `profile_picture`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `profile_picture` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  `type` varchar(45) DEFAULT NULL,
  `size` int(11) DEFAULT NULL,
  `active` tinyint(1) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`,`user_id`),
  KEY `fk_profile_picture_user1_idx` (`user_id`),
  CONSTRAINT `fk_profile_picture_user1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `profile_picture`
--

LOCK TABLES `profile_picture` WRITE;
/*!40000 ALTER TABLE `profile_picture` DISABLE KEYS */;
/*!40000 ALTER TABLE `profile_picture` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `search_tags`
--

DROP TABLE IF EXISTS `search_tags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `search_tags` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `search_tags`
--

LOCK TABLES `search_tags` WRITE;
/*!40000 ALTER TABLE `search_tags` DISABLE KEYS */;
/*!40000 ALTER TABLE `search_tags` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shipping_methods`
--

DROP TABLE IF EXISTS `shipping_methods`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shipping_methods` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `active` tinyint(1) DEFAULT '1',
  `key` varchar(45) DEFAULT NULL,
  `value` varchar(45) DEFAULT NULL,
  `logo` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shipping_methods`
--

LOCK TABLES `shipping_methods` WRITE;
/*!40000 ALTER TABLE `shipping_methods` DISABLE KEYS */;
INSERT INTO `shipping_methods` VALUES (1,1,'US-MAIL','US Mail','/images/us-postal-service.png'),(2,1,'FED-EX','FedEx','/images/fedex.png'),(3,1,'UPS','UPS','/images/ups.png');
/*!40000 ALTER TABLE `shipping_methods` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `social_media`
--

DROP TABLE IF EXISTS `social_media`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `social_media` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `service` enum('Facebook','Google','Twitter','Linkedin','Paypal') NOT NULL,
  `service_id` varchar(45) DEFAULT NULL,
  `service_email` varchar(45) DEFAULT NULL,
  `service_username` varchar(45) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`,`user_id`),
  KEY `fk_social_media_user1_idx` (`user_id`),
  CONSTRAINT `fk_social_media_user1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `social_media`
--

LOCK TABLES `social_media` WRITE;
/*!40000 ALTER TABLE `social_media` DISABLE KEYS */;
INSERT INTO `social_media` VALUES (1,1,'Facebook','941113595977521','johnnyangelnj@gmail.com','John Cordero','2016-02-20 21:01:52','2016-02-20 21:01:52',NULL),(3,1,'Google','115641041068719438369','johnnyangelnj@gmail.com','John Cordero','2016-02-22 21:59:00','2016-02-22 21:59:00',NULL);
/*!40000 ALTER TABLE `social_media` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(45) NOT NULL,
  `username` varchar(45) DEFAULT NULL,
  `password` varchar(45) DEFAULT NULL,
  `first_name` varchar(45) DEFAULT NULL,
  `last_name` varchar(45) DEFAULT NULL,
  `profile_picture_url` varchar(250) DEFAULT NULL,
  `gender` enum('Male','Female') DEFAULT NULL,
  `status` enum('PENDING','APPROVED','DENIED') DEFAULT 'PENDING',
  `remember_me` tinyint(1) DEFAULT '0',
  `accepted_terms` tinyint(1) DEFAULT '0',
  `email_validation` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`,`email`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  UNIQUE KEY `email_UNIQUE` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'johnnyangelnj@gmail.com',NULL,'30e0be74d8db22eaa8e72e8f11b66a30','John','Cordero','https://scontent.xx.fbcdn.net/hprofile-xfa1/v/t1.0-1/c182.0.621.621/s50x50/550747_319500128138874_1261885944_n.jpg?oh=853a070eb4198cc855ea5c79077c31db&oe=575F9C2C',NULL,'APPROVED',1,1,NULL,'2016-01-01 05:00:00','2016-03-25 16:12:05',NULL),(2,'naocor59@msn.com',NULL,'af23d700f1aecbedf42580cfb9857443','Naomi','Cordero','',NULL,'APPROVED',1,1,NULL,'2016-01-31 05:00:00','2016-03-27 21:45:22',NULL);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-04-23  9:25:59
