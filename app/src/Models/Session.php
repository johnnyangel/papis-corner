<?php
/**
 *  Session Model
 * 
 * Provides data from "'session'"
 *
 * Author: Johnny Angel <johnnyangelnj@gmail.com>
 * Generated on Sunday 17th of January 2016 08:26:54 PM
 *
 */
namespace App\Models;

use App\Models\Base\SessionBase;
 
class Session extends SessionBase
{
    // used to hide columns
    protected $hidden = array();
	
    // used to append columns
    protected $appends = array();
    
    public function user()
    {
    	return $this->hasOne('\App\Models\User', 'id', 'user_id');
    }
    
    public function shopping_cart()
    {
    	return $this->hasOne('\App\Models\ShoppingCart', 'session_id', 'id');
    }
}
