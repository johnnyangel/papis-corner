<?php
namespace App\Utils;

use Psr\Log\LoggerInterface;
use Psr\Http\Message\ServerRequestInterface as Request;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\Exception\UnsatisfiedDependencyException;

class Keys {
	
	private $logger;
	private $config;
	
	public function __construct(LoggerInterface $logger, $config)
	{
		$this->logger = $logger;
		$this->config = $config;
	}
	
	public function encrypt($string) {
		$result = null;
		$key = hash($this->config["encryption.options"]["encryption_hash_algorithm"], $this->config["encryption.options"]["encryption_key"]);
		$iv = substr(hash($this->config["encryption.options"]["encryption_hash_algorithm"], $this->config["encryption.options"]["encryption_initialization_vector"]), 0, 16);
		$result = base64_encode(openssl_encrypt($string, $this->config["encryption.options"]["encryption_method"], $key, 0, $iv));
		return $result;
	}


	public function decrypt($string) {
		$result = null;
		$key = hash($this->config["encryption.options"]["encryption_hash_algorithm"], $this->config["encryption.options"]["encryption_key"]);
		$iv = substr(hash($this->config["encryption.options"]["encryption_hash_algorithm"], $this->config["encryption.options"]["encryption_initialization_vector"]), 0, 16);
		$result = openssl_decrypt(base64_decode($string), $this->config["encryption.options"]["encryption_method"], $key, 0, $iv);
		return $result;
	}	
	
	public function generatePassWord($length = 8)
	{
		$password = null;
		$chars =  'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz' . '0123456789!@#$%&*_+,.?:';
  		$max = strlen($chars) - 1;
	
		for ($i=0; $i < $length; $i++)
			$password .= $chars[rand(0, $max)];
	
		return $password;
	}
	
	public function generateGUID()
	{
		$guid = null;
		
		try {
			$guid = Uuid::uuid4();
		} catch (UnsatisfiedDependencyException $e) {
			throw $e;
		}
		
		return $guid;
	}
	
	public function packGUID($guid)
	{
		return pack("h*", str_replace('-', '', $guid));
	}
	
	public function unpackGUID($guid)
	{
		$unpacked = unpack("h*", $guid);
		return implode('',preg_replace("/([0-9a-f]{8})([0-9a-f]{4})([0-9a-f]{4})([0-9a-f]{4})([0-9a-f]{12})/", "$1-$2-$3-$4-$5", $unpacked));
	}
}