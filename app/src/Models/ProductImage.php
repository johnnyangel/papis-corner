<?php
/**
 *  ProductImage Model
 * 
 * Provides data from "'product_image'"
 *
 * Author: Johnny Angel <johnnyangelnj@gmail.com>
 * Generated on Sunday 17th of January 2016 08:26:54 PM
 *
 */
namespace App\Models;

use App\Models\Base\ProductImageBase;
 
class ProductImage extends ProductImageBase
{
    // used to hide columns
    protected $hidden = array();
	
    // used to append columns
    protected $appends = array();
    
    public function product()
    {
    	return $this->belongsTo('\App\Models\Product');
    }

}
