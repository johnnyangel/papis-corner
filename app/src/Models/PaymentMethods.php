<?php
/**
 *  PaymentMethods Model
 * 
 * Provides data from "'payment_methods'"
 *
 * Author: Johnny Angel <johnnyangelnj@gmail.com>
 * Generated on Friday 11th of March 2016 01:05:47 PM
 *
 */
namespace App\Models;

use App\Models\Base\PaymentMethodsBase;
 
class PaymentMethods extends PaymentMethodsBase
{
    // used to hide columns
    protected $hidden = array();
	
    // used to append columns
    protected $appends = array();

}
