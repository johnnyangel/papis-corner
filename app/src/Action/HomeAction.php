<?php
namespace App\Action;

use Slim\Views\Twig;
use Psr\Log\LoggerInterface;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

final class HomeAction
{
    private $view;
    private $logger;
    private $client_session;

    public function __construct(Twig $view, LoggerInterface $logger, $client_session)
    {
    	$this->view = $view;
        $this->logger = $logger;
        $this->client_session = $client_session;
    }

    public function dispatch(Request $request, Response $response, $args)
    {
        $this->logger->debug("Home Dispatched Action");
        
        $this->logger->debug("Args [". json_encode($args) ."]");
        
        $user_session = $this->client_session->get();
                        
        $meta_data = array(
        		"title" => "Home",
        		"user" =>  $this->client_session->getUserProfile(),
        		"show_title_bar_home" => false,
        		"attributes" => $request->getAttributes(),
        		"is_authenticated" => isset($user_session->is_authenticated)?$user_session->is_authenticated:false,
        		"shopping_cart_item_count" => $this->client_session->getUserShoppingCartItemCount(),
        		"shopping_cart" => $this->client_session->getUserShoppingCart()
        );
        
        $this->logger->debug(json_encode($meta_data));
                

        $this->view->render($response, "home.twig", $meta_data);
        return $response;
    }
    
}
