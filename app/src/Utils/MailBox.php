<?php

namespace App\Utils;

use Psr\Log\LoggerInterface;
use Psr\Http\Message\ServerRequestInterface as Request;

use Zend\Mail;
use Zend\Mail\Message;
use Zend\Mime;
use Zend\Mime\Message as MimeMessage;
use Zend\Mime\Part as MimePart;

class MailBox {

	private $logger;
	private $config;
	private $twig;
	private $transport;
	private $email;
	
	private $service_provider_sms = [
		'T-Mobile'		=> 'phone-number@tmomail.net',
		'Virgin Mobile' => 'phone-number@vmobl.com',
		'Cingular' 		=> 'phone-number@cingularme.com',
		'Sprint' 		=> 'phone-number@messaging.sprintpcs.com',
		'Verizon' 		=> 'phone-number@vtext.com',
		'Nextel' 		=> 'phone-number@messaging.nextel.com'
	];
	
	public function __construct(LoggerInterface $logger, $config, $twig, $transport)
	{
		$this->logger = $logger;
		$this->config = $config;
		$this->twig = $twig;
		$this->transport = $transport;
		$this->email = array();
	}
	
	public function send($template, $letter = array())
	{
		$status = 'Failed';
		$response = 'Mail Transport is switched Off!';
		$template = $this->twig->loadTemplate($template); 
		
		$subject  = $template->renderBlock('subject',   $letter);
		$bodyHtml = $template->renderBlock('body_html', $letter);
		$bodyText = $template->renderBlock('body_text', $letter);

		//Email Format array('YourName@email.com' => 'Your Name');
		$from = array($letter['from'] => $letter['from_username']);
		$to = array($letter['to']  => $letter['to_username']);
		
		$this->logger->debug("MailBox HtmlBody " . $bodyHtml);
		$this->logger->debug("MailBox TextBody " . $bodyText);
		
		
		if($this->config['smtp.switch']){
			try {
				$failures;
				$message = new \Swift_Message($subject);
				$message->setFrom($from);
				$message->setBody($bodyHtml, 'text/html');
				$message->setTo($to);
				$message->addPart($bodyText, 'text/plain');
				//$message->embed(\Swift_Image::fromPath('http://www.papiscorner.com/images/logos/papicorners-email-logo.png'));
				//$message->attach(\Swift_Attachment::fromPath('http://www.papiscorner.com/images/logos/papicorners-email-logo.png')->setDisposition('inline'));
				
				if ($recipients = $this->transport->send($message, $failures)) {
					$this->logger->debug("MailBox Message Successfully Sent!");
					$status = 'Successful';
					$response = 'Email Sent!';
				} else {
					$this->logger->error("MailBox Error: " . $e->getMessage());
					$this->logger->error("Failures: " .json_encode($failures));
					$response = $e->getMessage();
				}
				
			}catch(Exception $e){
				$this->logger->error("MailBox Error: " . $e->getMessage());
				$response = $e->getMessage();
			}
				
			
		} else {
			$this->logger->info("MailBox Send Mail Using Transport? No");
		}
		
		return array('status' => $status, 'response' => $response);		
	}
}