<?php
/**
 *  Order Model
 * 
 * Provides data from "'order'"
 *
 * Author: Johnny Angel <johnnyangelnj@gmail.com>
 * Generated on Sunday 17th of January 2016 08:26:54 PM
 *
 */
namespace App\Models;

use App\Models\Base\OrderBase;
 
class Order extends OrderBase
{
    // used to hide columns
    protected $hidden = array();
	
    // used to append columns
    protected $appends = array();
    
    public function account()
    {
    	return $this->belongsTo('\App\Models\Account');
    }

}
