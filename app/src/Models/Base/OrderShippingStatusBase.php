<?php
/**
 * OrderShippingStatusBase Model
 * 
 * Provides data from "'order_shipping_status'"
 *
 * Author: Johnny Angel <johnnyangelnj@gmail.com>
 *
 */

namespace App\Models\Base;




/**
 * Class OrderShippingStatusBase
 *
 * @package App\Models
 *
 * @SWG\Definition(
 *   definition="OrderShippingStatus",
 *  required={"id", "order_id", "order_guid", "status"}
 * )
 *
 */

class OrderShippingStatusBase extends Model
{
	
	
    // turn on auto timestamps create_at and updated_at
    public $timestamps = true;

    // turn on softe delete_at
    protected $softDelete = false;
    
    // used to hide columns 
    protected $hidden = array();
    
    // used to append columns
    protected $appends = array();
    
    protected $table = 'order_shipping_status';
    
    
    


/**
 *
 * @SWG\Property(property="id", type="integer", format="int32")
 * @SWG\Property(property="order_id", type="integer", format="int32")
 * @SWG\Property(property="order_guid", type="string")
 * @SWG\Property(property="status", type="string", description="values ['UNKNOWN', 'PROCESSING', 'ENROUTE', 'DELIVERED', 'CANCELLED'].")

 *
 */
     
     
    protected $fillable = array(
	'id',
	'order_id',
	'order_guid',
	'status',
	'created_at',
	'updated_at'
    );

    protected function getIdAttribute($v){ return (int)$v; }
    protected function getOrderIdAttribute($v){ return (int)$v; }
    protected function getOrderGuidAttribute($v){ return self::unpackGUID($v); }
    protected function getCreatedAtAttribute($v){ return $v; }
    protected function getUpdatedAtAttribute($v){ return $v; }

    protected function setOrderGuidAttribute($v){ $this->attributes['order_guid'] = self::packGUID($v); }

}
