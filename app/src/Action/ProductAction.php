<?php
namespace App\Action;

use Slim\Views\Twig;
use Psr\Log\LoggerInterface;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

final class ProductAction
{
    private $view;
    private $logger;
    private $keys;
    

    public function __construct(Twig $view, LoggerInterface $logger, $keys)
    {
    	$this->view = $view;
        $this->logger = $logger;
        $this->keys = $keys;
    }
        
    public function initialize(Request $request, Response $response, $args)
    {
        $this->logger->debug("Product Initialize Action");
        
        $this->logger->debug("Args [". json_encode($args) ."]");

        $response = $response->withHeader('Content-Type', 'application/json');
        
        $guid = $this->keys->generateGUID();
        
        $product = new \App\Models\Product(array(
        	'guid' => $guid,
			'catalog_id' => $args['catalog_id']
        ));
        
		$product->save();

		$product_images = [];
		
		foreach (array('Large','Medium','Small', 'Tiny') as $image_type) {
			$product_images[] = new \App\Models\ProductImage([
				'product_guid'	=> $guid,
				'type'	=> $image_type
			]);
		}
		
		$product->product_images()->saveMany($product_images);

		echo $product->toJson();
		
		return $response;
    }
    
    /**
     * @return \Illuminate\Http\JsonResponse
     *
     * @SWG\Get(
     *   path="/products",
     *   summary="Papi's Products. Get an inventory of Papi's products.",
     *   operationId="getProducts",
     *   produces={"application/json"},
     *   tags={"Product"},
     *   @SWG\Response(
     *   	response=200,
     *   	description="Product Inventory List, If products not found, response is empty object [].",
     *   	@SWG\Schema(ref="#/definitions/Product")
     *   )
     * )
     */
    
    public function products(Request $request, Response $response, $args)
    {
    	$this->logger->debug("Products Action");
    
    	$response = $response->withHeader('Content-Type', 'application/json');
    
    	echo \App\Models\Product::all('id', 'guid', 'status', 'name', 'description')->toJson();
    
    	return $response;
    }
    
    /**
     * @return \Illuminate\Http\JsonResponse
     * 
     * @SWG\Get(
     *   path="/product/{product_guid}",
     *   summary="Product Details. Get a product by product guid",
     *   operationId="getProductByGuid",
     *   produces={"application/json"},
     *   tags={"Product"},
     *   @SWG\Parameter(ref="#/parameters/product_guid"),
     *   @SWG\Response(
     *   	response=200, 
     *   	description="Product Model, If product not found, response is empty object {}.",
     *   	@SWG\Schema(ref="#/definitions/Product")
     *   )
     * )
     */

    /**
     * @SWG\Parameter(name="product_guid", in="path", type="string", required=true, description="Enter a product guid in the format: 4b572800-d2ff-4afc-836d-70a0bbd23d04")
     */
    
    public function product_details(Request $request, Response $response, $args)
    {
    	$this->logger->debug("Product Details Action");
    	 
    	$this->logger->debug("Args [". json_encode($args) ."]");
    	 
    	//TODO: Refactor to use Key and Guid
    	$product_details = \App\Models\Product::with(['product_images'])->where('guid', '=', \App\Models\Product::packGUID($args['product_guid']))->orWhere('key', '=', $args['product_guid'])->first();

    	$response = $response->withHeader('Content-Type', 'application/json');
    	 
    	echo ( $product_details )?$product_details->toJson():'{}';
    	 
    	return $response;
    }
    
    /**
     * @return \Illuminate\Http\JsonResponse
     *
     * @SWG\Put(
     *   path="/product",
     *   summary="Update Product.",
     *   operationId="putProductbyGuid",
     *   produces={"application/json","text/html"},
     *   tags={"Product"},
     *   @SWG\Parameter(ref="#/parameters/product_status_payload"),
     *   @SWG\Response(
     *   	response=200,
     *   	description="Product update successful."
     *   ),
     *   @SWG\Response(
     *   	response=404,
     *   	description="Product not found."
     *   )
     * )
     *
     * @SWG\Parameter(name="product_status_payload", in="body", type="objext", required=true, description="Product Status Object", @SWG\Schema(ref="#/definitions/ProductStatus"))
     *
     * @SWG\Definition(
     * 	definition="ProductStatus",
     *	required={"product_guid", "product_status","csrf_key_name","csrf_value_name"},
     * @SWG\Property(property="product_guid", type="string", description="Enter a product guid in the format: 4b572800-d2ff-4afc-836d-70a0bbd23d04."),
     * @SWG\Property(property="product_status", type="string", description="Enter a product status values ['PENDING', 'AVAILABLE', 'ON_HOLD', 'SOLD_OUT']"),
     * @SWG\Property(property="csrf_key_name", type="string", description="CSRF token key."),
     * @SWG\Property(property="csrf_value_name", type="string", description="CSRF token value.")
     * )
     *
     */
    
    public function product_status(Request $request, Response $response, $args)
    {
    	$this->logger->debug("Product Status Action");
    	 
    	$json = $request->getBody();
    	 
    	$payload = json_decode($json, true);
    	 
    	$this->logger->debug("Payload [". json_encode($payload) ."]");
    	 
    	$product_guid = $payload['product_guid'];
    	$product_status = $payload['product_status'];
    	    	    	
    	try {
	    	$product = \App\Models\Product::where('guid', '=', \App\Models\Product::packGUID($product_guid))->first();
    	
	    	if( $product ){
	    		$this->logger->debug("Product to update " . json_encode($product) . " Status: " . $product_status);
	    		$response = $response->withHeader('Content-Type', 'application/json');
	    		$product->status = $product_status;
	    		$product->save();
	    		echo json_encode(array('status' => 'Successful', 'response' => 'Product status has been updated.'));
	    	} else {
	    		$response = $response->withStatus(404)->write('Product not found.');
	    	}
    	} catch(Exception $e){
    		$response = $response->withStatus(500)->write($e->getMessage());
    	}
    	     	 
    	return $response;
    }
}
