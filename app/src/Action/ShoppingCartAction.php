<?php
namespace App\Action;

use Slim\Views\Twig;
use Psr\Log\LoggerInterface;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

final class ShoppingCartAction
{
    private $view;
    private $logger;
    private $config;
    private $cookies;
    private $keys;
    private $client_session;
    private $db;
    
    private $shopping_cart_guid;

    public function __construct(Twig $view, LoggerInterface $logger, $client_session, $db)
    {
    	$this->view = $view;
        $this->logger = $logger;
        $this->config = $client_session->getConfig();
        $this->cookies = $client_session->getCookies();
        $this->keys = $client_session->getKeys();
        $this->client_session = $client_session;
        $this->db = $db;
    }

    public function dispatch(Request $request, Response $response, $args)
    {
        $this->logger->debug("Shopping Cart Dispatched Action");
        
        $user_session = $this->client_session->get();

        $show_title_bar_home = true;

        $shopping_cart = $this->client_session->getUserShoppingCart();
        
        $shopping_cart_item_count = $this->client_session->getUserShoppingCartItemCount();
        
        $meta_data = array(
        	
        	"title" => "Shopping Cart",
        	"user" =>  $this->client_session->getUserProfile(),
        	"attributes" => $request->getAttributes(),
        	"show_logo" => true,
        	"is_authenticated" => isset($user_session->is_authenticated)?$user_session->is_authenticated:false,
        	"shopping_cart_item_count" => $this->client_session->getUserShoppingCartItemCount(),
        	"shopping_cart" => $shopping_cart
        );
        
        $this->logger->debug(json_encode($meta_data));
        
        $this->logger->debug("Is User Authenticated? ". ($user_session->is_authenticated?'Yes':'No'));
        
        $this->view->render($response, "shopping-cart.twig", $meta_data);
        return $response;
    }
    
    /**
     * @return \Illuminate\Http\JsonResponse
     *
     * @SWG\Get(
     *   path="/user/shopping-cart/{shopping_cart_guid}",
     *   summary="User Shopping Cart. Get a user shopping cart by shopping cart guid",
     *   operationId="getSessionShoppingCart",
     *   produces={"application/json"},
     *   tags={"User"},
     *   @SWG\Parameter(ref="#/parameters/shopping_cart_guid"),
     *   @SWG\Response(
     *   	response=200,
     *   	description="User Shopping Cart Model and Relationships, If shopping cart not found, response is empty array [].",
     *   	@SWG\Schema(ref="#/definitions/ShoppingCart")
     *   )
     * )
     */
    
    /**
     * @SWG\Parameter(name="shopping_cart_guid", in="path", type="string",  description="Enter a shopping cart guid.")
     */
    
    public function cart(Request $request, Response $response, $args){
    	
    	$this->logger->debug("Shopping Cart Cart Action");
    	
    	$shopping_cart_guid = $args['shopping_cart_guid'];
    	
    	$response = $response->withHeader('Content-Type', 'application/json');
    	
    	echo \App\Models\ShoppingCart::with(['items'])->where('guid', '=', \App\Models\ShoppingCart::packGUID($shopping_cart_guid))->first()->toJson();
    	
    	return $response;
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     *
     * @SWG\Delete(
     *   path="/user/shopping-cart/{shopping_cart_guid}",
     *   summary="User Shopping Cart Remove. Remove a user shopping cart by shopping cart guid.",
     *   operationId="deleteSessionShoppingCart",
     *   produces={"application/json","text/html"},
     *   tags={"User"},
     *   @SWG\Parameter(ref="#/parameters/shopping_cart_guid"),
     *   @SWG\Response(
     *   	response=200,
     *   	description="User Shopping Cart Delete Successful Message."
     *   ),
     *   @SWG\Response(
     *   	response=404,
     *   	description="User Shopping Cart Not Found."
     *   ),
     *   @SWG\Response(
     *   	response=500,
     *   	description="Application Error."
     *   ),
     * )
     */
    
    public function remove(Request $request, Response $response, $args){
    
    	$this->logger->debug("Shopping Cart Remove Action");
    	
    	$shopping_cart_guid = $args['shopping_cart_guid'];
    	
    	$user_session = $this->client_session->get();
    	
    	$response = $response->withHeader('Content-Type', 'application/json');
    	
    	try{
    		$shopping_cart = \App\Models\ShoppingCart::with(['items'])->where('guid', '=', \App\Models\ShoppingCart::packGUID($shopping_cart_guid))->first();
    	
    		//TODO: Need to delete to be soft.
	    	if( $shopping_cart ){
		    	foreach($shopping_cart->items as $item)
		    		$item->delete();
		    	
	    		$shopping_cart->delete();
	    		
	    		$user_session->has_shopping_cart = false;
	    		$user_session->shopping_cart = [];
	    		$response = $response->withHeader('Set-Cookie', $this->cookies->setDomainCookie($this->config['cookie.options']['cookie_name'], json_encode($user_session)));
	    		 
	    		echo json_encode(array('status' => 'Successful'));
	    	} else {
	    		$response = $response->withStatus(404)->write('Shopping Cart not found!');
	    	}
    	} catch(Exception $e) {
    		$response = $response->withStatus(500)->write($e->getMessage());
    	}
    	 
    	return $response;
    }
    
    public function item(Request $request, Response $response, $args){
    	
    	$this->logger->debug("Shopping Cart Item Action");
    	
    	$user_session = $this->client_session->get();
    	
    	$user_shopping_cart = null;
    	
    	if(! $user_session->has_shopping_cart ){
    		//Create Shopping Cart & Item
    		$this->shopping_cart_guid = $this->keys->generateGUID();
    		$this->logger->debug("Shopping Cart Creating ".$this->shopping_cart_guid);
    		$user_shopping_cart = new \App\Models\ShoppingCart(array('guid' => $this->shopping_cart_guid, 'session_id' => $user_session->id));
    		$user_shopping_cart->save();
    	} else {
    		//Get Shopping Cart & Items
    		$user_shopping_cart =  \App\Models\ShoppingCart::where('guid', '=', \App\Models\ShoppingCart::packGUID($user_session->shopping_cart->guid))->first();
			//Re-Create Shopping Cart & Items if not found
			if(! $user_shopping_cart){
				$user_shopping_cart = new \App\Models\ShoppingCart(array('guid' => $user_session->shopping_cart->guid, 'session_id' => $user_session->id));
				$user_shopping_cart->save();

				//Add items
			}
    	}
    	
    	$this->logger->debug("User Shopping Cart: " . $user_shopping_cart->toJson());
    	
    	if( $user_shopping_cart ){
    		$user_session->has_shopping_cart = true;
    		
			$product_details = \App\Models\Product::where('guid','=', \App\Models\Product::packGUID($args['product_guid']))->first();	
			    	
	    	//Add Item to Cart
	    	$this->logger->debug("Product   ID: ". $product_details->id);
	    	$this->logger->debug("Product GUID: ". $product_details->guid);
	    	$this->logger->debug("Product Status: ". $product_details->status);
	    	
	    	$response = $response->withHeader('Content-Type', 'application/json');
	    	 
	    	if(\App\Models\Product::product_state()['A'] == $product_details->status){
	    		$this->logger->debug("Product is Available for Sale ");
	    		try {
	    			$this->db->select('SET SESSION TRANSACTION ISOLATION LEVEL SERIALIZABLE');
	    			$this->db->transaction(function() use ($response, $user_session, $user_shopping_cart, $product_details) {
	    				
	    				$product_details->status = 'ON_HOLD';
	    				$product_details->save();
	    				
	    				//Add Shopping Cart to Session
	    				$shopping_cart_item = \App\Models\ShoppingCartItems::where('product_id', '=', $product_details->id)->where('product_guid', '=', \App\Models\ShoppingCartItems::packGUID($product_details->guid))->first();
	    		
	    				if(! $shopping_cart_item ){
	    					$shopping_cart_item = new  \App\Models\ShoppingCartItems(array(
	    						'shopping_cart_id' => $user_shopping_cart->id,
	    						'shopping_cart_guid' => $user_shopping_cart->guid,
	    						'product_id' => $product_details->id,
	    						'product_guid' => $product_details->guid,
	    						'quantity' => 1,
	    						'price' => $product_details->price,
	    					));
	    					$shopping_cart_item->save();
	    				} else {
	    					$this->logger->debug("Item Exists in Shopping Cart " . json_encode($shopping_cart_item));
	    				}
	    			});
	    			
	    			$user_session->shopping_cart =  \App\Models\ShoppingCart::with(['items'])->find($user_shopping_cart->id);
	    				 
	    			$response = $response->withHeader('Set-Cookie', $this->cookies->setDomainCookie($this->config['cookie.options']['cookie_name'], json_encode($user_session)));
	    				 
	    			echo $user_session->shopping_cart;
	    				 
	    		} catch(\RuntimeException $e) {
	    			$response = $response->withStatus(500)->write('Error: '.$e->getMessage());
	    		}	    		
	    	} else {
	    		//Message indicating that the product is not available
	    	}
    	}
    	
    	return $response;    	 
    }
    
    /**
     * @return \Illuminate\Http\JsonResponse
     *
     * @SWG\Delete(
     *   path="/user/shopping-cart/item/{shopping_cart_guid}/{shopping_cart_item_id}",
     *   summary="User Shopping Cart Item Remove. Remove a user shopping cart item by shopping cart guid and item id.",
     *   operationId="deleteSessionShoppingCartItem",
     *   produces={"application/json","text/html"},
     *   tags={"User"},
     *   @SWG\Parameter(ref="#/parameters/shopping_cart_guid"),
     *   @SWG\Parameter(ref="#/parameters/shopping_cart_item_id"),
     *   @SWG\Response(
     *   	response=200,
     *   	description="User Shopping Cart Item Delete Successful Message."
     *   ),
     *   @SWG\Response(
     *   	response=404,
     *   	description="User Shopping Cart Item Not Found."
     *   ),
     *   @SWG\Response(
     *   	response=500,
     *   	description="Application Error."
     *   ),
     * )
     */
    
    /**
     * @SWG\Parameter(name="shopping_cart_item_id", in="path", type="integer",  description="Enter a shopping cart item id.")
     */
    
    public function remove_item(Request $request, Response $response, $args){
    	 
    	$this->logger->debug("Shopping Cart Remove Item Action");

    	$shopping_cart_guid = $args['shopping_cart_guid'];
    	$shopping_cart_item_id = $args['shopping_cart_item_id'];
    	 
    	$user_session = $this->client_session->get();
    	 
    	$response = $response->withHeader('Content-Type', 'application/json');
    	 
    	try{
    		$shopping_cart = \App\Models\ShoppingCart::with(['items'])->where('guid', '=', \App\Models\ShoppingCart::packGUID($shopping_cart_guid))->first();
    		 
    		$item_index;
    		
    		//TODO: Need to delete to be soft.
    		if( $shopping_cart ){
    			foreach($shopping_cart->items as $key => $item){
    				if($item->id == $shopping_cart_item_id){
    					$item->delete();
    					$item_index = $key;
    				}
    			}
    			
    			$shopping_cart = $shopping_cart->toArray();
    			unset($shopping_cart[$item_index]);
    			$user_session->shopping_cart = $shopping_cart;
    			
    			$response = $response->withHeader('Set-Cookie', $this->cookies->setDomainCookie($this->config['cookie.options']['cookie_name'], json_encode($user_session)));
    	
    			echo json_encode(array('status' => 'Successful'));
    		} else {
    			$response = $response->withStatus(404)->write('Shopping Cart not found!');
    		}
    	} catch(Exception $e) {
    		$response = $response->withStatus(500)->write($e->getMessage());
    	}
    	
    	return $response;
    }
    

}
