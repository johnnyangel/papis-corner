<?php
/**
 *  User Model
 * 
 * Provides data from "'user'"
 *
 * Author: Johnny Angel <johnnyangelnj@gmail.com>
 * Generated on Sunday 17th of January 2016 08:26:54 PM
 *
 */
namespace App\Models;

use App\Models\Base\UserBase;
 
class User extends UserBase
{
    // used to hide columns
    protected $hidden = array();
	
    // used to append columns
    protected $appends = array();
    
    public function account()
    {
    	return $this->hasOne('\App\Models\Account');
    }
    
    public function permissions()
    {
    	return $this->hasMany('\App\Models\Permissions');
    }
    
    public function social_media()
    {
    	return $this->hasMany('\App\Models\SocialMedia');
    }

    public function profile_picture()
    {
    	return $this->hasMany('\App\Models\ProfilePicture');
    }
}
