<?php
namespace App\Utils;

use Psr\Log\LoggerInterface;
use Psr\Http\Message\ServerRequestInterface as Request;

class FaceBook {
	
	private $logger;
	private $config;
	private $cookies;
	private $fb;
	private $access_token;
	private $user_profile;
	
	private $isError = false;
	private $error = array(
		'code' => 0,
		'message' => ''
	);
	
	public function __construct(LoggerInterface $logger, $cookies)
	{
		$this->logger  = $logger;
		$this->config  = $cookies->getConfig();
        $this->cookies = $cookies;
        $this->error = (object) $this->error;
	}
	
	public function getLoginLink(){
		
		$this->logger->debug("Facebook Get Login Link Action");
		
		$this->connect();
		
		$helper = $this->fb->getRedirectLoginHelper();
		$permissions = $this->config['facebook.options']['permissions']; // optional
		$loginUrl = $helper->getLoginUrl('http://' . $this->config['domain'] . '/user/facebook-login', $permissions);
				
		$this->disconnect();
	
		return  (object) array('login_url' => $loginUrl);
	}
	
	public function getUserProfile($id, $name) {
		
		$this->logger->debug("Facebook Get User Profile Action for ID: " . $id . " Name:" . $name);
	
		$this->connect();
										
		$helper = $this->fb->getJavaScriptHelper();
				
		try {
			$this->accessToken = $helper->getAccessToken();
		} catch(Facebook\Exceptions\FacebookResponseException $e) {
			$this->isError = true;
			$this->error->code = $e->getCode();
			$this->error->message = "Graph returned an error:  " . $e->getMessage();
			$this->logger->error(json_encode($this->error));
		} catch(Facebook\Exceptions\FacebookSDKException $e) {
			$this->isError = true;
			$this->error->code = $e->getCode();
			$this->error->message = "Facebook SDK returned an error: " . $e->getMessage();
			$this->logger->error(json_encode($this->error));
		}
			
		if (! isset($this->accessToken)) {
			$this->isError = true;
			$this->error->code = 401;
			$this->error->message = 'No OAuth data could be obtained from the signed request. User has not authorized your app yet.';
			$this->logger->error(json_encode($this->error));
		} else {
			$isConnected = true;
			$this->logger->debug("Facebook Access Token: " . json_encode($this->accessToken->getValue()));
			
			$response = null;
			
			try {
				$response = $this->fb->get('/me?fields=id,email,name,first_name,last_name,picture', $this->accessToken);
			} catch(Facebook\Exceptions\FacebookResponseException $e) {
				$this->isError = true;
				$this->error->code = $e->getCode();
				$this->error->message = "Graph returned an error:  " . $e->getMessage();
				$this->logger->error(json_encode($this->error));
			} catch(Facebook\Exceptions\FacebookSDKException $e) {
				$this->isError = true;
				$this->error->code = $e->getCode();
				$this->error->message = "Facebook SDK returned an error: " . $e->getMessage();
				$this->logger->error(json_encode($this->error));
			}
			
			$this->user_profile = $response->getGraphUser();
			//$this->logger->debug("Facebook User Profile: " . json_encode($this->user_profile));
			$this->logger->debug("Facebook User Name: " . $this->user_profile->getName());
			$this->logger->debug("Facebook User Email: " . $this->user_profile->getEmail());
			$this->logger->debug("Facebook User Picture: " . $this->user_profile->getPicture());
		}
		
		$this->disconnect();
		
		return (object) array('status' => ($isConnected)?'Successful':'Failed', 'user_profile' => $this->user_profile);
	}
	
	public function connect() {
		
		$this->logger->debug("Facebook Connect");
		
		$env = $this->config['env'];
		
		$app_id = $this->config['facebook.options'][$env]['app_id'];
		$secret = $this->config['facebook.options'][$env]['secret'];
		
		$this->fb = new \Facebook\Facebook([
				'app_id'     => $app_id,
				'app_secret' => $secret,
				'default_graph_version' => $this->config['facebook.options']['default_graph_version'],
				'persistent_data_handler'=>'session'
		]);
	}
	
	public function disconnect() {
		$this->logger->debug("Facebook Disconnect");
	}
	
	public function getError() {
		return $this->error;
	}
}