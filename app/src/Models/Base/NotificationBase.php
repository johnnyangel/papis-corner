<?php
/**
 * NotificationBase Model
 * 
 * Provides data from "'notification'"
 *
 * Author: Johnny Angel <johnnyangelnj@gmail.com>
 *
 */

namespace App\Models\Base;


use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class NotificationBase
 *
 * @package App\Models
 *
 * @SWG\Definition(
 *   definition="Notification",
 *  required={"id", "user_id", "type", "html"}
 * )
 *
 */

class NotificationBase extends Model
{
	use SoftDeletes;
	
    // turn on auto timestamps create_at and updated_at
    public $timestamps = true;

    // turn on softe delete_at
    protected $softDelete = true;
    
    // used to hide columns 
    protected $hidden = array();
    
    // used to append columns
    protected $appends = array();
    
    protected $table = 'notification';
    
    protected $dates = ['deleted_at'];
    


/**
 *
 * @SWG\Property(property="id", type="integer", format="int32")
 * @SWG\Property(property="user_id", type="integer", format="int32")
 * @SWG\Property(property="type", type="string", description="values ['SYSTEM', 'ACCOUNT'].")
 * @SWG\Property(property="html", type="string")

 *
 */
     
     
    protected $fillable = array(
	'id',
	'user_id',
	'type',
	'html',
	'created_at',
	'updated_at',
	'deleted_at'
    );

    protected function getIdAttribute($v){ return (int)$v; }
    protected function getUserIdAttribute($v){ return (int)$v; }
    protected function getHtmlAttribute($v){ return ($v)?$v:''; }
    protected function getCreatedAtAttribute($v){ return $v; }
    protected function getUpdatedAtAttribute($v){ return $v; }
    protected function getDeletedAtAttribute($v){ return $v; }

    

}
