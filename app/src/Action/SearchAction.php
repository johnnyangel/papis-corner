<?php
namespace App\Action;

use Slim\Views\Twig;
use Psr\Log\LoggerInterface;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

final class SearchAction
{
    private $view;
    private $logger;
    private $client_session;

    public function __construct(Twig $view, LoggerInterface $logger, $client_session)
    {
    	$this->view = $view;
        $this->logger = $logger;
        $this->client_session = $client_session;
    }

    public function dispatch(Request $request, Response $response, $args)
    {
        $this->logger->debug("Search Dispatched Action");
        
        $query_string = $request->getParam('search_filter');
        
        $this->logger->debug("Args [". $query_string  ."]");
        
        $user_session = $this->client_session->get();

        $search_tags = $this->get_search_tags($query_string); 
                     
        $query_tags = $this->get_search_tags($query_string);
        
        $this->logger->debug("Search Tags [". json_encode($query_tags)  ."]");
        
        $search_results_for_tag = array();
        
        $featured_product_images = \App\Models\Product::with(['product_images' =>
        	function($query) {
        		$query->where('type', '=' ,'SMALL')->orWhere('type', '=' ,'LARGE')->orWhere('type', '=' ,'TINY');
        	}
        ])->where('status', '!=', 'PENDING')->get();
        
        foreach($query_tags as $tag){
        	$search_results_for_tag[$tag] = \App\Models\SearchTags::with(['search_tag_products'])->where('name', 'LIKE', '%'.$tag.'%')->get();
        }
        
        if(! empty($query_tags))	
        	$featured_product_images = $this->apply_search_algorithm($search_results_for_tag, $featured_product_images);
        
        $meta_data = array(
        		"title" => "Search",
        		"user" =>  $this->client_session->getUserProfile(),
        		"attributes" => $request->getAttributes(),
        		"show_title_bar_home" => true,
        		"show_title_bar_search_filter" => true,
        		"search_string" => $query_string,
        		"is_authenticated" => isset($user_session->is_authenticated)?$user_session->is_authenticated:false,
        		"shopping_cart_item_count" => $this->client_session->getUserShoppingCartItemCount(),
        		"shopping_cart" => $this->client_session->getUserShoppingCart(),
        		"featured_product_images" => $featured_product_images,
        );
        
        $this->logger->debug(json_encode($meta_data));
        
        $this->logger->debug("Is User Authenticated? ". ($user_session->is_authenticated?'Yes':'No'));
        
        $this->view->render($response, "search.twig", $meta_data);
        return $response;
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     *
     * @SWG\Post(
     *   path="/search/build-index",
     *   summary="Papi's Search. Build Papi's search index.",
     *   operationId="postSearchIndex",
     *   produces={"application/json"},
     *   tags={"Search"},
     *   @SWG\Parameter(ref="#/parameters/build_search_index_payload"),
     *   @SWG\Response(
     *   	response=200,
     *   	description="Search index build Successful",
     *   )
     * )
     * 
     * @SWG\Parameter(name="build_search_index_payload", in="body", type="objext", required=true, description="Build Search Index Object", @SWG\Schema(ref="#/definitions/Build_Search_Index"))
     * 
     * @SWG\Definition(
     * 	definition="Build_Search_Index",
     *	required={"csrf_key_name","csrf_value_name"},
     * @SWG\Property(property="csrf_key_name", type="string", description="CSRF token key."),
     * @SWG\Property(property="csrf_value_name", type="string", description="CSRF token value.")
     * )
     */
    
    public function build_index(Request $request, Response $response, $args)
    {
    	$this->logger->info("Search Build Index Action");
    
    	$response = $response->withHeader('Content-Type', 'application/json');
    	
    	$products = \App\Models\Product::all()->toArray();
    	
    	foreach($products as $product){
    		if($product['status'] != 'PENDING'){
    			$parse_string = $product['name'] . ' ' . $product['caption'] . ' ' . $product['description'] . ' ' . $product['provided_by'] . ' ' . $product['status'] . ' ' . $product['tags'];
    			if(strlen($parse_string) > 1) $search_words = $this->get_search_tags($parse_string);
    			$search_words[] = strtolower($product['key']);
    			$search_words[] = $product['price'];
    			$this->logger->debug("Search Tags [". json_encode($search_words)  ."]");
    			    			
    			foreach($search_words as $tag){
    				
    				$this->logger->debug("Tag [". json_encode($tag)  ."]");
    				
    				$search_tags = \App\Models\SearchTags::firstOrCreate(array('name' => $tag));
    				
    				$this->logger->debug("DB Tag [". json_encode($search_tags)  ."]");
    				
    				$search_tags_has_product = \App\Models\SearchTagsHasProduct::where('guid', '=', \App\Models\SearchTagsHasProduct::packGUID($product['guid']));
    				
    				if(isset($search_tags_has_product) || empty($search_tags_has_product) ){
	    				$product_search_tag = new \App\Models\SearchTagsHasProduct(array(
	    					'search_tags_id' => $search_tags->id,
	    					'product_id' => $product['id'],
				        	'product_guid' => $product['guid']
				        ));
	    				$product_search_tag->save();
    				} else 
    					$this->logger->debug("Search Tags Has Product [". json_encode($search_tags_has_product)  ."]");
    			}
    		}
    	}
    	
    	echo json_encode(array('status' => 'Successful'));

    	return $response;
    }
    
    private function get_search_tags($query_string) {
    	$search_tags = [];
    	$words_to_ignore = ['by', 'to', 'from', 'of', 'a', 'on', 'my', 'and', 'with', ''];
    	
    	$query_string = preg_replace("/[^a-zA-Z 0-9]+/", "", $query_string );
    	
    	$search_tags = explode ( ' ' , strtolower($query_string));
    	
    	$search_tags = array_diff($search_tags, $words_to_ignore);
    	
    	$search_tags = array_unique($search_tags);
    	
    	return $search_tags;
    }
    
    private function apply_search_algorithm($search_results_for_tags, $featured_product_images){
    	
    	$product_guids = $temp_feature_product_images = array();
    	
    	foreach($search_results_for_tags as $tags){
    		foreach($tags as $tag){
    			foreach($tag->search_tag_products as $search_product){
					$product_guids[$search_product->product_id] = $search_product->product_guid;
    			}
    		}
    	}
    	
    	$this->logger->debug("Search Product Guids [". json_encode($product_guids)  ."]");
    	
    	foreach($product_guids as $id => $guid){
    		foreach($featured_product_images as $product){
    			if($product['guid'] == $guid)
    				$temp_feature_product_images[] = $product;
    		}
    		
    	}
    	
    	return $temp_feature_product_images;
    	
    }
    
}
