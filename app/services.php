<?php
// DIC configuration
$container = $app->getContainer();

// SMTP Service
//$container->register(new App\Service\SmtpProvider());

// Twitter Oauth Service
$container->register(new App\Service\TwitterOauthProvider());