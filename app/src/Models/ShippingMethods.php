<?php
/**
 *  ShippingMethods Model
 * 
 * Provides data from "'shipping_methods'"
 *
 * Author: Johnny Angel <johnnyangelnj@gmail.com>
 * Generated on Friday 11th of March 2016 02:23:33 PM
 *
 */
namespace App\Models;

use App\Models\Base\ShippingMethodsBase;
 
class ShippingMethods extends ShippingMethodsBase
{
    // used to hide columns
    protected $hidden = array();
	
    // used to append columns
    protected $appends = array();

}
