<?php
/**
 *  OrderItems Model
 * 
 * Provides data from "'order_items'"
 *
 * Author: Johnny Angel <johnnyangelnj@gmail.com>
 * Generated on Sunday 17th of January 2016 08:26:54 PM
 *
 */
namespace App\Models;

use App\Models\Base\OrderItemsBase;
 
class OrderItems extends OrderItemsBase
{
    // used to hide columns
    protected $hidden = array();
	
    // used to append columns
    protected $appends = array();

}
