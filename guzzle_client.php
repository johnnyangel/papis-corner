$jar = new GuzzleHttp\Cookie\CookieJar();
$default = [
    "cookies" => $jar
];
$c = new GuzzleHttp\Client(["defaults" => $default]);


// Set up a cookie - name, value AND domain.
$cookie = new Guzzle\Plugin\Cookie\Cookie();
$cookie->setName('PHPSESSID');
$cookie->setValue($session_id_from_api);
$cookie->setDomain($domain_of_my_service_url);

// Set up a cookie jar and add the cookie to it.
$jar = new Guzzle\Plugin\Cookie\CookieJar\ArrayCookieJar();
$jar->add($cookie);

// Set up the cookie plugin, giving it the cookie jar.
$plugin = new Guzzle\Plugin\Cookie\CookiePlugin($jar);

// Register the plugin with the client.
$client->addSubscriber($plugin);

	    	//Get Product and Set Hold
	    	$guzzle_client = new \GuzzleHttp\Client(['base_uri' => $this->config['protocol'] . $this->config['domain'], 'cookies' => true]);
	    	$guzzle_request = new \GuzzleHttp\Psr7\Request('GET', '/api/product/'. $args['product_guid']);
	    	$guzzle_response = $guzzle_client->send($guzzle_request);
			$json = $guzzle_response->getBody(true);
	    	$this->logger->debug("Guzzle Response Body " . $json);
	    	$product_details = json_decode($json);


// Now do the request as normal.
$request = $client->get($url);
$response = $client->send($request);

// The returned page body.
echo $response->getBody(true);
