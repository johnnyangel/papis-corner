<?php 
namespace App\Middleware;

use Psr\Log\LoggerInterface;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;


class Authenticate
{
	private $admin_permission = 'MASTER';
	private $super_permission = 'SLAVE';
	private $admin_home = '/admin/__ID__/home';
	private $user_home = '/user/__ID__/home';
	private $public_home = '/';
	
	private $admin_routes = array(
			array("method" => "GET", "uri" => "/\/admin\/(\d+)\/home/i"),
			array("method" => "GET", "uri" => "/\/api\/home/i"),
			array("method" => "GET", "uri" => "/\/api\/swagger\/console/i"),
			array("method" => "GET", "uri" => "/\/api\/swagger\/json/i"),
			array("method" => "GET", "uri" => "/\/api\/user\/csrf_token/i"),
			array("method" => "GET", "uri" => "/\/api\/user\/sessions/i"),
			array("method" => "GET", "uri" => "/\/api\/products/i"),
			
			array("method" => "PUT", "uri" => "/\/api\/user\/profile/i"),
			array("method" => "PUT", "uri" => "/\/api\/user\/reset_password/i"),
			array("method" => "PUT", "uri" => "/\/api\/product/i"),
			
			array("method" => "POST", "uri" => "/\/api\/user\/profile/i"),
			array("method" => "POST", "uri" => "/\/api\/search\/build-index/i"),
			
	);
	
	private $super_routes = array(
				
	);
	
	private $user_routes = array(
			array("method" => "GET", "uri" => "/\/user\/(\d+)\/home/i"),
			array("method" => "GET", "uri" => "/\/user\/(\d+)\/my-account/i"),
			array("method" => "GET", "uri" => "/\/user\/(\d+)\/csrf-token/i"),
			array("method" => "GET", "uri" => "/\/user\/(\d+)\/my-profile/i"),
			array("method" => "PUT", "uri" => "/\/user\/(\d+)\/profile/i"),
			array("method" => "POST", "uri" => "/\/user\/(\d+)\/address/i"),
			array("method" => "POST", "uri" => "/\/user\/(\d+)\/profile-picture/i"),		
			array("method" => "DELETE", "uri" => "/\/user\/(\d+)\/logout/i"),
			array("method" => "DELETE", "uri" => "/\/user\/(\d+)\/address/i"),
	);
	
	private $public_routes = array(
			array("method" => "GET",  "uri" => "/\/$/i"),
			array("method" => "GET",  "uri" => "/\/user\/login/i"),
			array("method" => "GET",  "uri" => "/\/user\/facebook-login/i"),
			array("method" => "GET",  "uri" => "/\/user\/twitter-login-url/i"),
			array("method" => "GET",  "uri" => "/\/user\/twitter-login/i"),
			array("method" => "GET",  "uri" => "/\/user\/search/i"),
			array("method" => "GET",  "uri" => "/\/user\/shopping-cart/i"),
			array("method" => "GET",  "uri" => "/\/user\/registration-confirmation/i"),
			array("method" => "GET",  "uri" => "/\/user\/csrf-token/i"),
			array("method" => "GET",  "uri" => "/\/user\/known-location/i"),
			array("method" => "GET",  "uri" => "/\/user\/product\/view\/.+/i"),
			array("method" => "GET",  "uri" => "/\/user\/product-bookmark\/view\/.+/i"),
			
			array("method" => "GET",  "uri" => "/\/api\/product\/.+/i"),
			array("method" => "GET",  "uri" => "/\/api\/references\/address-types/i"),
			array("method" => "GET",  "uri" => "/\/api\/references\/payment-methods/i"),
			array("method" => "GET",  "uri" => "/\/api\/references\/shipping-methods/i"),
			array("method" => "GET",  "uri" => "/\/api\/references\/states/i"),
			array("method" => "GET",  "uri" => "/\/api\/references\/countries/i"),
			array("method" => "GET",  "uri" => "/\/api\/references\/months/i"),
			array("method" => "GET",  "uri" => "/\/api\/user\/shopping-cart\/.+/i"),
			
			array("method" => "PUT",  "uri" => "/\/user\/reset-password\/.+/i"),
			
			array("method" => "POST", "uri" => "/\/user\/authenticate/i"),
			array("method" => "POST", "uri" => "/\/user\/google-login/i"),
			array("method" => "POST", "uri" => "/\/user\/profile/i"),
			array("method" => "POST", "uri" => "/\/user\/register/i"),
			
			array("method" => "POST", "uri" => "/\/api\/user\/shopping-cart\/item\/.+/i"),
			
			array("method" => "DELETE",  "uri" => "/\/api\/user\/shopping-cart\/.+/i"),
			array("method" => "DELETE",  "uri" => "/\/api\/user\/shopping-cart\/.+\/\/item\/.+/i"),
			
	);
	
	private $config;
	private $logger;
	private $cookies;
	private $method;
	private $uri;
	private $user_id;
	private $permissions;
	private $user_group;

	public function __construct(LoggerInterface $logger, $cookies)
	{
		
		$this->logger = $logger;
		$this->config = $cookies->getConfig();
		$this->cookies = $cookies;
	}
	
	/**
	 *  __invoke
	 *
	 */
	
	public function __invoke($request, $response, $next)
    {

    	$this->user_group = null;
    	$this->method = $request->getMethod();
    	$this->uri = $request->getUri()->getPath();
    	
    	$this->logger->info("Authenticate User Access Method [". $this->method . "] URI [" . $this->uri ."]");
    	
    	//Check if Request Uri has Embedded User Id
    	$request_uri_user_id = null;
    	$pattern = "/\/([\d+?])\//";
    	if(preg_match($pattern, $this->uri, $matches))
    		$request_uri_user_id =  $matches[1];
    	
    	$user_session = $this->cookies->getDomainCookie($this->config['cookie.options']['cookie_name']);

    	$this->logger->debug("User Session [". $user_session ."]");
    	
    	if( $user_session ){
    		$user_session = json_decode($user_session);
    	
    		if( isset($user_session->user_profile->permissions)  )
    			$this->permissions = $user_session->user_profile->permissions;
    	
    		if( isset($user_session->user_profile->id) )
    			$this->user_id = $user_session->user_profile->id;
    	}
    	    	
    	if( $request_uri_user_id && $this->user_id && ($this->user_id != $request_uri_user_id)){
    		$this->logger->debug("Access Denied Request User ID[" . $request_uri_user_id . "] == Session User ID[" . $this->user_id . "]");
    		return $response->withStatus(401)
    				->withHeader('Content-type', 'text/html')
	    				->withHeader('X-Status-Reason', '401 Access Denied.')
							->write('Papi told me not to talk to strangers!');
    	}
    	 
		if (!$this->isPublicRoutes()) {
			$this->logger->info("Access is not Public Authorize User Required!");
			
			if(! isset($user_session->is_authenticated) ) $user_session->is_authenticated = false;
		
			if($this->isAdminRoutes() && $this->hasPermissions($this->admin_permission) && $user_session->is_authenticated){
				$this->logger->info("User has Admin Access");
				$this->user_group = 'MASTER';
			} else if($this->isSuperRoutes() && $this->hasPermissions($this->super_permission) && $user_session->is_authenticated){
				$this->logger->info("User has Super Access");
				$this->user_group = 'SUPER';
			} else 	if($this->isUserRoutes() && $user_session->is_authenticated){
					$this->logger->info("User has User Access");
					$this->user_group = 'USER';
			} else {
				$response = $response
				->withStatus(401)
				->withHeader('Content-type', 'text/html')
				->withHeader('X-Status-Reason', '401 Access Denied.');
				if( ! $user_session->is_authenticated && !($this->user_group == 'USER' || $this->user_group == null) ) {	
					$this->logger->info("401 - Access Denied!");
					$response = $response
						->withHeader('Location', '/user/login', true);
				} else {
					$this->admin_home = str_replace('__ID__', $this->user_id, $this->admin_home);
					$this->user_home = str_replace('__ID__', $this->user_id, $this->user_home);
					
					switch ($this->user_group) {
					    case 'MASTER':
					    	$response = $response
					    		->withHeader('Location', $this->admin_home, true);
					        break;
					    case 'SUPER':
					    	$response = $response
					    		->withHeader('Location', $this->admin_home, true);
					        break;
					    case 'USER':
					    	$response = $response
					    		->withHeader('Location', $this->user_home, true);					        
					        break;
					    default:
					    	$response = $response
					    		->withHeader('Location', $this->public_home, true);
					    	
					}
				}
			}
		} else {
			$this->logger->info("Public Access Allowed");
		}
		
		$response = $next($request, $response);
	
		return $response;
	}
	
	/**
	 * isPublic
	 *
	 */
	public function isPublicRoutes()
	{	 
		$isPublic = $this->isRoutes($this->public_routes);
		 
		$this->logger->debug("isPublicRoutes? " . ($isPublic?'Yes':'No'));
		
		return $isPublic;
	}

	public function isUserRoutes()
	{
		$isUserRoute = $this->isRoutes($this->user_routes);
				
		$this->logger->debug("isUserRoute? " . ($isUserRoute?'Yes':'No'));
			
		return $isUserRoute;
	}
	
	public function isSuperRoutes()
	{
		$isSuperRoute =  $this->isRoutes($this->super_routes);
			
		$this->logger->debug("isSuperRoute? " . ($isSuperRoute?'Yes':'No'));
			
		return $isSuperRoute;
	}
	
	public function isAdminRoutes()
	{	
		$isAdminRoute = $this->isRoutes($this->admin_routes);
			 
		$this->logger->debug("isAdminRoute? " . ($isAdminRoute?'Yes':'No'));
			
		return $isAdminRoute;
	}
	
	public function hasPermissions($permission) {
		$hasPermissions = false;

		$this->logger->debug("User Permission [" . json_encode($this->permissions) . "] = [" . $permission . "]");
		
		if(! $this->permissions) return $hasPermissions;
				
		if(is_array($this->permissions)){
			foreach($this->permissions as $p){
				$this->logger->debug("Does User Have Permission [" . $p->permission . "] = [" . $permission . "]");
				if($p->permission == $permission)
					$hasPermissions = true;
			}
		} else if($this->permissions->permission == $permission){
			$hasPermissions = true;
		}
		
		$this->logger->debug("hasPermissions? " . ($hasPermissions?'Yes':'No'));

		return $hasPermissions;
	}
	
	private function isRoutes($routes)
	{
		$isRoute = false;
		
		foreach($routes as $key => $route){
			$this->logger->debug($route['method'] . " [" . $route['uri'] . "] " . $this->method . " [" . $this->uri . "]");
			if($route['method'] == $this->method && $this->uri == $route['uri'])
				$isRoute = true;
			else if($route['method'] == $this->method && preg_match($route['uri'], $this->uri))
				$isRoute = true;
		}
			
		return $isRoute;		
	}
	
	private function sendUserHome(Response $response){
		
		switch ($this->user_group) {
			case $this->admin_permission:
				$response = $response
				->withHeader('Location', $this->admin_home, true);
				break;
			case $this->super_permission:
				$response = $response
				->withHeader('Location', $this->admin_home, true);
				break;
			case $this->user_permission:
				$response = $response
				->withHeader('Location', $this->user_home, true);
				break;
			default:
				$response = $response
				->withHeader('Location', $this->public_home, true);
		}
		
		return 	$response->withStatus(301)->withHeader('X-Status-Reason', '301 Moved Permanently.');	
	}
}

