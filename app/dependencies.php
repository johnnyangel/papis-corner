<?php
// DIC configuration
$container = $app->getContainer();


// -----------------------------------------------------------------------------
// Service providers
// -----------------------------------------------------------------------------

// Config
$container['config'] = function ($c) {
	$config = __DIR__ . '/../config.php';
	if (! file_exists($config)) die('<code>Missing '.$config.' !</code>');
	return  require($config);
};

// Csrf
$container['csrf'] = function ($c) {
	$guard = new \Slim\Csrf\Guard;
	
	/* Custom Error Handling */
	$guard->setFailureCallable(function ($request, $response, $next) {
		$request = $request->withAttribute("csrf_result", false);
		return $next($request, $response);
	});
	
	return $guard;
};

// Flash messages
$container['flash'] = function ($c) {
	return new \Slim\Flash\Messages;
};

// Swift Mailer Transport
$container['transport'] = function ($c) {
	$config = $c->get('config');
	$transport = Swift_SmtpTransport::newInstance($config['smtp.options']['host'], $config['smtp.options']['connection_config']['port'], $config['smtp.options']['connection_config']['ssl']);
	$transport->setUsername($config['smtp.options']['connection_config']['username']);
	$transport->setPassword($config['smtp.options']['connection_config']['password']);
	return  Swift_Mailer::newInstance($transport);
};

// DB Connection
$container['db'] = function ($c) {
	$config  = $c->get('config');
	$container = new Illuminate\Container\Container;
	$connFactory = new \Illuminate\Database\Connectors\ConnectionFactory($container);
	return $connFactory->make($config['db.connections']['default']);
};

// Error Handling

// 404 Error
$container['notFoundHandler'] = function ($c) {
	return function ($request, $response) use ($c) {
		
		return $response->withStatus(404)
		->withHeader('Content-Type', 'text/html')
		->write('Papi are you in the right place?');
	};
};

// 405 Error
$container['notAllowedHandler'] = function ($c) {
	return function ($request, $response, $methods) use ($c) {
		return $c['response']->withStatus(405)
		->withHeader('Allow', implode(', ', $methods))
		->withHeader('Content-type', 'text/html')
		->write('Papi, your method must be one of: ' . implode(', ', $methods));
	};
};

// 500 Error
$c['errorHandler'] = function ($c) {
	return function ($request, $response, $exception) use ($c) {
		return $c['response']->withStatus(500)
		->withHeader('Content-Type', 'text/html')
		->write('Oops Something went wrong Papi!');
	};
};


// -----------------------------------------------------------------------------
// Service factories
// -----------------------------------------------------------------------------

// Monolog
$container['logger'] = function ($c) {
    $settings = $c->get('settings');
    $logger = new \Monolog\Logger($settings['logger']['name']);
    $logger->pushProcessor(new \Monolog\Processor\UidProcessor());
    $logger->pushHandler(new \Monolog\Handler\StreamHandler($settings['logger']['path'], $settings['logger']['log_signal']));
    return $logger;
};

// -----------------------------------------------------------------------------
// Utilities services
// -----------------------------------------------------------------------------

// Cookies
$container['cookies'] = function ($c) {
	return new App\Utils\Cookies($c->get('logger'), $c->get('config'));
};

// Keys
$container['keys'] = function ($c) {
	return new App\Utils\Keys($c->get('logger'), $c->get('config'));
};

// Session
$container['client_session'] = function ($c) {
	return new App\Utils\ClientSession($c->get('logger'), $c->get('config'), $c->get('cookies'), $c->get('csrf'), $c->get('keys'));
};

// Mail Box
$container['mail_box'] = function ($c) {
	return new App\Utils\MailBox($c->get('logger'), $c->get('config'), $c->get('view')->getEnvironment(), $c->get('transport'));
};

// Product Images
$container['product_images'] = function ($c) {
	return new App\Utils\ProductImages($c->get('logger'));
};



/* Social Media */

// Facebook
$container['facebook'] = function ($c) {
	return new App\Utils\FaceBook($c->get('logger'), $c->get('cookies'));
};

// Google
$container['google'] = function ($c) {
	return new App\Utils\Google($c->get('logger'), $c->get('cookies'));
};

// Twitter
$container['twitter'] = function ($c) {
	return new App\Utils\Twitter($c->get('logger'), $c->get('cookies'), $c->get('twitteroauth'));
};

// Linkedin
$container['linkedin'] = function ($c) {
	return new App\Utils\LinkedIn($c->get('logger'), $c->get('cookies'));
};

// Twig View
$container['twig.extension'] = function ($c) {
	return new App\Utils\Twig($c->get('logger'), $c->get('client_session'), $c->get('product_images'));
};

$container['view'] = function ($c) {
	$settings = $c->get('settings');
	$view = new \Slim\Views\Twig($settings['view']['template_path'], $settings['view']['twig']);

	// Add extensions
	$view->addExtension(new Slim\Views\TwigExtension($c->get('router'), $c->get('request')->getUri()));
	$view->addExtension(new Twig_Extension_Debug());

	$view->addExtension($c->get('twig.extension'));

	return $view;
};

/* Middleware */

// Authenticate
$container['authenticate'] = function ($c) {
	return new  App\Middleware\Authenticate($c->get('logger'), $c->get('cookies'));
};

// Csrf Header
$container['csrf_header'] = function($c) {
	return new App\Middleware\CsrfResponseHeader($c->get('logger'), $c->get('cookies'));
};


// -----------------------------------------------------------------------------
// Action factories
// -----------------------------------------------------------------------------

$container['App\Action\MaintenanceAction'] = function ($c) {
	return new App\Action\MaintenanceAction($c->get('view'), $c->get('logger'));
};

$container['App\Action\ProductAction'] = function ($c) {
	return new App\Action\ProductAction($c->get('view'), $c->get('logger'), $c->get('keys'));
};

$container['App\Action\AdminAction'] = function ($c) {
	return new App\Action\AdminAction($c->get('view'), $c->get('logger'), $c->get('client_session'));
};

$container['App\Action\HomeAction'] = function ($c) {
	return new App\Action\HomeAction($c->get('view'), $c->get('logger'), $c->get('client_session'));
};

$container['App\Action\AccountAction'] = function ($c) {
	return new App\Action\AccountAction($c->get('view'), $c->get('logger'), $c->get('client_session'));
};

$container['App\Action\LandingAction'] = function ($c) {
	return new App\Action\LandingAction($c->get('view'), $c->get('logger'), $c->get('client_session'));
};

$container['App\Action\ReferencesAction'] = function ($c) {
	return new App\Action\ReferencesAction($c->get('view'), $c->get('logger'), $c->get('client_session'));
};

$container['App\Action\SearchAction'] = function ($c) {
	return new App\Action\SearchAction($c->get('view'), $c->get('logger'), $c->get('client_session'));
};

$container['App\Action\ShoppingCartAction'] = function ($c) {
	return new App\Action\ShoppingCartAction($c->get('view'), $c->get('logger'), $c->get('client_session'), $c->get('db'));
};

$container['App\Action\SocialMediaAction'] = function ($c) {
	return new App\Action\SocialMediaAction($c->get('logger'), $c->get('client_session'), $c->get('facebook'), $c->get('google'), $c->get('twitter'), $c->get('linkedin'));
};

$container['App\Action\UserAction'] = function ($c) {
	return new App\Action\UserAction($c->get('view'), $c->get('logger'), $c->get('client_session'), $c->get('mail_box'), $c->get('keys'), $c->get('db'));
};

$container['App\Controller\ConsoleController'] = function ($c) {
	return new App\Controller\ConsoleController($c->get('view'), $c->get('logger'));
};

