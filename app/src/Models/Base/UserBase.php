<?php
/**
 * UserBase Model
 * 
 * Provides data from "'user'"
 *
 * Author: Johnny Angel <johnnyangelnj@gmail.com>
 *
 */

namespace App\Models\Base;


use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class UserBase
 *
 * @package App\Models
 *
 * @SWG\Definition(
 *   definition="User",
 *  required={"id", "email", "username", "password", "first_name", "last_name", "profile_picture_url", "gender", "status", "remember_me", "accepted_terms", "email_validation"}
 * )
 *
 */

class UserBase extends Model
{
	use SoftDeletes;
	
    // turn on auto timestamps create_at and updated_at
    public $timestamps = true;

    // turn on softe delete_at
    protected $softDelete = true;
    
    // used to hide columns 
    protected $hidden = array();
    
    // used to append columns
    protected $appends = array();
    
    protected $table = 'user';
    
    protected $dates = ['deleted_at'];
    


/**
 *
 * @SWG\Property(property="id", type="integer", format="int32")
 * @SWG\Property(property="email", type="string")
 * @SWG\Property(property="username", type="string")
 * @SWG\Property(property="password", type="string")
 * @SWG\Property(property="first_name", type="string")
 * @SWG\Property(property="last_name", type="string")
 * @SWG\Property(property="profile_picture_url", type="string")
 * @SWG\Property(property="gender", type="string", description="values ['Male', 'Female'].")
 * @SWG\Property(property="status", type="string", description="values ['PENDING', 'APPROVED', 'DENIED'].")
 * @SWG\Property(property="remember_me", type="boolean", description="True or False")
 * @SWG\Property(property="accepted_terms", type="boolean", description="True or False")
 * @SWG\Property(property="email_validation", type="string", description="Format as YYYY-MM-DD HH:MI:SS")

 *
 */
     
     
    protected $fillable = array(
	'id',
	'email',
	'username',
	'password',
	'first_name',
	'last_name',
	'profile_picture_url',
	'gender',
	'status',
	'remember_me',
	'accepted_terms',
	'email_validation',
	'created_at',
	'updated_at',
	'deleted_at'
    );

    protected function getIdAttribute($v){ return (int)$v; }
    protected function getEmailAttribute($v){ return ($v)?$v:''; }
    protected function getUsernameAttribute($v){ return ($v)?$v:''; }
    protected function getPasswordAttribute($v){ return ($v)?$v:''; }
    protected function getFirstNameAttribute($v){ return ($v)?$v:''; }
    protected function getLastNameAttribute($v){ return ($v)?$v:''; }
    protected function getProfilePictureUrlAttribute($v){ return ($v)?$v:''; }
    protected function getRememberMeAttribute($v){ return (bool)$v; }
    protected function getAcceptedTermsAttribute($v){ return (bool)$v; }
    protected function getEmailValidationAttribute($v){ return $v; }
    protected function getCreatedAtAttribute($v){ return $v; }
    protected function getUpdatedAtAttribute($v){ return $v; }
    protected function getDeletedAtAttribute($v){ return $v; }

    

}
