<?php
/**
 *  SessionHasNotification Model
 * 
 * Provides data from "'session_has_notification'"
 *
 * Author: Johnny Angel <johnnyangelnj@gmail.com>
 * Generated on Thursday 14th of April 2016 03:17:21 PM
 *
 */
namespace App\Models;

use App\Models\Base\SessionHasNotificationBase;
 
class SessionHasNotification extends SessionHasNotificationBase
{
    // used to hide columns
    protected $hidden = array();
	
    // used to append columns
    protected $appends = array();

}
