<?php
/**
 * SearchTagsBase Model
 * 
 * Provides data from "'search_tags'"
 *
 * Author: Johnny Angel <johnnyangelnj@gmail.com>
 *
 */

namespace App\Models\Base;


use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class SearchTagsBase
 *
 * @package App\Models
 *
 * @SWG\Definition(
 *   definition="SearchTags",
 *  required={"id", "name"}
 * )
 *
 */

class SearchTagsBase extends Model
{
	use SoftDeletes;
	
    // turn on auto timestamps create_at and updated_at
    public $timestamps = true;

    // turn on softe delete_at
    protected $softDelete = true;
    
    // used to hide columns 
    protected $hidden = array();
    
    // used to append columns
    protected $appends = array();
    
    protected $table = 'search_tags';
    
    protected $dates = ['deleted_at'];
    


/**
 *
 * @SWG\Property(property="id", type="integer", format="int32")
 * @SWG\Property(property="name", type="string")

 *
 */
     
     
    protected $fillable = array(
	'id',
	'name',
	'created_at',
	'updated_at',
	'deleted_at'
    );

    protected function getIdAttribute($v){ return (int)$v; }
    protected function getNameAttribute($v){ return ($v)?$v:''; }
    protected function getCreatedAtAttribute($v){ return $v; }
    protected function getUpdatedAtAttribute($v){ return $v; }
    protected function getDeletedAtAttribute($v){ return $v; }

    

}
