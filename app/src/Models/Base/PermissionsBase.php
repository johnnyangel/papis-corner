<?php
/**
 * PermissionsBase Model
 * 
 * Provides data from "'permissions'"
 *
 * Author: Johnny Angel <johnnyangelnj@gmail.com>
 *
 */

namespace App\Models\Base;




/**
 * Class PermissionsBase
 *
 * @package App\Models
 *
 * @SWG\Definition(
 *   definition="Permissions",
 *  required={"id", "user_id", "permission"}
 * )
 *
 */

class PermissionsBase extends Model
{
	
	
    // turn on auto timestamps create_at and updated_at
    public $timestamps = false;

    // turn on softe delete_at
    protected $softDelete = false;
    
    // used to hide columns 
    protected $hidden = array();
    
    // used to append columns
    protected $appends = array();
    
    protected $table = 'permissions';
    
    
    


/**
 *
 * @SWG\Property(property="id", type="integer", format="int32")
 * @SWG\Property(property="user_id", type="integer", format="int32")
 * @SWG\Property(property="permission", type="string", description="values ['MASTER', 'SLAVE'].")

 *
 */
     
     
    protected $fillable = array(
	'id',
	'user_id',
	'permission'
    );

    protected function getIdAttribute($v){ return (int)$v; }
    protected function getUserIdAttribute($v){ return (int)$v; }

    

}
