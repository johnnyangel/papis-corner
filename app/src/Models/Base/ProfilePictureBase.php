<?php
/**
 * ProfilePictureBase Model
 * 
 * Provides data from "'profile_picture'"
 *
 * Author: Johnny Angel <johnnyangelnj@gmail.com>
 *
 */

namespace App\Models\Base;


use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class ProfilePictureBase
 *
 * @package App\Models
 *
 * @SWG\Definition(
 *   definition="ProfilePicture",
 *  required={"id", "user_id", "name", "type", "size", "active"}
 * )
 *
 */

class ProfilePictureBase extends Model
{
	use SoftDeletes;
	
    // turn on auto timestamps create_at and updated_at
    public $timestamps = true;

    // turn on softe delete_at
    protected $softDelete = true;
    
    // used to hide columns 
    protected $hidden = array();
    
    // used to append columns
    protected $appends = array();
    
    protected $table = 'profile_picture';
    
    protected $dates = ['deleted_at'];
    


/**
 *
 * @SWG\Property(property="id", type="integer", format="int32")
 * @SWG\Property(property="user_id", type="integer", format="int32")
 * @SWG\Property(property="name", type="string")
 * @SWG\Property(property="type", type="string")
 * @SWG\Property(property="size", type="integer", format="int32")
 * @SWG\Property(property="active", type="boolean", description="True or False")

 *
 */
     
     
    protected $fillable = array(
	'id',
	'user_id',
	'name',
	'type',
	'size',
	'active',
	'created_at',
	'updated_at',
	'deleted_at'
    );

    protected function getIdAttribute($v){ return (int)$v; }
    protected function getUserIdAttribute($v){ return (int)$v; }
    protected function getNameAttribute($v){ return ($v)?$v:''; }
    protected function getTypeAttribute($v){ return ($v)?$v:''; }
    protected function getSizeAttribute($v){ return (int)$v; }
    protected function getActiveAttribute($v){ return (bool)$v; }
    protected function getCreatedAtAttribute($v){ return $v; }
    protected function getUpdatedAtAttribute($v){ return $v; }
    protected function getDeletedAtAttribute($v){ return $v; }

    

}
