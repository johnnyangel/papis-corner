<?php
namespace App\Action;

use Slim\Views\Twig;
use Psr\Log\LoggerInterface;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

final class MaintenanceAction
{
    private $view;
    private $logger;

    public function __construct(Twig $view, LoggerInterface $logger)
    {
        $this->view = $view;
        $this->logger = $logger;
    }

    public function dispatch(Request $request, Response $response, $args)
    {
        $this->logger->debug("Maintenance Dispatched Action");
        
        $meta_data = array(	
        		"title" => "Coming Soon", 
        		"show_title_bar" => false
        );
        
        //$this->view->render($response, "maintenance.twig", $meta_data);
        $this->view->render($response, "maintenance-orbit-lg.twig", $meta_data);
        return $response;
    }
}
