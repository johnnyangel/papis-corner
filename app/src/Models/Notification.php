<?php
/**
 *  Notification Model
 * 
 * Provides data from "'notification'"
 *
 * Author: Johnny Angel <johnnyangelnj@gmail.com>
 * Generated on Thursday 14th of April 2016 03:17:21 PM
 *
 */
namespace App\Models;

use App\Models\Base\NotificationBase;
 
class Notification extends NotificationBase
{
    // used to hide columns
    protected $hidden = array();
	
    // used to append columns
    protected $appends = array();

}
