<?php

if (PHP_SAPI == 'cli-server') {
    // To help the built-in PHP dev server, check if the request was actually for
    // something which should probably be served as a static file
    $file = __DIR__ . $_SERVER['REQUEST_URI'];
    if (is_file($file)) {
        return false;
    }
}

require __DIR__ . '/../vendor/autoload.php';

session_start();

// Instantiate the app
$settings = require __DIR__ . '/../app/settings.php';
$app = new \Slim\App($settings);

// Set up dependencies
require __DIR__ . '/../app/dependencies.php';

// Register middleware
require __DIR__ . '/../app/middleware.php';

// Register services
require __DIR__ . '/../app/services.php';
		
// Register routes
require __DIR__ . '/../app/routes.php';

// -----------------------------------------------------------------------------
// Database bootstrap
// -----------------------------------------------------------------------------

// Bootstrap Eloquent ORM
$c = $app->getContainer();
$conn  = $c->get('db');
$resolver = new \Illuminate\Database\ConnectionResolver();
$resolver->addConnection('default', $conn);
$resolver->setDefaultConnection('default');
\Illuminate\Database\Eloquent\Model::setConnectionResolver($resolver);
/*
echo "Slim Container<br>" ;
print_r($app->getContainer());
*/
// Run!
$app->run();
