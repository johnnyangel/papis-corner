<?php
/**
 *  Permissions Model
 * 
 * Provides data from "'permissions'"
 *
 * Author: Johnny Angel <johnnyangelnj@gmail.com>
 * Generated on Sunday 31st of January 2016 05:26:38 AM
 *
 */
namespace App\Models;

use App\Models\Base\PermissionsBase;
 
class Permissions extends PermissionsBase
{
    // used to hide columns
    protected $hidden = array();
	
    // used to append columns
    protected $appends = array();
    
    public function user()
    {
    	return $this->belongsTo('\App\Models\User');
    }

}
