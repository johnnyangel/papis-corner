<?php
/**
 *  ShoppingCart Model
 * 
 * Provides data from "'shopping_cart'"
 *
 * Author: Johnny Angel <johnnyangelnj@gmail.com>
 * Generated on Sunday 17th of January 2016 08:26:54 PM
 *
 */
namespace App\Models;

use App\Models\Base\ShoppingCartBase;
 
class ShoppingCart extends ShoppingCartBase
{
    // used to hide columns
    protected $hidden = array();
	
    // used to append columns
    protected $appends = array();
    
    public function items()
    {
    	return $this->hasMany('\App\Models\ShoppingCartItems');
    }

}
