
function device_image(){
	console.log($( window ).width()+'x'+ $( window ).height());
	
	console.log($('.device-type').css("background-image"));
	$('.device-type').attr('title', ' Device '+$( window ).width()+'x'+$( window ).height()+' ');
	/*
	if($( window ).width() > 1200 && $( window ).width() < 1500){
		//$('.device-title').html(' Laptop '+$( window ).width()+'x'+$( window ).width()+' ');
		$('.device-type').css("background-image", "url(/css/devices/images/laptop.png)");
		$('.device-type').css("background-size", "35px 25px");
		$('.device-type').css("height", "35px");
		$('.device-type').css("width", "35px");
	}
	*/
		
}

function login($login_form){
	
	var url = '/user/authenticate';
	var payload = {};
	
	payload['email'] = $login_form.find( 'input[name="login-email"]' ).val();
	payload['password'] = $login_form.find( 'input[name="login-password"]' ).val();
	payload['rememberme'] = $login_form.find( 'input[name="login-rememberme"]:checked' ).val();
	
	// CSRF Token
	your_csrf_token = parent.csrf_token();
	for (var key in your_csrf_token) {
		payload[key] = your_csrf_token[key];
	}
	
	console.log(JSON.stringify(payload, null, 2));
	
	$.ajax( {
		type: 'POST',
		url: url,
		data: JSON.stringify(payload, null, 2),
		contentType: "application/json; charset=utf-8",
		dataType: "json",  // dataType of response is JSON
		success: function( response ) {
			$( '#login-message-container' ).show();
			$( '#login-message' ).addClass('success label message-box').html('Login Successful');
			console.log(response);
			location.href = response['url'];
		},
		error: function( response ) {
			$( '#login-message-container' ).show();
			$( '#login-message' ).addClass('warning label message-box').html(response.responseText);
			console.log(JSON.stringify(response, null, 2));
			setCsrfToken(0);
		}
		
	});
	return false;
}

function logout(user_id){
	
	var payload = {};

	// CSRF Token
	your_csrf_token = parent.csrf_token();
	for (var key in your_csrf_token) {
		payload[key] = your_csrf_token[key];
	}
	
	console.log(JSON.stringify(payload, null, 2));
	
	var social_media_logout = $( '#user_login_with_social_media' ).val();
	console.log('Any additional logouts? ' + social_media_logout);
	
	if(social_media_logout == 'Facebook'){
		facebook_logout();
	} else if(social_media_logout == 'Google'){
		google_logout();
	}

	jQuery.ajax({
        type: "DELETE",
        url: '/user/'+ user_id +'/logout',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        data: JSON.stringify(payload, null, 2),
        success: function( response ){
        	console.log(response);
        	location.href = response['url'];
        },
        error: function( response ){
        	console.log(response)
        }

	});

}

function reset_password($reset_password_form){
	
	var url = '/user/reset-password/';
	
	var email = $reset_password_form.find( 'input[name="email"]' ).val();
	
	var payload = {};

	// CSRF Token
	your_csrf_token = parent.csrf_token();
	for (var key in your_csrf_token) {
		payload[key] = your_csrf_token[key];
	}
	
	console.log(JSON.stringify(payload, null, 2));
	
	jQuery.ajax({
        type: "PUT",
        url: url + email,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        data: JSON.stringify(payload, null, 2),
        success: function( response ){
			$( '#password-reset-message-container' ).show();
			$( '#password-reset-message' ).addClass('success label message-box').html('Password Reset Successful');

        	console.log(response);
        	location.href = response['url'];
        },
        error: function( response ){
			$( '#password-reset-message-container' ).show();
			$( '#password-reset-message' ).addClass('warning label message-box').html(response.responseText);
			console.log(response);
        }

	});
	
	return true;
}


function addCartItem(product_id){
	
	var url = '/api/user/shopping-cart/item/'+product_id;
	
	var payload = {};
		
	// CSRF Token
	your_csrf_token = parent.csrf_token();
	for (var key in your_csrf_token) {
		payload[key] = your_csrf_token[key];
	}
	
	$.ajax( {
		type: 'POST',
		url: url,
		data: JSON.stringify(payload, null, 2),
		contentType: "application/json; charset=utf-8",
		dataType: "json",  // dataType of response is JSON
		success: function( response ) {
			//$( '#login-message-container' ).show();
			//$( '#login-message' ).addClass('success label message-box').html('Login Successful');
			console.log(JSON.stringify(response, null, 2));
			//location.href = response['url'];
			//setCsrfToken(0);
		},
		error: function( response ) {
			//$( '#login-message-container' ).show();
			//$( '#login-message' ).addClass('warning label message-box').html(response.responseText);
			console.log(JSON.stringify(response, null, 2));
			setCsrfToken(0);
		}
		
	});
	return false;	
	
	
}

function getMyOptions(url, select, display, active_option){
	
	$.get(url, function(data) {
	
		console.log(data);
		
		formatSelectOptions(data, select, display, active_option)

	});
}

function formatSelectOptions(data, select, display, active_option) {
	
	select.find('option').remove().end();
	
    var select_options = data;
    
    var found_logos = false;
    var temp_array = new Array();
    var selector_name = select.selector.substring(1,select.selector.length);
    
    select.get(0).options.add(new Option(display, ''));
    
    //console.log('Select Logo Array Name: ' + selector_name);
    
    for (loop=0; loop < select_options.length; loop++) {
    	option = select_options[loop];
    	key = option['key'];
    	value = option['value'];
    	
    	// Custom Lists
    	if(! key && ! value){
	    	key = option['abbreviation'];
	    	value = option['name'];
    	}
    	select.get(0).options.add(new Option(value, key));
    	
    	if( option['logo'] ){
    		found_logos = true;
    		temp_array[key] = option['logo'];
    	}
    }
    
    select.val(active_option);

    if( found_logos ) window.logo_array[selector_name] = temp_array;
    
}

function getDomain(url) {
	/*
	window.location.protocol;
    window.location.host;
    window.location.pathname;
    */
	
	url = url.replace(/https?:\/\/(www.)?/i, '');
	
	if (url.indexOf('/') === -1) {
	    console.log('IndexOf ' + url);
	    //return url;
	}

	console.log('Split ' + url);
	return url.split('/')[0];
} 

function setCsrfToken(user_id) {
    
   	$.get( "/user"+(user_id?"/"+user_id+"/":"/")+"csrf-token", function( response ) {
		console.log( response );
		$( '#csrf_key' ).val(response['csrf_key_name']);
		$( '#csrf_value' ).val(response['csrf_value_name']);
	 });
   	
}

function csrf_token () {              	
	key_name = $('#csrf_key').attr("name");
	key_name_value = $('#csrf_key').val();
	value_name = $('#csrf_value').attr("name");
	value_name_value = $('#csrf_value').val();
	
	var tokenArray = {};
    tokenArray[key_name] = key_name_value,
	tokenArray[value_name] = value_name_value
    
    console.log(JSON.stringify(tokenArray, null, 2));
    
    return tokenArray;
}