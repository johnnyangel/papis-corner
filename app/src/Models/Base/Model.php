<?php
/**
 * Model
 * 
 * Implements base common functionality for all models.
 */

namespace App\Models\Base;

use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\Exception\UnsatisfiedDependencyException;
use Illuminate\Database\Eloquent\Model as EloquentModel;


class Model extends EloquentModel
{
    // default "id" to be guarded
    protected $guarded = array('id');
    
    // list of related models to create
    // array( <relAttr> => <relation> )
    protected static $cascadeOnCreate = array();
    
    // list of related models to delete
    protected $cascadeOnDelete = array();
    
    // manually cascade creation on each related item
    /*
    public static function create(Array $attributes)
    {
        $relations = static::$cascadeOnCreate;
        
        // filter for actual attributes and create model
        $modelAttrs = array_diff_key($attributes, $relations);
        $model = parent::create($modelAttrs);
        
        // process the related models
        foreach ($relations as $relAttr => $relation){
            
            // skip if attributes are not present
            if (! isset($attributes[$relAttr])) continue;
            
            // create the related model
            $model->createCascade($relation, $attributes[$relAttr]);
        }
        
        return $model;
    }*/ 
    
    public function createCascade($relationship, $attrs)
    {
        // start with the model relationship
        if (is_string($relationship)){
            $relationship = $this->{$relationship}();
        }
        
        // determine the related model
        $RelatedModelClass = $relationship->getRelated();
        
        // check for single set of model attributes
        if (!empty($attrs) && array_keys($attrs) !== range(0, count($attrs) - 1)){
            $attrs = array($attrs); // wrap into array
        }
        
        // create the related models
        foreach ($attrs as $attr){
            $attr[$relationship->getPlainForeignKey()] = $relationship->getParentKey();
            $RelatedModelClass::create($attr);
        }
    }
    
    // manually cascade deletion on each related item
    public function delete()
    {
        foreach ($this->cascadeOnDelete as $rel){
            $this->{$rel}()->get()->transform(function($item){
                return $item->delete();
            });
        }
        
        return parent::delete();
    }   

    /**
     * generateGUID
     */
	public static function generateGUID()
	{
		$guid = null;
		
		try {
			$guid = Uuid::uuid4();
		} catch (UnsatisfiedDependencyException $e) {
			throw $e;
		}
		
		return $guid;
	}
    
    /**
     * packGUID
     */
    public static function packGUID($guid){
    	
    	if( ctype_xdigit(str_replace ('-', '', $guid)))
    		return pack("h*", str_replace('-', '', $guid));
    	else 
    		return null;
    }
    
    /**
     * unpackGUID
     */
    public static function unpackGUID($guid){
    	$unpacked = unpack("h*", $guid);
    	return implode('',preg_replace("/([0-9a-f]{8})([0-9a-f]{4})([0-9a-f]{4})([0-9a-f]{4})([0-9a-f]{12})/", "$1-$2-$3-$4-$5", $unpacked));
    }
}
