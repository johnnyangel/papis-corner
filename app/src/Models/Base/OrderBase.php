<?php
/**
 * OrderBase Model
 * 
 * Provides data from "'order'"
 *
 * Author: Johnny Angel <johnnyangelnj@gmail.com>
 *
 */

namespace App\Models\Base;


use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class OrderBase
 *
 * @package App\Models
 *
 * @SWG\Definition(
 *   definition="Order",
 *  required={"id", "guid", "session_id", "account_id", "service_address_id", "billing_address_id", "shipping_method", "coupon_code", "sales_tax", "discount_amount", "total_amount", "payment_method_id", "payment_amount", "fulfilment_date"}
 * )
 *
 */

class OrderBase extends Model
{
	use SoftDeletes;
	
    // turn on auto timestamps create_at and updated_at
    public $timestamps = true;

    // turn on softe delete_at
    protected $softDelete = true;
    
    // used to hide columns 
    protected $hidden = array();
    
    // used to append columns
    protected $appends = array();
    
    protected $table = 'order';
    
    protected $dates = ['deleted_at'];
    


/**
 *
 * @SWG\Property(property="id", type="integer", format="int32")
 * @SWG\Property(property="guid", type="string")
 * @SWG\Property(property="session_id", type="integer", format="int32")
 * @SWG\Property(property="account_id", type="integer", format="int32")
 * @SWG\Property(property="service_address_id", type="integer", format="int32")
 * @SWG\Property(property="billing_address_id", type="integer", format="int32")
 * @SWG\Property(property="shipping_method", type="string", description="values ['US-MAIL', 'FED-EX', 'UPS'].")
 * @SWG\Property(property="coupon_code", type="string")
 * @SWG\Property(property="sales_tax", type="string", description="Two decimal value, 0.00")
 * @SWG\Property(property="discount_amount", type="string", description="Two decimal value, 0.00")
 * @SWG\Property(property="total_amount", type="string", description="Two decimal value, 0.00")
 * @SWG\Property(property="payment_method_id", type="integer", format="int32")
 * @SWG\Property(property="payment_amount", type="string", description="Two decimal value, 0.00")
 * @SWG\Property(property="fulfilment_date", type="string", description="Format as YYYY-MM-DD HH:MI:SS")

 *
 */
     
     
    protected $fillable = array(
	'id',
	'guid',
	'session_id',
	'account_id',
	'service_address_id',
	'billing_address_id',
	'shipping_method',
	'coupon_code',
	'sales_tax',
	'discount_amount',
	'total_amount',
	'payment_method_id',
	'payment_amount',
	'fulfilment_date',
	'created_at',
	'updated_at',
	'deleted_at'
    );

    protected function getIdAttribute($v){ return (int)$v; }
    protected function getGuidAttribute($v){ return self::unpackGUID($v); }
    protected function getSessionIdAttribute($v){ return (int)$v; }
    protected function getAccountIdAttribute($v){ return (int)$v; }
    protected function getServiceAddressIdAttribute($v){ return (int)$v; }
    protected function getBillingAddressIdAttribute($v){ return (int)$v; }
    protected function getCouponCodeAttribute($v){ return ($v)?$v:''; }
    protected function getPaymentMethodIdAttribute($v){ return (int)$v; }
    protected function getFulfilmentDateAttribute($v){ return $v; }
    protected function getCreatedAtAttribute($v){ return $v; }
    protected function getUpdatedAtAttribute($v){ return $v; }
    protected function getDeletedAtAttribute($v){ return $v; }

    protected function setGuidAttribute($v){ $this->attributes['guid'] = self::packGUID($v); }

}
