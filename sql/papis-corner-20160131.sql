-- MySQL dump 10.13  Distrib 5.7.9, for linux-glibc2.5 (x86_64)
--
-- Host: localhost    Database: papis-corner
-- ------------------------------------------------------
-- Server version	5.6.27

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `address`
--

DROP TABLE IF EXISTS `address`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `address` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `order_guid` varbinary(18) DEFAULT NULL,
  `type` enum('Billing','Shipping') DEFAULT NULL,
  `address_1` varchar(128) DEFAULT NULL,
  `address_2` varchar(128) DEFAULT NULL,
  `city` varchar(128) DEFAULT NULL,
  `postal_code` varchar(10) DEFAULT NULL,
  `lat` decimal(10,8) DEFAULT NULL,
  `long` decimal(11,8) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_address_user1_idx` (`user_id`),
  KEY `fk_address_order1_idx` (`order_guid`),
  CONSTRAINT `fk_address_order1` FOREIGN KEY (`order_guid`) REFERENCES `order` (`guid`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_address_user1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `address`
--

LOCK TABLES `address` WRITE;
/*!40000 ALTER TABLE `address` DISABLE KEYS */;
/*!40000 ALTER TABLE `address` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `catalog`
--

DROP TABLE IF EXISTS `catalog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `catalog` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(150) DEFAULT NULL,
  `discription` varchar(500) DEFAULT NULL,
  `path` varchar(250) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `catalog`
--

LOCK TABLES `catalog` WRITE;
/*!40000 ALTER TABLE `catalog` DISABLE KEYS */;
INSERT INTO `catalog` VALUES (1,'The Naomi Cordero Collection','Home furnishing art work for all. Canvas, pencil portriats that will brighten and add texture to your personal space.','/media/catalog/naomi-cordero','2016-01-17 14:44:56','2016-01-17 14:44:56',NULL);
/*!40000 ALTER TABLE `catalog` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order`
--

DROP TABLE IF EXISTS `order`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `order` (
  `guid` varbinary(18) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`guid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order`
--

LOCK TABLES `order` WRITE;
/*!40000 ALTER TABLE `order` DISABLE KEYS */;
/*!40000 ALTER TABLE `order` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order_items`
--

DROP TABLE IF EXISTS `order_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `order_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_guid` varbinary(18) NOT NULL,
  `quantity` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`,`order_guid`),
  KEY `fk_order_item_order1_idx` (`order_guid`),
  CONSTRAINT `fk_order_item_order1` FOREIGN KEY (`order_guid`) REFERENCES `order` (`guid`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order_items`
--

LOCK TABLES `order_items` WRITE;
/*!40000 ALTER TABLE `order_items` DISABLE KEYS */;
/*!40000 ALTER TABLE `order_items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permissions`
--

DROP TABLE IF EXISTS `permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `permission` enum('MASTER','SLAVE') DEFAULT 'SLAVE',
  PRIMARY KEY (`id`,`user_id`),
  KEY `fk_permissions_user1_idx` (`user_id`),
  CONSTRAINT `fk_permissions_user1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permissions`
--

LOCK TABLES `permissions` WRITE;
/*!40000 ALTER TABLE `permissions` DISABLE KEYS */;
INSERT INTO `permissions` VALUES (1,1,'MASTER'),(2,2,'SLAVE'),(3,1,'SLAVE');
/*!40000 ALTER TABLE `permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product`
--

DROP TABLE IF EXISTS `product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `guid` varbinary(18) NOT NULL,
  `catalog_id` int(11) NOT NULL,
  `status` enum('PENDING','AVAILABLE','ON HOLD','SOLD OUT') DEFAULT 'PENDING',
  `name` varchar(150) DEFAULT NULL,
  `caption` varchar(200) DEFAULT NULL,
  `description` varchar(500) DEFAULT NULL,
  `path` varchar(250) DEFAULT NULL,
  `price` decimal(10,2) DEFAULT '50.00',
  `provided_by` varchar(45) DEFAULT NULL,
  `sort_order` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`,`guid`,`catalog_id`),
  KEY `fk_product_catalog1_idx` (`catalog_id`),
  CONSTRAINT `fk_product_catalog1` FOREIGN KEY (`catalog_id`) REFERENCES `catalog` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product`
--

LOCK TABLES `product` WRITE;
/*!40000 ALTER TABLE `product` DISABLE KEYS */;
INSERT INTO `product` VALUES (1,'�\�0���[\�	5\��\n',1,'AVAILABLE','Pottery and Fruit','Canvas oil painting of clay pitcher and bowl of fruit.',NULL,'/media/catalog/naomi-cordero/products/art-work/images',50.00,'Naomi Cordero',NULL,'2016-01-18 02:47:41','2016-01-18 02:47:41',NULL),(2,'�u�\0-��\�8\�\n�-\�@',1,'AVAILABLE','Lighthouse on the Coastal Waterway','Canvas oil painting of lighthouse on a sunny day.',NULL,'/media/catalog/naomi-cordero/products/art-work/images',50.00,'Naomi Cordero',NULL,'2016-01-18 04:52:21','2016-01-18 04:52:21',NULL),(3,'��\�\�z�T&	\�\�]mA\�q',1,'AVAILABLE','Birds on a Branch','Pencil drawing of two tropical birds on a branch.',NULL,'/media/catalog/naomi-cordero/products/art-work/images',50.00,'Naomi Cordero',NULL,'2016-01-18 05:10:23','2016-01-18 05:10:23',NULL),(4,'�\�5a5h�1Z�br�ލ',1,'AVAILABLE','Bird on a Limb','Pencil drawing of a wild turkey perched on a branch.',NULL,'/media/catalog/naomi-cordero/products/art-work/images',50.00,'Naomi Cordero',NULL,'2016-01-18 09:51:18','2016-01-18 09:51:18',NULL),(5,'XP+\�C;S\�+R?\�:',1,'AVAILABLE','Bird in a Bush','Pencil drawing of black and yellow bird in a flowering bush.',NULL,'/media/catalog/naomi-cordero/products/art-work/images',50.00,'Naomi Cordero',NULL,'2016-01-18 10:01:17','2016-01-18 10:01:17',NULL),(6,'S��|\���\�@�?�_�',1,'AVAILABLE','Birds in Berry Branchs','Pencil drawing of a flock of birds on a berry branchs.',NULL,'/media/catalog/naomi-cordero/products/art-work/images',50.00,'Naomi Cordero',NULL,'2016-01-18 10:08:15','2016-01-18 10:08:15',NULL),(7,'J\�\0�E�\��|�\\wc',1,'PENDING',NULL,NULL,NULL,NULL,50.00,NULL,NULL,'2016-01-24 02:46:54','2016-01-24 02:46:54',NULL),(8,'L\�\�(�4kK��+$&',1,'PENDING',NULL,NULL,NULL,NULL,50.00,NULL,NULL,'2016-01-30 23:02:59','2016-01-30 23:02:59',NULL),(9,'b���\nWD�V�\�',1,'PENDING',NULL,NULL,NULL,NULL,50.00,NULL,NULL,'2016-01-30 23:40:23','2016-01-30 23:40:23',NULL),(10,'4/L+�M\�9\n^\�\��\�',1,'PENDING',NULL,NULL,NULL,NULL,50.00,NULL,NULL,'2016-01-30 23:49:11','2016-01-30 23:49:11',NULL),(11,'\��Dyg\�;\�Cz-�',1,'PENDING',NULL,NULL,NULL,NULL,50.00,NULL,NULL,'2016-01-31 00:00:34','2016-01-31 00:00:34',NULL),(12,'\�f�4���\�ct#i�<',1,'PENDING',NULL,NULL,NULL,NULL,50.00,NULL,NULL,'2016-01-31 00:01:02','2016-01-31 00:01:02',NULL),(13,'\�\'8�\�R�^>輋�r',1,'PENDING',NULL,NULL,NULL,NULL,50.00,NULL,NULL,'2016-01-31 00:01:18','2016-01-31 00:01:18',NULL),(14,'S\�g\��[\�}.��dvs',1,'PENDING',NULL,NULL,NULL,NULL,50.00,NULL,NULL,'2016-01-31 00:20:19','2016-01-31 00:20:19',NULL),(15,'���I{s}\r\�',1,'PENDING',NULL,NULL,NULL,NULL,50.00,NULL,NULL,'2016-01-31 00:20:39','2016-01-31 00:20:39',NULL);
/*!40000 ALTER TABLE `product` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product_has_search_tags`
--

DROP TABLE IF EXISTS `product_has_search_tags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_has_search_tags` (
  `product_id` int(11) NOT NULL,
  `product_guid` varbinary(18) NOT NULL,
  `product_catalog_id` int(11) NOT NULL,
  `search_tags_id` int(11) NOT NULL,
  PRIMARY KEY (`product_id`,`product_guid`,`product_catalog_id`,`search_tags_id`),
  KEY `fk_product_has_search_tags_search_tags1_idx` (`search_tags_id`),
  KEY `fk_product_has_search_tags_product1_idx` (`product_id`,`product_guid`,`product_catalog_id`),
  CONSTRAINT `fk_product_has_search_tags_product1` FOREIGN KEY (`product_id`, `product_guid`, `product_catalog_id`) REFERENCES `product` (`id`, `guid`, `catalog_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_product_has_search_tags_search_tags1` FOREIGN KEY (`search_tags_id`) REFERENCES `search_tags` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_has_search_tags`
--

LOCK TABLES `product_has_search_tags` WRITE;
/*!40000 ALTER TABLE `product_has_search_tags` DISABLE KEYS */;
/*!40000 ALTER TABLE `product_has_search_tags` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product_image`
--

DROP TABLE IF EXISTS `product_image`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_image` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `product_guid` varbinary(18) NOT NULL,
  `type` enum('LARGE','MEDIUM','SMALL','TINY') DEFAULT NULL,
  `filename` varchar(250) DEFAULT NULL,
  `backdrop_style` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`,`product_id`,`product_guid`),
  KEY `fk_product_image_product1_idx` (`product_id`,`product_guid`),
  CONSTRAINT `fk_product_image_product1` FOREIGN KEY (`product_id`, `product_guid`) REFERENCES `product` (`id`, `guid`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=57 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_image`
--

LOCK TABLES `product_image` WRITE;
/*!40000 ALTER TABLE `product_image` DISABLE KEYS */;
INSERT INTO `product_image` VALUES (1,1,'�\�0���[\�	5\��\n','LARGE','pottery-and-fruit-naomi-cordero-1024x778.png','color-pastel-red'),(2,1,'�\�0���[\�	5\��\n','MEDIUM','pottery-and-fruit-naomi-cordero-870x400.png','color-pastel-red'),(3,1,'�\�0���[\�	5\��\n','SMALL','pottery-and-fruit-naomi-cordero-300x400.png','color-pastel-red'),(4,2,'�u�\0-��\�8\�\n�-\�@','LARGE','lighthouse-redroof-house-naomi-cordero-1024x795.png','color-pastel-dark-red'),(5,2,'�u�\0-��\�8\�\n�-\�@','MEDIUM','lighthouse-redroof-house-naomi-cordero-870x400.png','color-pastel-dark-red'),(6,2,'�u�\0-��\�8\�\n�-\�@','SMALL','lighthouse-redroof-house-naomi-cordero-300x400.png','color-pastel-dark-red'),(7,3,'��\�\�z�T&	\�\�]mA\�q','LARGE','birds-on-branch-naomi-cordero-1024x1324.png','color-pastel-brown'),(8,3,'��\�\�z�T&	\�\�]mA\�q','MEDIUM','birds-on-branch-naomi-cordero-870x400.png','color-pastel-brown'),(9,3,'��\�\�z�T&	\�\�]mA\�q','SMALL','birds-on-branch-naomi-cordero-300x400.png','color-pastel-brown'),(10,4,'�\�5a5h�1Z�br�ލ','LARGE','bird-on-a-limb-naomi-cordero-1024x1326.png','color-pastel-orange'),(11,4,'�\�5a5h�1Z�br�ލ','MEDIUM','bird-on-a-limb-naomi-cordero-870x400.png','color-pastel-orange'),(12,4,'�\�5a5h�1Z�br�ލ','SMALL','bird-on-a-limb-naomi-cordero-300x400.png','color-pastel-orange'),(13,5,'XP+\�C;S\�+R?\�:','LARGE','bird-in-a-bush-naomi-cordero-1024x1326.png','color-pastel-blue'),(14,5,'XP+\�C;S\�+R?\�:','MEDIUM','bird-in-a-bush-naomi-cordero-870x400.png','color-pastel-blue'),(15,5,'XP+\�C;S\�+R?\�:','SMALL','bird-in-a-bush-naomi-cordero-300x400.png','color-pastel-blue'),(16,6,'S��|\���\�@�?�_�','LARGE','birds-in-berry-branches-naomi-cordero-1024x1366.png','color-pastel-yellow'),(17,6,'S��|\���\�@�?�_�','MEDIUM','birds-in-berry-branches-naomi-cordero-870x400.png','color-pastel-yellow'),(18,6,'S��|\���\�@�?�_�','SMALL','birds-in-berry-branches-naomi-cordero-300x400.png','color-pastel-yellow'),(19,1,'�\�0���[\�	5\��\n','TINY','pottery-and-fruit-naomi-cordero-100x100.png','color-pastel-red'),(20,2,'�u�\0-��\�8\�\n�-\�@','TINY','lighthouse-redroof-house-naomi-cordero-100x100.png','color-pastel-dark-red'),(21,3,'��\�\�z�T&	\�\�]mA\�q','TINY','birds-on-branch-naomi-cordero-100x100.png','color-pastel-brown'),(22,4,'�\�5a5h�1Z�br�ލ','TINY','bird-on-a-limb-naomi-cordero-100x100.png','color-pastel-orange'),(23,5,'XP+\�C;S\�+R?\�:','TINY','bird-in-a-bush-naomi-cordero-100x100.png','color-pastel-blue'),(24,6,'S��|\���\�@�?�_�','TINY','birds-in-berry-branches-naomi-cordero-100x100.png','color-pastel-yellow'),(25,8,'L\�\�(�4kK��+$&','LARGE',NULL,NULL),(26,8,'L\�\�(�4kK��+$&','MEDIUM',NULL,NULL),(27,8,'L\�\�(�4kK��+$&','SMALL',NULL,NULL),(28,8,'L\�\�(�4kK��+$&','TINY',NULL,NULL),(29,9,'b���\nWD�V�\�','LARGE',NULL,NULL),(30,9,'b���\nWD�V�\�','MEDIUM',NULL,NULL),(31,9,'b���\nWD�V�\�','SMALL',NULL,NULL),(32,9,'b���\nWD�V�\�','TINY',NULL,NULL),(33,10,'4/L+�M\�9\n^\�\��\�','LARGE',NULL,NULL),(34,10,'4/L+�M\�9\n^\�\��\�','MEDIUM',NULL,NULL),(35,10,'4/L+�M\�9\n^\�\��\�','SMALL',NULL,NULL),(36,10,'4/L+�M\�9\n^\�\��\�','TINY',NULL,NULL),(37,11,'\��Dyg\�;\�Cz-�','LARGE',NULL,NULL),(38,11,'\��Dyg\�;\�Cz-�','MEDIUM',NULL,NULL),(39,11,'\��Dyg\�;\�Cz-�','SMALL',NULL,NULL),(40,11,'\��Dyg\�;\�Cz-�','TINY',NULL,NULL),(41,12,'\�f�4���\�ct#i�<','LARGE',NULL,NULL),(42,12,'\�f�4���\�ct#i�<','MEDIUM',NULL,NULL),(43,12,'\�f�4���\�ct#i�<','SMALL',NULL,NULL),(44,12,'\�f�4���\�ct#i�<','TINY',NULL,NULL),(45,13,'\�\'8�\�R�^>輋�r','LARGE',NULL,NULL),(46,13,'\�\'8�\�R�^>輋�r','MEDIUM',NULL,NULL),(47,13,'\�\'8�\�R�^>輋�r','SMALL',NULL,NULL),(48,13,'\�\'8�\�R�^>輋�r','TINY',NULL,NULL),(49,14,'S\�g\��[\�}.��dvs','LARGE',NULL,NULL),(50,14,'S\�g\��[\�}.��dvs','MEDIUM',NULL,NULL),(51,14,'S\�g\��[\�}.��dvs','SMALL',NULL,NULL),(52,14,'S\�g\��[\�}.��dvs','TINY',NULL,NULL),(53,15,'���I{s}\r\�','LARGE',NULL,NULL),(54,15,'���I{s}\r\�','MEDIUM',NULL,NULL),(55,15,'���I{s}\r\�','SMALL',NULL,NULL),(56,15,'���I{s}\r\�','TINY',NULL,NULL);
/*!40000 ALTER TABLE `product_image` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `search_tags`
--

DROP TABLE IF EXISTS `search_tags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `search_tags` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `search_tags`
--

LOCK TABLES `search_tags` WRITE;
/*!40000 ALTER TABLE `search_tags` DISABLE KEYS */;
/*!40000 ALTER TABLE `search_tags` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `session`
--

DROP TABLE IF EXISTS `session`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `session` (
  `guid` varbinary(18) NOT NULL,
  `client_ipa` varchar(45) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `shopping_cart_guid` varbinary(18) DEFAULT NULL,
  `order_guid` varbinary(18) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`guid`),
  KEY `fk_session_user_idx` (`user_id`),
  KEY `fk_session_shopping_cart1_idx` (`shopping_cart_guid`),
  KEY `fk_session_order1_idx` (`order_guid`),
  CONSTRAINT `fk_session_order1` FOREIGN KEY (`order_guid`) REFERENCES `order` (`guid`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_session_shopping_cart1` FOREIGN KEY (`shopping_cart_guid`) REFERENCES `shopping_cart` (`guid`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_session_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `session`
--

LOCK TABLES `session` WRITE;
/*!40000 ALTER TABLE `session` DISABLE KEYS */;
INSERT INTO `session` VALUES ('	��\�\0��LHuW%�i','127.0.0.1',NULL,NULL,NULL,'2016-01-31 07:59:00','2016-01-31 07:59:00',NULL),('Ha�l�Ԧ+N/\�\�z�','127.0.0.1',NULL,NULL,NULL,'2016-01-31 09:32:33','2016-01-31 09:32:33',NULL),('~�@J�DG[�\�bl\�\�','127.0.0.1',NULL,NULL,NULL,'2016-01-31 08:24:56','2016-01-31 08:24:56',NULL),('\��\�\�2Ȓ^\�\��Y�','127.0.0.1',NULL,NULL,NULL,'2016-01-31 02:24:05','2016-01-31 02:24:05',NULL),('~\�jƨTK�O3�P]�\�','127.0.0.1',NULL,NULL,NULL,'2016-01-31 07:30:09','2016-01-31 07:30:09',NULL),('\"Qt&�Z\�oy�[y\�f�`','127.0.0.1',NULL,NULL,NULL,'2016-01-31 10:37:33','2016-01-31 10:37:33',NULL),('&;�o�0D�J��\n�\�\�','127.0.0.1',NULL,NULL,NULL,'2016-01-24 08:37:17','2016-01-24 08:37:17',NULL),('PSY�\�Zd\�\�mw\�\���\�','127.0.0.1',NULL,NULL,NULL,'2016-01-29 09:09:59','2016-01-29 09:09:59',NULL),('V+]�\������\�4��','127.0.0.1',NULL,NULL,NULL,'2016-01-31 09:19:32','2016-01-31 09:19:32',NULL),('X0�����\�o�c)�{\�','127.0.0.1',NULL,NULL,NULL,'2016-01-28 08:47:44','2016-01-28 08:47:44',NULL),('}/�C�q�:s3@\�\�','127.0.0.1',NULL,NULL,NULL,'2016-01-31 09:09:14','2016-01-31 09:09:14',NULL),('�J\�/At\�\n\�\��h\�\�','127.0.0.1',NULL,NULL,NULL,'2016-01-31 08:41:23','2016-01-31 08:41:23',NULL),('�\�l&VTg)Xw�=\�f�','127.0.0.1',NULL,NULL,NULL,'2016-01-29 08:59:11','2016-01-29 08:59:11',NULL),('�\�@�C�AY�++Bv\�','127.0.0.1',NULL,NULL,NULL,'2016-01-31 08:55:47','2016-01-31 08:55:47',NULL),('����ot�[ty���\�','127.0.0.1',NULL,NULL,NULL,'2016-01-31 09:23:39','2016-01-31 09:23:39',NULL),('��}P\��庪(\�7��','127.0.0.1',NULL,NULL,NULL,'2016-01-31 02:50:44','2016-01-31 02:50:44',NULL),('��<udG+T\�\�\�\�','127.0.0.1',NULL,NULL,NULL,'2016-01-31 09:07:35','2016-01-31 09:07:35',NULL),('�>v>�-B\�Y\�0^l;\�','127.0.0.1',NULL,NULL,NULL,'2016-01-26 20:11:27','2016-01-26 20:11:27',NULL),('��\�y\�\�	�+G\�o','127.0.0.1',NULL,NULL,NULL,'2016-01-31 10:50:09','2016-01-31 10:50:09',NULL),('�y+���\�\�\�ֆ��DR','127.0.0.1',NULL,NULL,NULL,'2016-01-31 08:44:36','2016-01-31 08:44:36',NULL),('�{>:4��Z�2��','127.0.0.1',NULL,NULL,NULL,'2016-01-17 21:11:22','2016-01-17 21:11:22',NULL),('�R4\�\�DD\�q=r�c��','127.0.0.1',NULL,NULL,NULL,'2016-01-18 03:56:49','2016-01-18 03:56:49',NULL),('��Խ?\�Ԣ�V60E0','127.0.0.1',NULL,NULL,NULL,'2016-01-18 02:37:36','2016-01-18 02:37:36',NULL),('ϋ�8T�\�]�{�\�','127.0.0.1',NULL,NULL,NULL,'2016-01-31 09:10:39','2016-01-31 09:10:39',NULL),('µ\�l��T�i\�\\��\�|','127.0.0.1',NULL,NULL,NULL,'2016-01-24 18:28:51','2016-01-24 18:28:51',NULL),('\�+;z\�0\�S\�sR�','127.0.0.1',NULL,NULL,NULL,'2016-01-31 08:45:56','2016-01-31 08:45:56',NULL),('\�i�Q!\�)|zE\�\�','127.0.0.1',NULL,NULL,NULL,'2016-01-31 02:52:05','2016-01-31 02:52:05',NULL),('\�\��\�\�\�a��','127.0.0.1',NULL,NULL,NULL,'2016-01-31 08:38:31','2016-01-31 08:38:31',NULL),('\��d]�O�y\�\"�Qb\'','127.0.0.1',NULL,NULL,NULL,'2016-01-30 00:30:09','2016-01-30 00:30:09',NULL),('\�\��\��\�[8�ՠ\\�','127.0.0.1',NULL,NULL,NULL,'2016-01-31 09:00:14','2016-01-31 09:00:14',NULL),('尸d�MD\�FN\�e','127.0.0.1',NULL,NULL,NULL,'2016-01-29 08:59:11','2016-01-29 08:59:11',NULL),('�\�w��4�Y�-��!','127.0.0.1',NULL,NULL,NULL,'2016-01-26 07:07:51','2016-01-26 07:07:51',NULL);
/*!40000 ALTER TABLE `session` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shopping_cart`
--

DROP TABLE IF EXISTS `shopping_cart`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shopping_cart` (
  `guid` varbinary(18) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`guid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shopping_cart`
--

LOCK TABLES `shopping_cart` WRITE;
/*!40000 ALTER TABLE `shopping_cart` DISABLE KEYS */;
/*!40000 ALTER TABLE `shopping_cart` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shopping_cart_items`
--

DROP TABLE IF EXISTS `shopping_cart_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shopping_cart_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `shopping_cart_guid` varbinary(18) NOT NULL,
  `quantity` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`,`shopping_cart_guid`),
  KEY `fk_shopping_cart_items_shopping_cart1_idx` (`shopping_cart_guid`),
  CONSTRAINT `fk_shopping_cart_items_shopping_cart1` FOREIGN KEY (`shopping_cart_guid`) REFERENCES `shopping_cart` (`guid`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shopping_cart_items`
--

LOCK TABLES `shopping_cart_items` WRITE;
/*!40000 ALTER TABLE `shopping_cart_items` DISABLE KEYS */;
/*!40000 ALTER TABLE `shopping_cart_items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(45) NOT NULL,
  `password` varchar(45) DEFAULT NULL,
  `first_name` varchar(45) DEFAULT NULL,
  `last_name` varchar(45) DEFAULT NULL,
  `status` enum('PENDING','APPROVED','DENIED') DEFAULT 'PENDING',
  `remember_me` tinyint(1) DEFAULT '0',
  `accepted_terms` tinyint(1) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`,`email`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  UNIQUE KEY `email_UNIQUE` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'johnnyangelnj@gmail.com','Papi1!','John','Cordero','APPROVED',1,1,'2016-01-01 05:00:00','2016-01-01 05:00:00',NULL),(2,'naocor59@msn.com','Mommy1!','Naomi','Cordero','APPROVED',1,1,'2016-01-31 05:00:00','2016-01-31 05:00:00',NULL);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-01-31 10:30:56
