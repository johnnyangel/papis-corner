<?php
/**
 *  Account Model
 * 
 * Provides data from "'account'"
 *
 * Author: Johnny Angel <johnnyangelnj@gmail.com>
 * Generated on Saturday 5th of March 2016 01:40:33 AM
 *
 */
namespace App\Models;

use App\Models\Base\AccountBase;
 
class Account extends AccountBase
{
    // used to hide columns
    protected $hidden = array();
	
    // used to append columns
    protected $appends = array();
    
    public function owner()
    {
    	return $this->hasOne('\App\Modles\User');
    }
    
    public function address_book()
    {
    	return $this->hasMany('\App\Models\Address');
    }

    public function payment_method()
    {
    	return $this->hasMany('\App\Models\PaymentMethod');
    }
}
