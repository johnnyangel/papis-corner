<?php
/**
 *  SocialMedia Model
 * 
 * Provides data from "'social_media'"
 *
 * Author: Johnny Angel <johnnyangelnj@gmail.com>
 * Generated on Sunday 14th of February 2016 12:43:33 AM
 *
 */
namespace App\Models;

use App\Models\Base\SocialMediaBase;
 
class SocialMedia extends SocialMediaBase
{
    // used to hide columns
    protected $hidden = array();
	
    // used to append columns
    protected $appends = array();
    
    public function user()
    {
    	return $this->belongsTo('\App\Models\User');
    }

}
