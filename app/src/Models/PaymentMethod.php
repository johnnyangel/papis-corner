<?php
/**
 *  PaymentMethod Model
 * 
 * Provides data from "'payment_method'"
 *
 * Author: Johnny Angel <johnnyangelnj@gmail.com>
 * Generated on Saturday 5th of March 2016 01:40:33 AM
 *
 */
namespace App\Models;

use App\Models\Base\PaymentMethodBase;
 
class PaymentMethod extends PaymentMethodBase
{
    // used to hide columns
    protected $hidden = array();
	
    // used to append columns
    protected $appends = array();

}
