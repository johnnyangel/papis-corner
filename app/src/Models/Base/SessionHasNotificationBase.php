<?php
/**
 * SessionHasNotificationBase Model
 * 
 * Provides data from "'session_has_notification'"
 *
 * Author: Johnny Angel <johnnyangelnj@gmail.com>
 *
 */

namespace App\Models\Base;




/**
 * Class SessionHasNotificationBase
 *
 * @package App\Models
 *
 * @SWG\Definition(
 *   definition="SessionHasNotification",
 *  required={"session_id", "notification_id"}
 * )
 *
 */

class SessionHasNotificationBase extends Model
{
	
	
    // turn on auto timestamps create_at and updated_at
    public $timestamps = false;

    // turn on softe delete_at
    protected $softDelete = false;
    
    // used to hide columns 
    protected $hidden = array();
    
    // used to append columns
    protected $appends = array();
    
    protected $table = 'session_has_notification';
    
    
    


/**
 *
 * @SWG\Property(property="session_id", type="integer", format="int32")
 * @SWG\Property(property="notification_id", type="integer", format="int32")

 *
 */
     
     
    protected $fillable = array(
	'session_id',
	'notification_id'
    );

    protected function getSessionIdAttribute($v){ return (int)$v; }
    protected function getNotificationIdAttribute($v){ return (int)$v; }

    

}
