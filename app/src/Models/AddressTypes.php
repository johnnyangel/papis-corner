<?php
/**
 *  AddressTypes Model
 * 
 * Provides data from "'address_types'"
 *
 * Author: Johnny Angel <johnnyangelnj@gmail.com>
 * Generated on Friday 11th of March 2016 01:05:47 PM
 *
 */
namespace App\Models;

use App\Models\Base\AddressTypesBase;
 
class AddressTypes extends AddressTypesBase
{
    // used to hide columns
    protected $hidden = array();
	
    // used to append columns
    protected $appends = array();

}
