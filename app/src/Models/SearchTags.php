<?php
/**
 *  SearchTags Model
 * 
 * Provides data from "'search_tags'"
 *
 * Author: Johnny Angel <johnnyangelnj@gmail.com>
 * Generated on Sunday 31st of January 2016 05:26:38 AM
 *
 */
namespace App\Models;

use App\Models\Base\SearchTagsBase;
 
class SearchTags extends SearchTagsBase
{
    // used to hide columns
    protected $hidden = array();
	
    // used to append columns
    protected $appends = array();
    
    public function search_tag_products()
    {
    	return $this->hasMany('\App\Models\SearchTagsHasProduct');
    }

}
