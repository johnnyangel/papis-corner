<?php
namespace App\Action;

use Slim\Views\Twig;
use Psr\Log\LoggerInterface;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

final class UserAction
{
    private $view;
    private $logger;
    private $config;
    private $cookies;
    private $client_session;
    
    private $mailbox;
    private $keys;
    private $db;
    
    public function __construct(Twig $view, LoggerInterface $logger, $client_session, $mailbox, $keys, $db)
    {
    	$this->view = $view;
        $this->logger = $logger;
        $this->config = $client_session->getConfig();
        $this->cookies = $client_session->getCookies();
        $this->client_session = $client_session;
        
        $this->mailbox = $mailbox;
        $this->keys = $keys;
        $this->db = $db;
    }

    public function login(Request $request, Response $response, $args)
    {
        $this->logger->debug("User Login Action");
        
        $json = $request->getBody();
        
        $payload = json_decode($json, true);
        
        $this->logger->debug("Payload [". json_encode($payload) ."]");
        
        $user = $this->client_session->getUserAccount($payload['email'], md5($payload['password']));
        
        $user_session = $this->client_session->get();
        
        $this->logger->debug("User Session [". json_encode($user_session) ."]");
        
        if( $user ) {
        	$this->logger->debug("Login User" . json_encode($user));
        	$response = $response->withHeader('Content-Type', 'application/json');
        	$user_session->is_authenticated = true;
        	$user_session->user_profile = $user;
        	$user_session->updated_at = date("Y-m-d H:m:s");
        	$response = $response->withHeader('Set-Cookie', $this->cookies->setDomainCookie($this->config['cookie.options']['cookie_name'], json_encode($user_session)));

        	$this->client_session->persistUserSession((object)array(
        		'guid' => $user_session->guid,
	        	'user_id' => $user->id
        	));

        	if( $this->client_session->isAdmin($user->id) )
        		echo json_encode(array('status' => 'Successful', 'url' => '/admin/'. $user->id . '/home'));
        	else
        		echo json_encode(array('status' => 'Successful', 'url' => '/user/'. $user->id . '/home'));
        	return $response;
        }
        
		return $response->withStatus(404)->write('Username or Password is Incorrect.');
    }
    
    public function logout(Request $request, Response $response, $args)
    {
    	$this->logger->debug("User Logout Action for User " . $args['id']);
    	
    	// TODO: Should we delete the session 
    	//session_destroy();
    	
    	$user_session = $this->client_session->get();
        
        $this->logger->debug("User Session [". json_encode($user_session) ."]");
         
    	$user_session->is_authenticated = false;
    	$user_session->user_profile = null;
    	$user_session->updated_at = date("Y-m-d H:m:s");
    	$response = $response->withHeader('Set-Cookie', $this->cookies->setDomainCookie($this->config['cookie.options']['cookie_name'], json_encode($user_session)));
    	 
    	$response = $response->withHeader('Content-Type', 'application/json');
    	echo json_encode(array('status' => 'Successful', 'url' => '/'));
    	return $response;
    }
    
    public function password_reset(Request $request, Response $response, $args)
    {
    	$this->logger->debug("User Password Reset Action for User " . $args['email']);
    	 	
    	$user = $this->client_session->getUserAccount($args['email']);
    		
    	if( $user ){
    		$password = $this->keys->generatePassWord();
    		
    		if($this->config['smtp.switch'] == true){
	    		$user->password = md5($password);
	    		$user = $this->client_session->setUserAccount($user);
    		}
    	
    		// send student reset email
    		$template_name = $this->config['email.templates']['email_user_password_reset']['template_name'];
    		$letter = [];
    		$letter['subject'] = $this->config['email.templates']['email_user_password_reset']['subject'];
    		$letter['to'] =  $args['email'];
    		$letter['to_username'] =  (isset($user->first_name) && isset($user->last_name))?$user->first_name . ', ' . $user->last_name:'';
    		$letter['from'] =  $this->config['email.templates']['email_user_password_reset']['from_email'];
    		$letter['from_username'] = $this->config['email.templates']['email_user_password_reset']['from_username'];
    		$letter['password'] = $password;
    		$letter['link'] = $this->config['protocol'] . $this->config['domain'] . $this->config['email.templates']['email_user_password_reset']['link'];
    		$letter['first_name'] = $user->first_name;
    		$letter['last_name'] = $user->last_name;
    		if(empty($letter['to_username'])) $letter['last_name'] = 'Customer';
    		
    		$this->logger->debug("User Password Reset Letter " . json_encode($letter));
    		
    		$results = $this->mailbox->send($template_name, $letter);
    		
    		if($results['status'] == 'Successful'){
    			$response = $response->withHeader('Content-Type', 'application/json');
    			echo json_encode(array('status' => 'Successful', 'url' => '/user/login'));
    		} else {
    			$response = $response->withHeader('Content-Type', 'application/json')->withStatus(404)->write($results['response']);    			 
    		}
    	} else $response = $response->withHeader('Content-Type', 'application/json')->withStatus(404)->write('Username Account can not be found.');    			 
    
    	
    	return $response;
    }
    
    public function get_profile(Request $request, Response $response, $args)
    {
    	$this->logger->debug("User Get DB Profile Action for User " . $args['id']);
    	
    	$user_profile = \App\Models\User::with(['permissions', 'social_media', 'profile_picture', 'account'])->where('id', '=', $args['id'])->first();

    	$response = $response->withHeader('Content-Type', 'application/json');
    	echo ( $user_profile )?$user_profile->toJson():'{}';
    	return $response;
    }
    
    public function registration_confirmation(Request $request, Response $response, $args)
    {
    	$this->logger->debug("User Registration Confirmation Action");
    
    	$meta_data = array(
    			"title" => "Registration Confirmation",
    			"user" =>  $this->client_session->getUserProfile(),
    			"attributes" => $request->getAttributes(),
    	);
    
    	$this->view->render($response, "registration-confirmation-landing.twig", $meta_data);
    	return $response;
    }
        
    public function user_details(Request $request, Response $response, $args) {
    	$this->logger->debug("User Details Action");
    	
    	$user_id = $request->getParam('user_id');
    	$ipa = $request->getParam('ipa');
    	$session_id = $request->getParam('session_id');
    	$session_guid = $request->getParam('session_guid');
    	 
    	$this->logger->debug("Args IPA:[" . $ipa . "] User ID[". $user_id  ."]  Session ID[". $session_id  ."] Session GUID[" . $session_guid . "]");
    	 
    	$user_sessions = $user_session_by_ipa = $user_session_by_id = $user_session_by_guid = $user_session_by_user_id = [];
    	 
    }
    
    public function known_location (Request $request, Response $response, $args) {
    	
    	$this->logger->debug("User know location Action");
    	
    	$response = $response->withHeader('Content-Type', 'application/json');
    	
    	$user_session = $this->client_session->get();
    	
    	$lat = $request->getParam('lat');
    	$long = $request->getParam('long');
    	$city = $request->getParam('city');
    	    	
    	if( isset($user_session->current_location) && !empty($user_session->current_location) ){
	    	$updated_at = isset($user_session->updated_at)?$user_session->updated_at:date("Y-m-d H:m:s");
	    	$last_updated_at = strtotime($updated_at);
	    	$right_now = strtotime(date("Y-m-d H:m:s"));
	    	$last_updated_minutes = round(abs($last_updated_at - $right_now) / 60,2);
	    	$this->logger->debug("Session Last Updated [". $last_updated_minutes . " minute");
	    	if($last_updated_minutes > 60){
	    		$user_session->current_location = '';
	    		$user_session->updated_at = date("Y-m-d H:m:s");
	    	}
	    	
	    	if( $user_session->current_location )
	    		echo json_encode(array('current_location' => $user_session->current_location));
	    	else
	    		echo json_encode(array('current_location' => 'FIND-LOCATION'));
    	} else if( $city ) {
    		$this->client_session->persistUserSession((object)array(
    				'guid' => $user_session->guid,
    				'lat' => $lat,
    				'long' => $long
    		));
    		$user_session->current_location =  $city;
    		$this->logger->debug("User Session [". json_encode($user_session) ."]");
    		echo json_encode(array('current_location' => $user_session->current_location));
    	} else 
    		echo json_encode(array('current_location' => 'FIND-LOCATION'));
    	
    	$response = $response->withHeader('Set-Cookie', $this->cookies->setDomainCookie($this->config['cookie.options']['cookie_name'], json_encode($user_session)));
    	 
    	return $response;
    }
    
    public function product_view(Request $request, Response $response, $args){
    	 
    	$this->logger->debug("User Product View Action");
    	 
    	$this->logger->debug("Product GUID [". json_encode($args['product_guid']) ."]");
    	 
    	$user_session = $this->client_session->get();
    	
    	$product_details = \App\Models\Product::with(['product_images'])->where('guid', '=', \App\Models\Product::packGUID($args['product_guid']))->first();
    	 
    	$meta_data = array(
    			"title" => "Product View",
    			"user" =>  $this->client_session->getUserProfile(),
    			"attributes" => $request->getAttributes(),
    			"featured_product_image" => $product_details->toArray(),
    			"is_authenticated" => isset($user_session->is_authenticated)?$user_session->is_authenticated:false,
    			"shopping_cart_item_count" => $this->client_session->getUserShoppingCartItemCount(),
        		"shopping_cart" => $this->client_session->getUserShoppingCart()
    	);
    	    	 
    	$this->logger->debug(json_encode($meta_data));
    	 
    	$this->view->render($response, "product-view.twig", $meta_data);
    	 
    	return $response;
    }
    
    public function product_bookmark_view(Request $request, Response $response, $args){
    
    	$this->logger->debug("User Product Bookmark View Action");
    
    	$this->logger->debug("Product Key [". json_encode($args['product_key']) ."]");
    
    	$user_session = $this->client_session->get();
    	 
    	$product_details = \App\Models\Product::with(['product_images'])->where('key', '=',$args['product_key'])->first();
    
    	$meta_data = array(
    			"title" => $product_details->name,
    			"attributes" => $request->getAttributes(),
    			"featured_product_image" => $product_details->toArray(),
    			"show_title_bar" => false
    	);
    
    	$this->logger->debug(json_encode($meta_data));
    
    	$this->view->render($response, "product-bookmark-view.twig", $meta_data);
    
    	return $response;
    } 

    /**
     * @return \Illuminate\Http\JsonResponse
     *
     * @SWG\Post(
     *   path="/user/profile",
     *   summary="Create User.",
     *   operationId="postUser",
     *   produces={"application/json","text/html"},
     *   tags={"User"},
     *   @SWG\Parameter(ref="#/parameters/profile_payload"),
     *   @SWG\Response(
     *   	response=200,
     *   	description="Successful."
     *   ),
     *   @SWG\Response(
     *   	response=404,
     *   	description="Not found."
     *   ),
     *   @SWG\Response(
     *   	response=400,
     *   	description="Application error."
     *   ),
     *   @SWG\Response(
     *   	response=500,
     *   	description="System error."
     *   )
     * )
     *
     * @SWG\Put(
     *   path="/user/profile",
     *   summary="Update User.",
     *   operationId="putUserbyId",
     *   produces={"application/json","text/html"},
     *   tags={"User"},
     *   @SWG\Parameter(ref="#/parameters/profile_payload"),
     *   @SWG\Response(
     *   	response=200,
     *   	description="Successful."
     *   ),
     *   @SWG\Response(
     *   	response=404,
     *   	description="Not found."
     *   ),
     *   @SWG\Response(
     *   	response=400,
     *   	description="Application error."
     *   ),
     *   @SWG\Response(
     *   	response=500,
     *   	description="System error."
     *   )
     * )
     *
     * @SWG\Parameter(name="profile_payload", in="body", type="objext", required=true, description="User Profile Object", @SWG\Schema(ref="#/definitions/UserProfile"))
     *
     * @SWG\Definition(
     * 	definition="UserProfile",
     *	required={"id", "email", "username", "password", "first_name", "last_name", "profile_picture_url", "gender", "status", "remember_me", "accepted_terms", "email_validation" ,"csrf_key_name", "csrf_value_name"},
	 * @SWG\Property(property="id", type="integer", format="int32"),
	 * @SWG\Property(property="email", type="string"),
	 * @SWG\Property(property="username", type="string"),
	 * @SWG\Property(property="password", type="string"),
	 * @SWG\Property(property="first_name", type="string"),
	 * @SWG\Property(property="last_name", type="string"),
	 * @SWG\Property(property="profile_picture_url", type="string"),
	 * @SWG\Property(property="gender", type="string", description="values ['Male', 'Female']."),
	 * @SWG\Property(property="status", type="string", description="values ['PENDING', 'APPROVED', 'DENIED']."),
	 * @SWG\Property(property="remember_me", type="boolean", description="True or False"),
	 * @SWG\Property(property="accepted_terms", type="boolean", description="True or False"),
	 * @SWG\Property(property="email_validation", type="string", description="Format as YYYY-MM-DD HH:MI:SS"),
     * @SWG\Property(property="csrf_key_name", type="string", description="CSRF token key."),
     * @SWG\Property(property="csrf_value_name", type="string", description="CSRF token value.")
     * )
     *
     */
    
    public function post_profile (Request $request, Response $response, $args){
    	$this->logger->debug("User Post Profile Action");
    	
    	$user_session = $this->client_session->get();
    	
    	$this->logger->debug("User Session [". json_encode($user_session) ."]");
    	
    	$json = $request->getBody();
    	
    	$payload = json_decode($json, true);
    	
    	$this->logger->debug("Payload [". json_encode($payload) ."]");
    	
    	$user_profile = [];
    	
    	$profile_picture = null;
    	$remeber_me = null;
    	$accepted_terms = null;
    	 
    	if(isset($payload['profile_picture_url'])) $profile_picture = $payload['profile_picture_url'];
    	if(isset($payload['remember_me'])) $remeber_me = $payload['remember_me'];
    	if(isset($payload['accepted_terms'])) $accepted_terms = $payload['accepted_terms'];
    	
    	if($payload['user_id'] > 0)
    		$user_profile = \App\Models\User::where('id', '=', $payload['user_id'])->first();
    	else if(isset($payload['email']) && !empty($payload['email']))
    		$user_profile = \App\Models\User::where('email', '=', $payload['email'])->first();
    	 
    	$this->db->transaction(function() use ($response, $user_session, $user_profile, $payload) {
    		 
	    	if( $user_profile ){
	    		$user_profile->email = $payload['email'];
	    		$user_profile->username = $payload['username'];
	    		$user_profile->first_name = $payload['first_name'];
	    		$user_profile->last_name = $payload['last_name'];
	    		//$user_profile->profile_picture_url = $payload['profile_picture_url'];
	    		$user_profile->gender = $payload['gender'];
	    		//$user_profile->remember_me = $payload['remember_me'];
	    		//$user_profile->accepted_terms = $payload['accepted_terms'];
	    		$user_profile->save();
	    	} else {
	    		
	    		$user_profile = new  \App\Models\User(array(
	    				'email' => $payload['email'],
	    				'username' => $payload['username'],
	    				
	    				'first_name' => $payload['first_name'],
	    				'last_name' => $payload['last_name'],
	    				'profile_picture_url' => $profile_picture,
	    				'gender' => $payload['gender'],
	    				'remember_me' => $remeber_me,
	    				'accepted_terms' => $accepted_terms
	    		));
	    		$user_profile->save();
	    	}
    	
    	});
    	
    	$response = $response->withHeader('Content-Type', 'application/json');
    	
    	echo ( $user_profile )?$user_profile->toJson():'{}';
    	
    	$response = $response->withHeader('Set-Cookie', $this->cookies->setDomainCookie($this->config['cookie.options']['cookie_name'], json_encode($user_session)));
    	 
    	return $response;
    }

}
