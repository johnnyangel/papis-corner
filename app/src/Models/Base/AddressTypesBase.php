<?php
/**
 * AddressTypesBase Model
 * 
 * Provides data from "'address_types'"
 *
 * Author: Johnny Angel <johnnyangelnj@gmail.com>
 *
 */

namespace App\Models\Base;




/**
 * Class AddressTypesBase
 *
 * @package App\Models
 *
 * @SWG\Definition(
 *   definition="AddressTypes",
 *  required={"id", "active", "key", "value"}
 * )
 *
 */

class AddressTypesBase extends Model
{
	
	
    // turn on auto timestamps create_at and updated_at
    public $timestamps = false;

    // turn on softe delete_at
    protected $softDelete = false;
    
    // used to hide columns 
    protected $hidden = array();
    
    // used to append columns
    protected $appends = array();
    
    protected $table = 'address_types';
    
    
    


/**
 *
 * @SWG\Property(property="id", type="integer", format="int32")
 * @SWG\Property(property="active", type="boolean", description="True or False")
 * @SWG\Property(property="key", type="string")
 * @SWG\Property(property="value", type="string")

 *
 */
     
     
    protected $fillable = array(
	'id',
	'active',
	'key',
	'value'
    );

    protected function getIdAttribute($v){ return (int)$v; }
    protected function getActiveAttribute($v){ return (bool)$v; }
    protected function getKeyAttribute($v){ return ($v)?$v:''; }
    protected function getValueAttribute($v){ return ($v)?$v:''; }

    

}
