<?php
namespace App\Action;

use Slim\Views\Twig;
use Psr\Log\LoggerInterface;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

final class AdminAction
{
    private $view;
    private $logger;
    private $csrf;
    private $client_session;
    
    public function __construct(Twig $view, LoggerInterface $logger, $client_session)
    {
    	$this->view = $view;
        $this->logger = $logger;
        $this->csrf   = $client_session->getCsrf();
        $this->client_session = $client_session;
    }

    public function dispatch(Request $request, Response $response, $args)
    {
        $this->logger->debug("Admin Dispatched Action");
        
        $this->logger->debug("Args [". json_encode($args) ."]");
        
        $user_session = $this->client_session->get();
                
        $meta_data = array(
        	"title" => "Admin",
        	"show_title_bar_home" => false,
        	"user" =>  $this->client_session->getUserProfile(),
        	"attributes" => $request->getAttributes(),        	
        	"is_authenticated" => isset($user_session->is_authenticated)?$user_session->is_authenticated:false,
        	"shopping_cart_item_count" => $this->client_session->getUserShoppingCartItemCount()
        );
        
        $this->view->render($response, "admin.twig", $meta_data);
        return $response;
    }
    
    /**
     * @return \Illuminate\Http\JsonResponse
     *
     * @SWG\Get(
     *   path="/user/csrf_token",
     *   summary="Papi's CSRF Token",
     *   operationId="getCsrfToken",
     *   produces={"application/json"},
     *	 tags={"User"},
     *   @SWG\Response(
     *   	response=200,
     *   	description="Papi's CSRF Token."
     *   )
     * )
     */
    
    public function csrf_token(Request $request, Response $response, $args) {
    	 
    	$this->logger->debug("User Csrf Token Action");
    	 
    	// CSRF token name and value
    	$nameKey = $this->csrf->getTokenNameKey();
    	$valueKey = $this->csrf->getTokenValueKey();
    	$csrf_key_name = $request->getAttribute($nameKey);
    	$csrf_value_name = $request->getAttribute($valueKey);
    	 
    	$response = $response->withHeader('Content-Type', 'application/json');
    	 
    	echo json_encode(array('csrf_key_name' => $csrf_key_name, 'csrf_value_name' => $csrf_value_name));
    	 
    	return $response;
    }
    
    /**
     * @return \Illuminate\Http\JsonResponse
     *
     * @SWG\Put(
     *   path="/user/reset_password",
     *   summary="Reset user password.",
     *   operationId="putUserPasswordByEmail",
     *   produces={"application/json","text/html"},
     *   tags={"User"},
     *   @SWG\Parameter(ref="#/parameters/authentication_payload"),
     *   @SWG\Response(
     *   	response=200,
     *   	description="Password reset successful."
     *   ),
     *   @SWG\Response(
     *   	response=404,
     *   	description="User not found."
     *   )
     * )
     * 
     * @SWG\Parameter(name="authentication_payload", in="body", type="objext", required=true, description="Authentication Object", @SWG\Schema(ref="#/definitions/Authentication"))
     * 
     * @SWG\Definition(
     * 	definition="Authentication",
     *	required={"user_email", "user_password","csrf_key_name","csrf_value_name"},
     * @SWG\Property(property="user_email", type="string", description="User email."),
     * @SWG\Property(property="user_password", type="string", description="User password (New)"),
     * @SWG\Property(property="csrf_key_name", type="string", description="CSRF token key."),
     * @SWG\Property(property="csrf_value_name", type="string", description="CSRF token value.")
     * )
     * 
     */

    public function reset_user_password(Request $request, Response $response, $args)
    {
    	
    	$this->logger->debug("Admin Reset User Password Action");
    	    	
    	$json = $request->getBody();
    	
    	$payload = json_decode($json, true);
    	
    	$this->logger->debug("Payload [". json_encode($payload) ."]");
    	
    	try {
	    	$user = $this->client_session->getUserAccount($payload['user_email']);
	    	
	    	if( $user ){
	    		$this->logger->info("Reset Password for User: [". json_encode($user) ."]");
	    		$response = $response->withHeader('Content-Type', 'application/json');
	    		$user->password = md5($payload['user_password']);
	    		$this->client_session->setUserAccount($user);
	    		echo json_encode(array('status' => 'Successful', 'response' => 'Users password has been updated.'));
	    	} else {
	    		$response = $response->withStatus(404)->write('User not found.');
	    	}
    	} catch(Exception $e){
    		$response = $response->withStatus(500)->write($e->getMessage());
    	}    	 
    	return $response;
    }
    
    /**
     * @return \Illuminate\Http\JsonResponse
     *
     * @SWG\Get(
     *   path="/user/sessions",
     *   summary="User Sessions. Get a user session by session id or guid or by user id or by ipa",
     *   operationId="getUserSessions",
     *   produces={"application/json"},
     *   tags={"User"},
     *   @SWG\Parameter(ref="#/parameters/user_id"),
     *   @SWG\Parameter(ref="#/parameters/session_id"),
     *   @SWG\Parameter(ref="#/parameters/session_guid"),
     *   @SWG\Parameter(ref="#/parameters/ipa"),
     *   @SWG\Response(
     *   	response=200,
     *   	description="User Session Model and Relationships, If sessions not found, response is empty array [].",
     *   	@SWG\Schema(ref="#/definitions/Session")
     *   )
     * )
     */
    
    /**
     * @SWG\Parameter(name="user_id", in="query", type="string",  description="Enter a user id.")
     * @SWG\Parameter(name="session_id", in="query", type="string",  description="Enter a session id.")
     * @SWG\Parameter(name="session_guid", in="query", type="string",  description="Enter a session guid.")
     * @SWG\Parameter(name="ipa", in="query", type="string",  description="Enter a ip address.")
     */

    
    public function user_sessions(Request $request, Response $response, $args)
    {
    	$this->logger->debug("Admin User Sessions Action");
    	 
    	$user_id = $request->getParam('user_id');
    	$ipa = $request->getParam('ipa');
    	$session_id = $request->getParam('session_id');
    	$session_guid = $request->getParam('session_guid');
    	
    	$this->logger->debug("Args IPA:[" . $ipa . "] User ID[". $user_id  ."]  Session ID[". $session_id  ."] Session GUID[" . $session_guid . "]");
    	
    	$user_sessions = $user_session_by_ipa = $user_session_by_id = $user_session_by_guid = $user_session_by_user_id = [];
    	
    	if(isset($ipa))	$user_session_by_ipa = \App\Models\Session::with(['user', 'shopping_cart', 'shopping_cart.items'])->where('client_ipa', '=', $ipa)->get()->toArray();
    	if(isset($session_id)) $user_session_by_id = \App\Models\Session::with(['user', 'shopping_cart', 'shopping_cart.items'])->where('id', '=', $session_id)->get()->toArray();
    	if(isset($session_guid)) $user_session_by_guid = \App\Models\Session::with(['user', 'shopping_cart', 'shopping_cart.items'])->where('guid', '=', \App\Models\Session::packGUID($session_guid))->get()->toArray();
    	if(isset($user_id)) $user_session_by_user_id = \App\Models\Session::with(['user', 'shopping_cart', 'shopping_cart.items'])->where('user_id', '=', $user_id)->get()->toArray();
    	 
    	$response = $response->withHeader('Content-Type', 'application/json');

    	$user_sessions = array_merge($user_session_by_user_id, array_merge($user_session_by_id, array_merge($user_session_by_ipa, $user_session_by_guid)));
    	
    	if(!isset($user_id) && !isset($ipa) && !isset($session_id) && !isset($session_guid)) 
    		$user_sessions = \App\Models\Session::with(['user', 'shopping_cart', 'shopping_cart.items'])->get()->toArray();

    	$this->logger->debug("User Sessions:[" . json_encode($user_sessions) . "]");
    	
    	echo json_encode(array_filter($user_sessions,'self::unique_obj'));
    	
    	return $response;
    }
    
    public function unique_obj($obj) {
    	static $keys = array();

    	if(in_array($obj['guid'],$keys)) {
    		return false;
    	}
    	$keys[] = $obj['guid'];
    	
    	$this->logger->debug("keys:[" . json_encode($keys) . "]");
    	 
    	return true;
    }
     
}
