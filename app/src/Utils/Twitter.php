<?php
namespace App\Utils;

use Psr\Log\LoggerInterface;
use Psr\Http\Message\ServerRequestInterface as Request;

class Twitter {
	
	private $logger;
	private $config;
	private $cookies;
	private $twitter;
	
	private $access_token;
	private $oauth_token;
	private $oauth_token_secret;
	private $user_profile;
	
	private $isError = false;
	private $error = array(
			'code' => 0,
			'message' => ''
	);
	
	
	
	public function __construct(LoggerInterface $logger, $cookies, $twitter)
	{
		$this->logger  = $logger;
		$this->config  = $cookies->getConfig();
        $this->cookies = $cookies;
        $this->twitter = $twitter;
        $this->error = (object) $this->error;
	}
	
	public function getProfile($oauth_token, $oauth_token_secret) {
		
		$this->logger->debug("Twitter Get User Profile");
		
		$this->oauth_token = $oauth_token;
		$this->oauth_token_secret = $oauth_token_secret;
		
		$isConnected = false;
		
		$params =array();
		$params['include_entities']='false';
		
		$this->twitter = $this->connect($oauth_token, $oauth_token_secret);
		
		$this->logger->debug("Twitter Get User Profile Twitter: " . json_encode($this->twitter));
		
				
		//TODO: check if user oauth_token is set
		//$access_token = $this->getAccessToken();
		
		//if( $access_token ){

			$response = $this->twitter->get('account/verify_credentials',$params);
			
			$this->logger->debug("Twitter Get User Profile Verify Credentials: " . json_encode($response));
	
			if($response && isset($response->screen_name) && isset($response->name)){
				$this->logger->debug("Twitter Get User Screen Name (ID): " . $response->screen_name);
				$this->logger->debug("Twitter Get User Profile name: " . $response->name);
				$this->logger->debug("Twitter Get User Profile image: " . $response->profile_image_url);
			} else {
				$this->logger->debug("Twitter Get User Profile Login Error");
			}
		//} else {
		//	$this->logger->debug("Twitter Get User Profile Login Error");
		//}

		return (object) array('status' => ($isConnected)?'Successful':'Failed', 'user_profile' => $this->user_profile);
	}
	
	public function accountVerification() {
		
		$response = null;
		
		$this->logger->debug("Twitter Account Verification");
		
		$response = $this->twitter->get("account/verify_credentials");
		
		$this->logger->debug("Account Verification: " . json_encode($response));
		
		return $response;
		
	}
	
	public function getAccessToken() {
		
		$response = null;
		
		$this->logger->debug("Twitter Get Access Token");
		
		try {
			$callback_url = $this->config['protocol'] . $this->config['domain'] . $this->config['twitter.options']['oauth_callback_url'];
			$response = $this->twitter->getRequestToken($callback_url);
		} catch (OAuthException $e) {
			// your error handling here
			$this->logger->error("Twitter Get Access Token Error: " . json_encode($e));
		}
		
		$this->logger->debug("Access Token: " . json_encode($response));
		
		return $response;
	}
	
	
	public function getLoginUrl(){
		
		//TODO; Check if User has authenticated with Twitter
		$isConnected = false;
				
		$request_token = $this->getAccessToken();
	
		if(	$request_token )
		{
			$this->oauth_token = $request_token['oauth_token'];
			$this->oauth_token_secret = $request_token['oauth_token_secret'];
	
			if($this->twitter->http_code == 200){
				$url = $this->twitter->getAuthorizeURL($this->oauth_token);
				$this->user_profile['oauth_token'] = $this->oauth_token;
				$this->user_profile['oauth_token_secret'] = $this->oauth_token_secret;
				$this->user_profile['url'] = $url;
				$isConnected = true;
			} else {
				$this->isError = true;
				$this->error->code = 401;
				$this->error->message = 'No OAuth data could be obtained from the signed request. User has not authorized your app yet.';
				$this->logger->error(json_encode($this->error));
			}
		} else {
			$this->isError = true;
			$this->error->code = $e->getCode();
			$this->error->message = "Twitter SDK returned an error: " . $e->getMessage();
			$this->logger->error(json_encode($this->error));
		}

		return (object) array('status' => ($isConnected)?'Successful':'Failed', 'user_profile' => $this->user_profile);
	}
	
	private function connect($oauth_token, $oauth_token_secret) {
		
		$this->logger->debug("Twitter Connect");
		
		$customer_key = $this->config['twitter.options'][$this->config['env']]['customer_key'];
		$customer_secret = $this->config['twitter.options'][$this->config['env']]['customer_secret'];
		
		return new \Abraham\TwitterOAuth\TwitterOAuth($customer_key, $customer_secret, $this->oauth_token, $this->oauth_token_secret);
	}

	public function disconnect() {
		$this->logger->debug("Twitter Disconnect");
	}
	
	public function getError() {
		return $this->error;
	}
}