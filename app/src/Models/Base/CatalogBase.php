<?php
/**
 * CatalogBase Model
 * 
 * Provides data from "'catalog'"
 *
 * Author: Johnny Angel <johnnyangelnj@gmail.com>
 *
 */

namespace App\Models\Base;


use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class CatalogBase
 *
 * @package App\Models
 *
 * @SWG\Definition(
 *   definition="Catalog",
 *  required={"id", "name", "discription", "path"}
 * )
 *
 */

class CatalogBase extends Model
{
	use SoftDeletes;
	
    // turn on auto timestamps create_at and updated_at
    public $timestamps = true;

    // turn on softe delete_at
    protected $softDelete = true;
    
    // used to hide columns 
    protected $hidden = array();
    
    // used to append columns
    protected $appends = array();
    
    protected $table = 'catalog';
    
    protected $dates = ['deleted_at'];
    


/**
 *
 * @SWG\Property(property="id", type="integer", format="int32")
 * @SWG\Property(property="name", type="string")
 * @SWG\Property(property="discription", type="string")
 * @SWG\Property(property="path", type="string")

 *
 */
     
     
    protected $fillable = array(
	'id',
	'name',
	'discription',
	'path',
	'created_at',
	'updated_at',
	'deleted_at'
    );

    protected function getIdAttribute($v){ return (int)$v; }
    protected function getNameAttribute($v){ return ($v)?$v:''; }
    protected function getDiscriptionAttribute($v){ return ($v)?$v:''; }
    protected function getPathAttribute($v){ return ($v)?$v:''; }
    protected function getCreatedAtAttribute($v){ return $v; }
    protected function getUpdatedAtAttribute($v){ return $v; }
    protected function getDeletedAtAttribute($v){ return $v; }

    

}
