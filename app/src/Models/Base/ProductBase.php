<?php
/**
 * ProductBase Model
 * 
 * Provides data from "'product'"
 *
 * Author: Johnny Angel <johnnyangelnj@gmail.com>
 *
 */

namespace App\Models\Base;


use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class ProductBase
 *
 * @package App\Models
 *
 * @SWG\Definition(
 *   definition="Product",
 *  required={"id", "guid", "key", "catalog_id", "status", "name", "caption", "description", "path", "price", "provided_by", "sort_order", "tags"}
 * )
 *
 */

class ProductBase extends Model
{
	use SoftDeletes;
	
    // turn on auto timestamps create_at and updated_at
    public $timestamps = true;

    // turn on softe delete_at
    protected $softDelete = true;
    
    // used to hide columns 
    protected $hidden = array();
    
    // used to append columns
    protected $appends = array();
    
    protected $table = 'product';
    
    protected $dates = ['deleted_at'];
    


/**
 *
 * @SWG\Property(property="id", type="integer", format="int32")
 * @SWG\Property(property="guid", type="string")
 * @SWG\Property(property="key", type="string")
 * @SWG\Property(property="catalog_id", type="integer", format="int32")
 * @SWG\Property(property="status", type="string", description="values ['PENDING', 'AVAILABLE', 'ON_HOLD', 'SOLD_OUT'].")
 * @SWG\Property(property="name", type="string")
 * @SWG\Property(property="caption", type="string")
 * @SWG\Property(property="description", type="string")
 * @SWG\Property(property="path", type="string")
 * @SWG\Property(property="price", type="string", description="Two decimal value, 0.00")
 * @SWG\Property(property="provided_by", type="string")
 * @SWG\Property(property="sort_order", type="integer", format="int32")
 * @SWG\Property(property="tags", type="string")

 *
 */
     
     
    protected $fillable = array(
	'id',
	'guid',
	'key',
	'catalog_id',
	'status',
	'name',
	'caption',
	'description',
	'path',
	'price',
	'provided_by',
	'sort_order',
	'tags',
	'created_at',
	'updated_at',
	'deleted_at'
    );

    protected function getIdAttribute($v){ return (int)$v; }
    protected function getGuidAttribute($v){ return self::unpackGUID($v); }
    protected function getKeyAttribute($v){ return ($v)?$v:''; }
    protected function getCatalogIdAttribute($v){ return (int)$v; }
    protected function getNameAttribute($v){ return ($v)?$v:''; }
    protected function getCaptionAttribute($v){ return ($v)?$v:''; }
    protected function getDescriptionAttribute($v){ return ($v)?$v:''; }
    protected function getPathAttribute($v){ return ($v)?$v:''; }
    protected function getProvidedByAttribute($v){ return ($v)?$v:''; }
    protected function getSortOrderAttribute($v){ return (int)$v; }
    protected function getTagsAttribute($v){ return ($v)?$v:''; }
    protected function getCreatedAtAttribute($v){ return $v; }
    protected function getUpdatedAtAttribute($v){ return $v; }
    protected function getDeletedAtAttribute($v){ return $v; }

    protected function setGuidAttribute($v){ $this->attributes['guid'] = self::packGUID($v); }

}
