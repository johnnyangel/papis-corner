<?php
namespace App\Utils;

use Psr\Log\LoggerInterface;
use Psr\Http\Message\ServerRequestInterface as Request;

class ClientSession {
	
	public static $default_profile_picture = '/images/social-media/user-avatar-blue.png';
	 
	private $logger;
	private $config;
	private $cookies;
	private $crsf;
	private $keys;
	
	private $session = array(
		'id'						=> '',
		'guid'						=> '',
		'client_ipa'				=> '',
		'user_agent'				=> '',
		'current_location'			=> '',
		'is_authenticated'			=> FALSE,
		'user_profile'				=> array(
				
		),
		'has_shopping_cart'			=> FALSE,
		'shopping_cart'				=> array(
		),
		'updated_at'				=> '',
		
	);
	
	public function __construct(LoggerInterface $logger, $config, $cookies, $crsf, $keys)
	{
		$this->logger 	= $logger;
		$this->config 	= $config;
		$this->cookies 	= $cookies;
		$this->crsf 	= $crsf;
		$this->keys 	= $keys;
	}
	
	public function init(Request $request){
		
		$headers = $request->getHeaders();

		$this->session['user_agent'] = $headers['HTTP_USER_AGENT'];
		
		$this->session['client_ipa'] = $_SERVER['REMOTE_ADDR'];
		
		$this->session['updated_at'] = date("Y-m-d H:m:s");
	
		try{
			$keys = new Keys($this->logger, $this->config);
			$this->session['guid'] = $keys->generateGUID();
		}catch(Execption $e){
			$this->logger->error("Session GUID ERROR ". $e->getMessage());
		}
		
		$this->logger->debug("Session ". json_encode($this->session));
		
		return $this->session;
	}
	
	public function getConfig()
	{
		return $this->config;
	}
	
	public function getCookies()
	{
		return $this->cookies;
	}
	
	public function getCsrf()
	{
		return $this->crsf;
	}
	
	public function getKeys()
	{
		return $this->keys;
	}
	
	public function get()
	{
		return json_decode($this->cookies->getDomainCookie($this->config['cookie.options']['cookie_name']));
	}
	
	public function getUserProfile()
	{
		
		$user_session = $this->get();
		
		$profile = [
			"user_id" => (isset($user_session->is_authenticated) && $user_session->is_authenticated)?$user_session->user_profile->id:'',
			"user_name" => (isset($user_session->is_authenticated) && $user_session->is_authenticated)?$user_session->user_profile->name:'Hola, Bienvenido',
			"profile_picture" => (!empty($user_session->user_profile))?$user_session->user_profile->profile_picture:self::$default_profile_picture,
			"home_page" => (isset($user_session->is_authenticated) && $user_session->is_authenticated)?'/user/' . $user_session->user_profile->id . '/home':'/',
			"user_login_with_social_media"	=> (!empty($user_session->user_profile))?$user_session->user_profile->user_login_with_social_media:'',
			"user_account_number"	=> (!empty($user_session->user_profile))?$user_session->user_profile->account_number:''
		];
		
		return (object) $profile;
	}

	public function setUserAccount($user)
	{
		$user_account = null;
				
		if( isset($user->id) ) $user_account = \App\Models\User::with(['permissions', 'social_media', 'profile_picture', 'account'])->find($user->id);
		
		if( $user_account && $user_account->exists()){
			$user_account->email = $user->email;
			$user_account->first_name = $user->first_name;
			$user_account->last_name = $user->last_name;
			$user_account->profile_picture_url = $user->profile_picture;
			if(isset($user->password)) $user_account->password = $user->password;
		} else {	
			$user_account = new \App\Models\User(array(
				'email' => $user->email,
				'profile_picture_url' => $user->profile_picture,
				'first_name' => $user->first_name,
				'last_name' => $user->last_name,
				'status' => 'PENDING',
			));
		}
		
		$user_account->save();
				
		if( $user->social_media ){
			foreach($user->social_media as $social_service){
				
				$this->logger->debug("Client Session Social Service " . json_encode($social_service));

				$user_account_social_media = \App\Models\SocialMedia::where('user_id', '=', $social_service->user_id)->where('service', '=', $social_service->service)->first();

				$this->logger->debug("Client Session Social Media Account Service ". $social_service->service . ' = ' . ((isset($user_account_social_media))?$user_account_social_media->service:""));
				
				if( $user_account_social_media ){
					// Update Social Media;
					$user_account_social_media->service_id = $social_service->service_id;
					$user_account_social_media->service_email = $social_service->service_email;
					$user_account_social_media->service_username = $social_service-> service_username;
					$user_account_social_media->save();
				} else {
					// Create Social Media
					$social_media = new \App\Models\SocialMedia((array)$social_service);
					$social_media = $social_media->save();
				}
			}
		}
		
		$user_login_with_social_media = $user->user_login_with_social_media;
		$user = $this-> getUserAccount($user->email);
		$user->user_login_with_social_media = $user_login_with_social_media;
				
		$this->logger->debug("Client Session User Account " . json_encode($user));
		
		return $user;
			
	}
	
	public function getUserAccount($email, $password = null)
	{
		$user = null;
		$user_profile = null;
		
		if(isset($password))
			$user = \App\Models\User::with(['permissions', 'social_media', 'profile_picture', 'account'])->where('email', '=', $email)->where('password', '=', $password)->first();
		else 
			$user = \App\Models\User::with(['permissions', 'social_media', 'profile_picture', 'account'])->where('email', '=', $email)->first();

		if( $user )
	        if($user->exists()){
				$user_profile = (object)  array(
						'id' => $user->id,
						'email' => $user->email,
						'name' => $user->first_name . " " . $user->last_name,
						'first_name' => $user->first_name,
						'last_name' => $user->last_name,
						'profile_picture' => $user->profile_picture_url,
						'account_number' => 'PAPI-'.$user->id.'-'.sprintf('%07d', $user->account->id),
						'user_login_with_social_media' => 'None',
						'permissions' => $user->permissions,
						'social_media' => $user->social_media
				);
			}
		
		return $user_profile;
	}
	
	public function persistUserSession($db_user_session){
		
		$this->logger->debug("persist DB User Session " . json_encode($db_user_session));
		
		$session = \App\Models\Session::where('guid', '=', \App\Models\Session::packGUID($db_user_session->guid))->first();

		try {
			if( $session ){
				$this->logger->debug("Current Session " . $session->toJson());
				
				if(isset($db_user_session->user_id)) $session->user_id = $db_user_session->user_id;
				if(isset($db_user_session->lat)) $session->lat = $db_user_session->lat;
				if(isset($db_user_session->long)) $session->long = $db_user_session->long;
			
				$session->save();
			}
		} catch(\Exception $e) {
			$this->logger->error("Error Persisting DB User Session " . $e->getMessage());
		}
		
		return (isset($session))?$session->toArray():'{}';
		
	}
	
	public function isAdmin($id) 
	{
		$admin = \App\Models\User::with(['permissions' =>
				function($query) {
					$query->where('permission', '=' ,'MASTER');
				}])->where('id', '=', $id)->where('status', '=', 'APPROVED')->get();
				 
				$this->logger->info("isAdmin " . $admin->tojson());
				 
				return ( isset($admin->permissions) && !empty($admin->permissions) )?true:false;
	}

	public function getDefaultProfilePicture() 
	{
		return self::$default_profile_picture;
	}
	
	public function getUserShoppingCartItemCount () {
		
		$this->logger->debug("get User Shopping Cart Items ");
		
		$items = 0;
		
		$user_session =  $this->get();
		
		$this->logger->debug("UserShoppingCartItemCount User Session [".json_encode($user_session)."]");
		
		if(isset($user_session->has_shopping_cart) && $user_session->has_shopping_cart){
			foreach($user_session->shopping_cart->items as $item){
				$this->logger->debug("Shopping Cart Item " . json_encode($item));
				$items++;
			}
			$this->logger->debug("User Shopping Cart [". $user_session->shopping_cart->guid ."] Items [". $items ."]");	
		} else 
			$this->logger->debug("User Does Not have a Shopping Cart");
			
		return $items;
	}
	
	public function getUserShoppingCart() {
	
		$this->logger->debug("get User Shopping Cart");
		
		$shopping_cart = null;
	
		if( $this->getUserShoppingCartItemCount() ){
			$shopping_cart = $this->get()->shopping_cart;
			foreach($shopping_cart->items as &$item){
				$item->featured_product_image = \App\Models\Product::with(['product_images'])->where('guid','=', \App\Models\Product::packGUID($item->product_guid))->first();
			}
		}
			
		return $shopping_cart;
	}
	
}