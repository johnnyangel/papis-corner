<?php
/**
 * SearchTagsHasProductBase Model
 * 
 * Provides data from "'search_tags_has_product'"
 *
 * Author: Johnny Angel <johnnyangelnj@gmail.com>
 *
 */

namespace App\Models\Base;




/**
 * Class SearchTagsHasProductBase
 *
 * @package App\Models
 *
 * @SWG\Definition(
 *   definition="SearchTagsHasProduct",
 *  required={"search_tags_id", "product_id", "product_guid"}
 * )
 *
 */

class SearchTagsHasProductBase extends Model
{
	
	
    // turn on auto timestamps create_at and updated_at
    public $timestamps = false;

    // turn on softe delete_at
    protected $softDelete = false;
    
    // used to hide columns 
    protected $hidden = array();
    
    // used to append columns
    protected $appends = array();
    
    protected $table = 'search_tags_has_product';
    
    
    


/**
 *
 * @SWG\Property(property="search_tags_id", type="integer", format="int32")
 * @SWG\Property(property="product_id", type="integer", format="int32")
 * @SWG\Property(property="product_guid", type="string")

 *
 */
     
     
    protected $fillable = array(
	'search_tags_id',
	'product_id',
	'product_guid'
    );

    protected function getSearchTagsIdAttribute($v){ return (int)$v; }
    protected function getProductIdAttribute($v){ return (int)$v; }
    protected function getProductGuidAttribute($v){ return self::unpackGUID($v); }

    protected function setProductGuidAttribute($v){ $this->attributes['product_guid'] = self::packGUID($v); }

}
