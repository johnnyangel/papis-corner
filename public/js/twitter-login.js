 

function twitter_signup(response) {

	var url = '/user/twitter-login';
	
    console.log('Twitter User Signin', auth2.isSignedIn.get());
	  
	//console.log(response);
    
    if (auth2.isSignedIn.get()) {
      console.log('Welcome Back!  Fetching your information.... ');
      
      
	  var profile = auth2.currentUser.get().getBasicProfile();
	  console.log('Successful login for: (id:' + profile.getId() + ') ' + profile.getName());
	  
	  var name = profile.getName().split(" ");
	  
	  var payload = {
			  'id': profile.getId(),
			  'name': profile.getName(),
			  'first_name': name[0],
			  'last_name': name[1],
			  'profile_image': profile.getImageUrl(),
			  'email': profile.getEmail()
      };
      
	  // CSRF Token
	  your_csrf_token = parent.csrf_token();
	  for (var key in your_csrf_token) {
	  	payload[key] = your_csrf_token[key];
	  }
	  
	  console.log('payload ' + JSON.stringify(payload, undefined, 2));

	  // Post Google User Profile to Social Media
	  $.ajax( {
		type: 'POST',
		url: url,
		data: JSON.stringify(payload, null, 2),
		contentType: "application/json; charset=utf-8",
		dataType: "json",  // dataType of response is JSON
		success: function( response ) {
			$( '#login-message-container' ).show();
			$( '#login-message' ).addClass('success label message-box').html('You are login using Google.');  			
			$( '#registration-message-container' ).show();
			$( '#registration-message' ).addClass('success label message-box').html('You are login using Google.');
			location.href = response.url;	
		},
		error: function( response ) {
			$( '#login-message-container' ).show();
			$( '#login-message' ).addClass('warning label message-box').html("Please Sign up for a Papi's Corner Account.");  
			$( '#registration-message-container' ).show();
			$( '#registration-message' ).addClass('warning  label message-box').html("Please Sign up for a Papi's Corner Account.");
			console.log(JSON.stringify(response, null, 2));
		}
	  });
	 
	} else {
		// The person is not logged into Google
		$( '#login-message-container' ).show();
		$( '#login-message' ).addClass('warning label message-box').html("Please Sign up for a Papi's Corner Account.");
		$( '#registration-message-container' ).show();
		$( '#registration-message' ).addClass('warning  label message-box').html("Please Sign up for a Papi's Corner Account.");
	}
}

function twitter_login_url() {
	
	var url = '/user/twitter-login-url';
	
    console.log('Twitter User Login Request');

    $.get( url, function( response ) {
    	console.log(response);
    	if(response['status'] == 'Successful')
    		location.href = response['user_profile']['url'];
    	
    });
    
}

function twitter_logout (){
   console.log('Logout Twitter!');
}


$(document).ready(function(){

    $('#twitter_signin').click(function() {
    	twitter_login_url();
    });

});
