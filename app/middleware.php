<?php
// Application middleware
// e.g: $app->add(new \Slim\Csrf\Guard);

$mw = function ($request, $response, $next) use ($app) {

	$c = $app->getContainer();
	$config  = $c->get('config');
	$cookies = $c->get('cookies');
	$cookie_name = $config['cookie.options']['cookie_name'];
	
	if(! $cookies->isDomainCookie($cookie_name)){
		$session = $c->get('client_session');
		$client_session = $session->init($request);
		
		$user_session = new \App\Models\Session(array(
			'guid' => $client_session['guid'],
			'client_ipa' => $client_session['client_ipa']
		));
		
		$user_session->save();
		
		$client_session['id'] = $user_session->id;
		$response = $response->withHeader('Set-Cookie', $cookies->setDomainCookie($cookie_name, json_encode($client_session)));
	}
	
	$response = $next($request, $response);

	return $response;
};

$app->add($app->getContainer()->get('authenticate'));

$app->add($app->getContainer()->get('csrf'));

//$app->add($app->getContainer()->get('csrf_header'));

