 
var auth2; // The Sign-In object.
var google_user; // The current user.

function google_signup(response) {

	var url = '/user/google-login';
	
    google_user = auth2.currentUser.get();

    //console.log('Google User: ', JSON.stringify(google_user, undefined, 2));
    console.log('Google User is Signin: ', auth2.isSignedIn.get());
	  
	//console.log(response);
    
    if (auth2.isSignedIn.get()) {
      console.log('Welcome Back!  Fetching your information.... ');
      
      
	  var profile = auth2.currentUser.get().getBasicProfile();
	  console.log('Successful login for: (id:' + profile.getId() + ') ' + profile.getName());
	  
	  var name = profile.getName().split(" ");
	  
	  var payload = {
			  'id': profile.getId(),
			  'name': profile.getName(),
			  'first_name': name[0],
			  'last_name': name[1],
			  'profile_image': profile.getImageUrl(),
			  'email': profile.getEmail()
      };
      
	  // CSRF Token
	  your_csrf_token = parent.csrf_token();
	  for (var key in your_csrf_token) {
	  	payload[key] = your_csrf_token[key];
	  }
	  
	  console.log('payload ' + JSON.stringify(payload, undefined, 2));

	  // Post Google User Profile to Social Media
	  $.ajax( {
		type: 'POST',
		url: url,
		data: JSON.stringify(payload, null, 2),
		contentType: "application/json; charset=utf-8",
		dataType: "json",  // dataType of response is JSON
		success: function( response ) {
			$( '#login-message-container' ).show();
			$( '#login-message' ).addClass('success label message-box').html('You are login using Google.');  			
			$( '#registration-message-container' ).show();
			$( '#registration-message' ).addClass('success label message-box').html('You are login using Google.');
			location.href = response.url;	
		},
		error: function( response ) {
			$( '#login-message-container' ).show();
			$( '#login-message' ).addClass('warning label message-box').html("Please Sign up for a Papi's Corner Account.");  
			$( '#registration-message-container' ).show();
			$( '#registration-message' ).addClass('warning  label message-box').html("Please Sign up for a Papi's Corner Account.");
			console.log(JSON.stringify(response, null, 2));
		}
	  });
	 
	} else {
		// The person is not logged into Google
		$( '#login-message-container' ).show();
		$( '#login-message' ).addClass('warning label message-box').html("Please Sign up for a Papi's Corner Account.");
		$( '#registration-message-container' ).show();
		$( '#registration-message' ).addClass('warning  label message-box').html("Please Sign up for a Papi's Corner Account.");
	}
}

function google_login() {
   	var auth2 = gapi.auth2.getAuthInstance();
    auth2.grantOfflineAccess({'redirect_uri': 'postmessage'}).then(google_signup);
}

function google_logout (){
    var auth2 = gapi.auth2.getAuthInstance();
    
    auth2.signOut().then(function () {
      console.log('Logout Google!');
    });	

    google_load_auth();
}


function google_gen_signup_button(){
	
   gapi.signin.render('custom_google_btn', {
        'redirecturi':  "postmessage",
        'accesstype':   "offline",
        'callback':     "signinCallback",
        'scope':        "https://www.googleapis.com/auth/userinfo.email",
        'clientid':     "****************.apps.google_usercontent.com",
        'cookiepolicy': "single_host_origin"
    });
}

function google_load_auth(){
	console.log('Google Loading Auth');
    gapi.load('auth2', function() {
        auth2 = gapi.auth2.init({
        client_id: $( '#google_app_id' ).val(),
        // Scopes to request in addition to 'profile' and 'email'
        //scope: 'additional_scope'
      });
  });
}

$(document).ready(function(){

	google_load_auth();

    $('#google_signin').click(function() {
    	google_login();
    });

});




/**
 * Calls startAuth after Sign in V2 finishes setting up.
 */
var appStart = function() {
  console.log('Google appStart');
  gapi.load('auth2', google_init_signinV2);
};

/**
 * Initializes Signin v2 and sets up listeners.
 */
var google_init_signinV2 = function() {
  console.log('Google google_init_signinV2');
  auth2 = gapi.auth2.init({
      client_id: $( '#google_app_id' ).val(),
      scope: 'profile'
  });

  // Listen for sign-in state changes.
  auth2.isSignedIn.listen(google_signin_changed);

  // Listen for changes to current user.
  auth2.currentUser.listen(google_user_changed);

  // Sign in the user if they are currently signed in.
  if (auth2.isSignedIn.get() == true) {
	console.log('Google Signin v2');
    auth2.signIn();
  }

  // Start with the current live values.
  google_refresh_values();
};

/**
 * Listener method for sign-out live value.
 *
 * @param {boolean} val the updated signed out state.
 */
var google_signin_changed = function (val) {
  console.log('Google Signin state changed to ', val);
};

/**
 * Listener method for when the user changes.
 *
 * @param {google_user} user the updated user.
 */
var google_user_changed = function (user) {
  console.log('Google User changed and is now: ', user);
  google_user = user;
  google_user_update();
  console.log(JSON.stringify(user, undefined, 2));
};

/**
 * Updates the properties in the Google User table using the current user.
 */
var google_user_update = function () {
  if (google_user) {
	 console.log('Google User Profile ID: ', google_user.getId());
	 console.log('Google User Profile Scopes: ',  google_user.getGrantedScopes());
	 console.log('Google User Profile Auth Response: ',  JSON.stringify(google_user.getAuthResponse(), undefined, 2));
  } 
};


var google_refresh_values = function() {
  if (auth2){
	  
    console.log('Google Refresh Values');

    google_user = auth2.currentUser.get();

    console.log('Google User: ', JSON.stringify(google_user, undefined, 2));
    console.log('Google User is Signin: ', auth2.isSignedIn.get());

    google_user_update();
  }
}