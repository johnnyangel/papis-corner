<?php
/**
 *  Product Model
 * 
 * Provides data from "'product'"
 *
 * Author: Johnny Angel <johnnyangelnj@gmail.com>
 * Generated on Sunday 17th of January 2016 08:26:54 PM
 *
 */
namespace App\Models;

use App\Models\Base\ProductBase;
 
class Product extends ProductBase
{
    // used to hide columns
    protected $hidden = array();
	
    // used to append columns
    protected $appends = array();
    
    public function product_state(){
    	return ['P' => 'PENDING', 'A' => 'AVAILABLE', 'H' => 'ON HOLD', 'S' => 'SOLD OUT'];
    }
    
    public function product_images()
    {
    	return $this->hasMany('\App\Models\ProductImage');
    }
}
