<?php
/**
 * SessionBase Model
 * 
 * Provides data from "'session'"
 *
 * Author: Johnny Angel <johnnyangelnj@gmail.com>
 *
 */

namespace App\Models\Base;


use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class SessionBase
 *
 * @package App\Models
 *
 * @SWG\Definition(
 *   definition="Session",
 *  required={"id", "guid", "client_ipa", "user_id", "lat", "long"}
 * )
 *
 */

class SessionBase extends Model
{
	use SoftDeletes;
	
    // turn on auto timestamps create_at and updated_at
    public $timestamps = true;

    // turn on softe delete_at
    protected $softDelete = true;
    
    // used to hide columns 
    protected $hidden = array();
    
    // used to append columns
    protected $appends = array();
    
    protected $table = 'session';
    
    protected $dates = ['deleted_at'];
    


/**
 *
 * @SWG\Property(property="id", type="integer", format="int32")
 * @SWG\Property(property="guid", type="string")
 * @SWG\Property(property="client_ipa", type="string")
 * @SWG\Property(property="user_id", type="integer", format="int32")
 * @SWG\Property(property="lat", type="string")
 * @SWG\Property(property="long", type="string")

 *
 */
     
     
    protected $fillable = array(
	'id',
	'guid',
	'client_ipa',
	'user_id',
	'lat',
	'long',
	'created_at',
	'updated_at',
	'deleted_at'
    );

    protected function getIdAttribute($v){ return (int)$v; }
    protected function getGuidAttribute($v){ return self::unpackGUID($v); }
    protected function getClientIpaAttribute($v){ return ($v)?$v:''; }
    protected function getUserIdAttribute($v){ return (int)$v; }
    protected function getLatAttribute($v){ return ($v)?$v:''; }
    protected function getLongAttribute($v){ return ($v)?$v:''; }
    protected function getCreatedAtAttribute($v){ return $v; }
    protected function getUpdatedAtAttribute($v){ return $v; }
    protected function getDeletedAtAttribute($v){ return $v; }

    protected function setGuidAttribute($v){ $this->attributes['guid'] = self::packGUID($v); }

}
