<?php
/**
 *  ProfilePicture Model
 * 
 * Provides data from "'profile_picture'"
 *
 * Author: Johnny Angel <johnnyangelnj@gmail.com>
 * Generated on Monday 7th of March 2016 11:10:18 PM
 *
 */
namespace App\Models;

use App\Models\Base\ProfilePictureBase;
 
class ProfilePicture extends ProfilePictureBase
{
    // used to hide columns
    protected $hidden = array();
	
    // used to append columns
    protected $appends = array();
    
    public function user()
    {
    	return $this->belongsTo('\App\Models\User');
    }

}
