<?php
namespace App\Action;

use Slim\Views\Twig;
use Psr\Log\LoggerInterface;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

final class SocialMediaAction
{
	
	public static $facebook_service = 'Facebook';
	public static $google_service = 'Google';
	public static $twitter_service = 'Twitter';
	public static $linkedin_service = 'Linkedin';
	public static $paypal_service = 'Paypal';
	
    private $logger;
    private $config;
    private $cookies;
    private $client_session;
    
    private $facebook;
    private $google;
    private $twitter;
    private $linkedin;
    
    private $social_media_user_profile;
    
    private $successful = array(
    		'code' => 200,
    		'message' => 'Successful'
    );

    public function __construct(LoggerInterface $logger, $client_session, $facebook, $google, $twitter, $linkedin)
    {
        $this->logger = $logger;
        $this->config = $client_session->getConfig();
        $this->cookies = $client_session->getCookies();
        $this->client_session = $client_session;
        
        $this->facebook = $facebook;
        $this->google = $google;
        $this->twitter = $twitter;
        $this->linkedin = $linkedin;
    }

    public function facebook_login_url(Request $request, Response $response, $args)
    {
    	$this->logger->debug("Social Media Get Facebook Login Url Action");
    
    	$response = $response->withHeader('Content-Type', 'application/json');
    	 
    	$fb_login_link = $this->facebook->getLoginLink();
    	
    	echo json_encode(array('login_url' => $fb_login_link->login_url));
    
    	return $response;
    }
    
    public function facebook_authenticate_user(Request $request, Response $response, $args)
    {
    	$this->logger->debug("Social Media Get Facebook Access Token Action");
    	
    	$id = $request->getParam('id');
    	$name = $request->getParam('name');
    	
    	$response = $response->withHeader('Content-Type', 'application/json');
    	
    	$user_session = $this->client_session->get();
    	
    	$this->logger->debug("User Session [". json_encode($user_session) ."]");
    	 
    	$fb_response = $this->facebook->getUserProfile($id, $name);
    	
    	if($fb_response->status == 'Successful'){
    		$fb_user_id = $fb_response->user_profile->getId();
	    	$fb_user_name = $fb_response->user_profile->getName();
	    	$fb_user_firstname = $fb_response->user_profile->getFirstName();
	    	$fb_user_lastname = $fb_response->user_profile->getLastName();
	    	$fb_user_email = $fb_response->user_profile->getEmail();
	    	$fb_user_profile_picture = json_decode($fb_response->user_profile->getPicture());

	    	if(empty($fb_user_email)) $fb_user_email = strtolower ( $fb_user_firstname ) . '.' . strtolower ( $fb_user_lastname ) . '@facebook.com';
	    	
	    	$user = $this->client_session->getUserAccount($fb_user_email);
	    	
	    	$user_has_account = false;
	    	
	    	if( $user ){
	    		$this->logger->debug("Login User" . json_encode($user));
	    		$user_has_account = true;
	    		$user->first_name = $fb_user_firstname;
	    		$user->last_name = $fb_user_lastname;
	    		$user->user_login_with_social_media = self::$facebook_service;
	    		if(! is_array($user->social_media)) $user->social_media = array();
	    		if( $user->profile_picture == null || $user->profile_picture ==  $this->client_session->getDefaultProfilePicture() ){
	    			$user->profile_picture = $fb_user_profile_picture->url;
	    		}
    		} 
	    	
	    	if(! $user_has_account){
	    		// Add User Account with FB User Profile
	    		$user = array(
	    			'email' => $fb_user_email,
	    			'profile_picture' => $fb_user_profile_picture,
	    			'first_name' => $fb_user_firstname,
	    			'last_name' => $fb_user_lastname,
	    			'status' => 'PENDING',
	    			'user_login_with_social_media' => self::$facebook_service,
	    			'social_media' => array()
	    		);
	    		$user = $this->client_session->setUserAccount($user);
	    	}
	    	
	    	$user_has_social_media_account = false;
	    	
	    	if($user_has_account && (isset($user->social_media) && !empty($user->social_media))){
	    		// Check if Facebook Social Media Entry exists
	    		foreach($user->social_media as &$social_serivce)
	    			$this->logger->debug("Social Media Account Service ". $social_service->service . ' = ' . self::$facebook_service);
		    		if($social_serivce->service == self::$facebook_service){
		    			$user_has_social_media_account = true;
		    			$user->social_media_id = $social_serivce->id;
		    			$social_serivce->service_id = $fb_user_id;
		    			$social_serivce->service_email = $fb_user_email;
		    			$social_serivce->service_username = $fb_user_name;
		    		}
	    	}
	    	
	    	if(! $user_has_social_media_account){
	    		// Add User Facebook Social Media Entry
	    		$social_media = array(
	    			'user_id' => $user->id,
					'service' => self::$facebook_service,
					'service_id' => $fb_user_id,
					'service_email' => $fb_user_email,
					'service_username' => $fb_user_name
	    		);
	    		$user->social_media[] = (object) $social_media;
	    	}
	    	
	    	$user = $this->client_session->setUserAccount($user);
	    	
	    	$user_session->is_authenticated = true;
	    	$user_session->user_profile = $user;
	    	$user_session->updated_at = date("Y-m-d H:m:s");
	    	
	    	$response = $response->withHeader('Set-Cookie', $this->cookies->setDomainCookie($this->config['cookie.options']['cookie_name'], json_encode($user_session)));
	    	
	    	$this->client_session->persistUserSession((object)array(
	    			'guid' => $user_session->guid,
	    			'user_id' => $user->id
	    	));
	    	
	    	if( $this->client_session->isAdmin($user->id) )
	    		echo json_encode(array('status' => 'Successful', 'url' => '/admin/'. $user->id . '/home'));
	    	else
	    		echo json_encode(array('status' => 'Successful', 'url' => '/user/'. $user->id . '/home'));
	    	
    	} else $response = $response->withStatus(404)->write('Username or Password is Incorrect.');
    	    
    	return $response;
    }
    
    public function google_authenticate_user(Request $request, Response $response, $args)
    {
    	$this->logger->debug("Social Media Google Authenticate User Action");
    	
    	$json = $request->getBody();
    	
    	$payload = json_decode($json, true);
    	
    	$this->logger->debug("Payload [". json_encode($payload) ."]");
    	
    	$response = $response->withHeader('Content-Type', 'application/json');
    	
    	$user = $this->client_session->getUserAccount($payload['email']);
    	
    	$user_session = $this->client_session->get();
    	
    	$this->logger->debug("User Session [". json_encode($user_session) ."]");
    	
    	$user_has_account = false;
    	
    	if( $user ){
    		$this->logger->debug("Login User" . json_encode($user));
    		$user_has_account = true;
    		$user->first_name = $payload['first_name'];
    		$user->last_name = $payload['last_name'];
    		$user->user_login_with_social_media = self::$google_service;
    		if(! is_array($user->social_media)) $user->social_media = array();
    		if( $user->profile_picture == null || $user->profile_picture ==  $this->client_session->getDefaultProfilePicture() ){
    			$user->profile_picture = $payload['profile_picture'];
    		}
    	}
    	
    	if(! $user_has_account){
    		// Add User Account with FB User Profile
    		$user = array(
    				'email' => $payload['email'],
    				'profile_picture' => $payload['profile_picture'],
    				'first_name' => $payload['first_name'],
    				'last_name' => $payload['last_name'],
    				'status' => 'PENDING',
    				'user_login_with_social_media' => self::$google_service,
    				'social_media' => array()
    		);
    		$user = $this->client_session->setUserAccount($user);
    	}
    	
    	$user_has_social_media_account = false;
    	
    	if($user_has_account && (isset($user->social_media) && !empty($user->social_media))){
    		// Check if Facebook Social Media Entry exists
    		foreach($user->social_media as &$social_serivce)
    			$this->logger->debug("Social Media Account Service ". $social_service->service . ' = ' . self::$google_service);
    		if($social_serivce->service == self::$google_service){
    			$user_has_social_media_account = true;
    			$user->social_media_id = $social_serivce->id;
    			$social_serivce->service_id = $fb_user_id;
    			$social_serivce->service_email = $fb_user_email;
    			$social_serivce->service_username = $fb_user_name;
    		}
    	}
    	
    	if(! $user_has_social_media_account){
    		// Add User Facebook Social Media Entry
    		$social_media = array(
    				'user_id' => $user->id,
    				'service' => self::$google_service,
    				'service_id' =>  $payload['id'],
    				'service_email' => $payload['email'],
    				'service_username' =>  $payload['name']
    		);
    		$user->social_media[] = (object) $social_media;
    	}
    	
    	$user = $this->client_session->setUserAccount($user);
    	
    	$user_session->is_authenticated = true;
    	$user_session->user_profile = $user;
    	$user_session->updated_at = date("Y-m-d H:m:s");
    	
    	$response = $response->withHeader('Set-Cookie', $this->cookies->setDomainCookie($this->config['cookie.options']['cookie_name'], json_encode($user_session)));
    	
    	$this->client_session->persistUserSession((object)array(
    			'guid' => $user_session->guid,
    			'user_id' => $user->id
    	));
    	
    	if( $this->client_session->isAdmin($user->id) )
    		echo json_encode(array('status' => 'Successful', 'url' => '/admin/'. $user->id . '/home'));
    	else
    		echo json_encode(array('status' => 'Successful', 'url' => '/user/'. $user->id . '/home'));
    	
    	//$response = $response->withStatus(404)->write('Username or Password is Incorrect.');
    		
    	return $response;
   
    }
    
    public function twitter_login_url(Request $request, Response $response, $args)
    {
    	$this->logger->debug("Social Media Twitter Login Url Action");
    	
    	$user_session = $this->client_session->get();
    	 
    	$this->logger->debug("User Session [". json_encode($user_session) ."]");
    	
    	
    	$response = $response->withHeader('Content-Type', 'application/json');
    	 
    	$twitter_authentication = $this->twitter->getloginUrl();
    	
    	$this->logger->debug("Social Media Twitter User Profile [". json_encode($twitter_authentication) ."]");
    	 
    	 if($twitter_authentication->status == 'Successful'){
    	 	$user_session->user_profile = $twitter_authentication->user_profile;
    	 	$user_session->updated_at = date("Y-m-d H:m:s");
    	 	$response = $response->withHeader('Set-Cookie', $this->cookies->setDomainCookie($this->config['cookie.options']['cookie_name'], json_encode($user_session)));
    	 	echo json_encode($twitter_authentication);
    	 } else {
    	 	echo json_encode($this->twitter->getError());
    	 }
    	 
    	 return $response;
    }
    
    public function twitter_authenticate_user(Request $request, Response $response, $args)
    {
    	$this->logger->debug("Social Media Twitter Authenticate User Action");
    	
    	$user_session = $this->client_session->get();
    	
    	$this->logger->debug("User Session [". json_encode($user_session) ."]");
   
    	$user_profile = $this->twitter->getProfile($user_session->user_profile->oauth_token, $user_session->user_profile->oauth_token_secret);
    	 
    	$this->logger->debug("Twitter User Profile [". json_encode($user_profile) ."]");
    
    	if($user_profile->status == 'Successful'){
    		$response = $response->withHeader('Content-Type', 'application/json');
    		echo json_encode($user_profile);
    	} else {
    		$response = $response->withStatus(404)->withHeader('Location', '/user/login', true)->write('Username or Password is Incorrect.');
    		$this->logger->debug("Twitter User Profile Error Response [". json_encode($this->twitter->getError()) ."]");
    	}
    	
    	return $response;
    }
    
    public function linkedin_authenticate_user(Request $request, Response $response, $args)
    {
    
    }
}

