<?php
/**
 * SocialMediaBase Model
 * 
 * Provides data from "'social_media'"
 *
 * Author: Johnny Angel <johnnyangelnj@gmail.com>
 *
 */

namespace App\Models\Base;


use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class SocialMediaBase
 *
 * @package App\Models
 *
 * @SWG\Definition(
 *   definition="SocialMedia",
 *  required={"id", "user_id", "service", "service_id", "service_email", "service_username"}
 * )
 *
 */

class SocialMediaBase extends Model
{
	use SoftDeletes;
	
    // turn on auto timestamps create_at and updated_at
    public $timestamps = true;

    // turn on softe delete_at
    protected $softDelete = true;
    
    // used to hide columns 
    protected $hidden = array();
    
    // used to append columns
    protected $appends = array();
    
    protected $table = 'social_media';
    
    protected $dates = ['deleted_at'];
    


/**
 *
 * @SWG\Property(property="id", type="integer", format="int32")
 * @SWG\Property(property="user_id", type="integer", format="int32")
 * @SWG\Property(property="service", type="string", description="values ['Facebook', 'Google', 'Twitter', 'Linkedin', 'Paypal'].")
 * @SWG\Property(property="service_id", type="string")
 * @SWG\Property(property="service_email", type="string")
 * @SWG\Property(property="service_username", type="string")

 *
 */
     
     
    protected $fillable = array(
	'id',
	'user_id',
	'service',
	'service_id',
	'service_email',
	'service_username',
	'created_at',
	'updated_at',
	'deleted_at'
    );

    protected function getIdAttribute($v){ return (int)$v; }
    protected function getUserIdAttribute($v){ return (int)$v; }
    protected function getServiceIdAttribute($v){ return ($v)?$v:''; }
    protected function getServiceEmailAttribute($v){ return ($v)?$v:''; }
    protected function getServiceUsernameAttribute($v){ return ($v)?$v:''; }
    protected function getCreatedAtAttribute($v){ return $v; }
    protected function getUpdatedAtAttribute($v){ return $v; }
    protected function getDeletedAtAttribute($v){ return $v; }

    

}
