<?php
/**
 *  ShoppingCartItems Model
 * 
 * Provides data from "'shopping_cart_items'"
 *
 * Author: Johnny Angel <johnnyangelnj@gmail.com>
 * Generated on Sunday 17th of January 2016 08:26:54 PM
 *
 */
namespace App\Models;

use App\Models\Base\ShoppingCartItemsBase;
 
class ShoppingCartItems extends ShoppingCartItemsBase
{
    // used to hide columns
    protected $hidden = array();
	
    // used to append columns
    protected $appends = array();
    
    public function product() 
    {
    	return $this->hasOne('\App\Models\Product');
    }
    
    public function shopping_cart()
    {
    	return $this->belongsTo('\App\Models\ShoppingCart');
    }

}
