<?php 

namespace App\Service;

require_once('TwitterOauth/twitteroauth.php');

use \Pimple\Container;


class TwitterOauthProvider implements \Pimple\ServiceProviderInterface
{
    public function register(Container $pimple)
    {
		// Twitter Oauth 
		$pimple['twitteroauth'] = function ($c) {
					
			$container = (object) $c;
			$config = (array) $container->config;
			
			$container->logger->debug("Twitter Oauth Service Provider");
			
			$twitter = new \TwitterOAuth($config['twitter.options'][$config['env']]['customer_key'], $config['twitter.options'][$config['env']]['customer_secret']);

			$container->logger->debug("Twitter Oauth Service Provider Twitter Oauth Object[". json_encode($twitter) ."]");
			
			return $twitter;
		};
    }
}