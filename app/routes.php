<?php
// Routes

$container = $app->getContainer();

if(isset($container['config']['maintenance']) && $container['config']['maintenance'] == true){
	$app->get('/', 'App\Action\MaintenanceAction:dispatch')
	->setName('maintenance')->add($mw);
} else {
	$app->get('/', 'App\Action\LandingAction:dispatch')->setName('landing')->add($mw);
}
	
$app->group('/user', function () use($app) {
	$app->get('/login', function ($request, $response, $args) use($app) {
		$c = $app->getContainer();
		
		$meta_data = array(
				"title" => "Login",
				"user" =>  $c->get('client_session')->getUserProfile(),
				"attributes" => $request->getAttributes(),
				"show_title_bar" => false
		);
		
		$this->view->render($response, "login.twig", $meta_data);
		return $response;
	})->setName('user-login');
	
	$app->get('/facebook-login', 'App\Action\SocialMediaAction:facebook_authenticate_user')->setName('user-facebook-login');
	$app->get('/twitter-login-url', 'App\Action\SocialMediaAction:twitter_login_url')->setName('user-twitter-login-url');
	$app->get('/twitter-login', 'App\Action\SocialMediaAction:twitter_authenticate_user')->setName('user-twitter-login');
	$app->get('/{id:[0-9]+}/home', 'App\Action\HomeAction:dispatch')->setName('user-home');
	$app->get('/{id:[0-9]+}/my-account', 'App\Action\AccountAction:dispatch')->setName('user-account');
	$app->get('/{id:[0-9]+}/my-profile', 'App\Action\UserAction:get_profile')->setName('user-profile');
	$app->get('/search', 'App\Action\SearchAction:dispatch')->setName('user-search');
	$app->get('/shopping-cart', 'App\Action\ShoppingCartAction:dispatch')->setName('user-shopping-cart');	
	$app->get('/registration-confirmation', 'App\Action\UserAction:registration_confirmation')->setName('user-registration_confirmation');
	$app->get('/csrf-token', 'App\Action\AdminAction:csrf_token')->setName('csrf-token');
	$app->get('/{id:[0-9]+}/csrf-token', 'App\Action\UserAction:csrf_token')->setName('user-csrf-token');
	$app->get('/known-location', 'App\Action\UserAction:known_location')->setName('user-known-location');
	$app->get('/product/view/{product_guid:[a-zA-Z0-9 -]+}', 'App\Action\UserAction:product_view')->setName('user-product-view');
	$app->get('/product-bookmark/view/{product_key:[a-zA-Z0-9 -]+}', 'App\Action\UserAction:product_bookmark_view')->setName('user-product-bookmark-view');
	
	
	//$app->get('/reset-password/{email}', 'App\Action\UserAction:password_reset')->setName('user-password-reset');
	$app->map(['GET', 'PUT'], '/reset-password/{email}', 'App\Action\UserAction:password_reset')->setName('user-password-reset');
	
	$app->map(['POST', 'PUT'], '/{id:[0-9]+}/profile', 'App\Action\UserAction:post_profile')->setName('user-profile');
	
	$app->post('/authenticate', 'App\Action\UserAction:login')->setName('user-authenticate');
	$app->post('/google-login', 'App\Action\SocialMediaAction:google_authenticate_user')->setName('user-google-login');
	$app->post('/{id:[0-9]+}/profile-picture', 'App\Action\AccountAction:profile_picture')->setName('user-profile-picture');
	
	$app->map(['POST', 'DELETE'], '/{id:[0-9]+}/address', 'App\Action\AccountAction:address')->setName('user-address');
		
	$app->delete('/{id:[0-9]+}/logout', 'App\Action\UserAction:logout')->setName('user-logout');
})->add($mw);

$app->group('/api', function () use($app) {
	$app->get('/home', function ($request, $response, $args) use($app) {
		$c = $app->getContainer();
	
		$meta_data = array(
				"title" => "API",
				"user" =>  $c->get('client_session')->getUserProfile(),
				"attributes" => $request->getAttributes(),
				"show_title_bar" => false
		);
	
		$this->view->render($response, "api.twig", $meta_data);
		return $response;
	})->setName('api');
	$app->get('/swagger/json', 'App\Controller\ConsoleController:swagger')->setName('api-swagger-json');
	$app->get('/products', 'App\Action\ProductAction:products')->setName('products');
	$app->get('/product/{product_guid:[a-zA-Z0-9 -]+}', 'App\Action\ProductAction:product_details')->setName('product-details');		
	$app->group('/references', function () use($app) {
		$app->get('/address-types', 'App\Action\ReferencesAction:address_types')->setName('references-address-types');
		$app->get('/payment-methods', 'App\Action\ReferencesAction:payment_methods')->setName('references-payment-methods');
		$app->get('/shipping-methods', 'App\Action\ReferencesAction:shipping_methods')->setName('references-shipping-methods');
		$app->get('/states', 'App\Action\ReferencesAction:states')->setName('references-states');
		$app->get('/countries', 'App\Action\ReferencesAction:countries')->setName('references-countries');
		$app->get('/months', 'App\Action\ReferencesAction:months')->setName('references-months');
	});
	$app->get('/user/shopping-cart/{shopping_cart_guid:[a-zA-Z0-9 -]+}', 'App\Action\ShoppingCartAction:cart')->setName('references-shopping-cart');
	$app->get('/user/csrf_token', 'App\Action\AdminAction:csrf_token')->setName('user-csrf-token');
	$app->get('/user/sessions', 'App\Action\AdminAction:user_sessions')->setName('user-sessions');
	
	$app->put('/user/reset_password', 'App\Action\AdminAction:reset_user_password')->setName('user-reset-password');
	$app->put('/product', 'App\Action\ProductAction:product_status')->setName('product-status');
	
	$app->map(['POST', 'PUT'], '/user/profile', 'App\Action\UserAction:post_profile')->setName('user-profile');
	
	$app->post('/user/shopping-cart/item/{product_guid:[a-zA-Z0-9 -]+}', 'App\Action\ShoppingCartAction:item')->setName('references-shopping-cart-item');
	
	$app->post('/search/build-index', 'App\Action\SearchAction:build_index')->setName('build-search-index');
	
	$app->delete('/user/shopping-cart/{shopping_cart_guid:[a-zA-Z0-9 -]+}', 'App\Action\ShoppingCartAction:remove')->setName('references-shopping-cart-delete');
	$app->delete('/user/shopping-cart/{shopping_cart_guid:[a-zA-Z0-9 -]+}/item/{shopping_cart_item_id:[0-9]+}', 'App\Action\ShoppingCartAction:remove_item')->setName('references-shopping-cart-item-delete');
	
})->add($mw);

$app->group('/admin', function () use($app) {
	$app->get('/{id:[0-9]+}/home', 'App\Action\AdminAction:dispatch')->setName('admin-home');
	$app->get('/{id:[0-9]+}/product/initialize/{catalog_id:[0-9]+}', 'App\Action\ProductAction:initialize')->setName('initialize-product');
	$app->get('/{id:[0-9]+}/search/build-index', 'App\Action\SearchAction:build_index')->setName('build-search-index');
	
})->add($mw);




