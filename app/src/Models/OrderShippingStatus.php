<?php
/**
 *  OrderShippingStatus Model
 * 
 * Provides data from "'order_shipping_status'"
 *
 * Author: Johnny Angel <johnnyangelnj@gmail.com>
 * Generated on Saturday 5th of March 2016 01:40:33 AM
 *
 */
namespace App\Models;

use App\Models\Base\OrderShippingStatusBase;
 
class OrderShippingStatus extends OrderShippingStatusBase
{
    // used to hide columns
    protected $hidden = array();
	
    // used to append columns
    protected $appends = array();

}
