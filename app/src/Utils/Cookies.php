<?php
namespace App\Utils;

use Psr\Log\LoggerInterface;
use Psr\Http\Message\ServerRequestInterface as Request;

class Cookies extends \Slim\Http\Cookies {
	
	private $config;
	private $logger;

	public function __construct(LoggerInterface $logger, $config)
	{
		$this->config = $config;
		$this->logger = $logger;

		parent::__construct();
		
		$this->setDefaults(array(
			'cookies.encrypt' => $this->config['cookie.options']['cookie_encrypt'],
			'cookies.secure' => $this->config['cookie.options']['cookie_secure'],
			'cookies.httponly' => $this->config['cookie.options']['cookie_http_only'],
			'cookies.cipher' => MCRYPT_RIJNDAEL_256,
			'cookies.cipher_mode' => MCRYPT_MODE_CBC
		));

	}
	
	public function getConfig() 
	{
		return $this->config;
	}
	
	public function isDomainCookie($cookie_name)
	{
		$isCookie = false;
	
		if (isset($_COOKIE[$cookie_name]))
			$isCookie = true;
		
		$this->logger->debug("Does Client Cookie ". $cookie_name . " Exist? " . ($isCookie?'Yes':'No'));
	
		return $isCookie;
	}
	
	public function getDomainCookie($cookie_name)
	{

		$keys = new Keys($this->logger, $this->config);
		
		if(self::isDomainCookie($cookie_name)){
			$cookie =  $_COOKIE[$cookie_name];
			
			if($cookie != null && !empty($cookie))
				return $keys->decrypt($cookie);

		}
	
		return '{}';
	}
	
	public function setDomainCookie($cookie_name, $value)
	{
		$cookie = null;
		
		$keys = new Keys($this->logger, $this->config);
		
		$encoded_value = $keys->encrypt($value);
		
		$minutes = (60 * (int)$this->config['cookie.options']['cookie_time_in_minutes']);
		$path = $this->config['cookie.options']['cookie_path'];
		$domain = $this->config['cookie.options']['cookie_domain'];
		$secure = $this->config['cookie.options']['cookie_secure'];
		$httponly = $this->config['cookie.options']['cookie_http_only'];

		setcookie ($cookie_name, $encoded_value, $minutes, $path, $domain, $secure, $httponly);
		
		$cookie = $cookie_name . '=' . $encoded_value . '; path=' . $path . '; expires=' . $minutes . '; domain=' . $domain;

		$this->logger->debug("Client Cookie  [". $cookie . "]");
		
		return $cookie;
	}
}