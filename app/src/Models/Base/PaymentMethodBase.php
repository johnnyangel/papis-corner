<?php
/**
 * PaymentMethodBase Model
 * 
 * Provides data from "'payment_method'"
 *
 * Author: Johnny Angel <johnnyangelnj@gmail.com>
 *
 */

namespace App\Models\Base;


use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class PaymentMethodBase
 *
 * @package App\Models
 *
 * @SWG\Definition(
 *   definition="PaymentMethod",
 *  required={"id", "account_id", "type", "card_number", "expiration_date", "csv"}
 * )
 *
 */

class PaymentMethodBase extends Model
{
	use SoftDeletes;
	
    // turn on auto timestamps create_at and updated_at
    public $timestamps = true;

    // turn on softe delete_at
    protected $softDelete = true;
    
    // used to hide columns 
    protected $hidden = array();
    
    // used to append columns
    protected $appends = array();
    
    protected $table = 'payment_method';
    
    protected $dates = ['deleted_at'];
    


/**
 *
 * @SWG\Property(property="id", type="integer", format="int32")
 * @SWG\Property(property="account_id", type="integer", format="int32")
 * @SWG\Property(property="type", type="string", description="values ['PAYPAL', 'CREDIT CARD'].")
 * @SWG\Property(property="card_number", type="string")
 * @SWG\Property(property="expiration_date", type="string")
 * @SWG\Property(property="csv", type="string")

 *
 */
     
     
    protected $fillable = array(
	'id',
	'account_id',
	'type',
	'card_number',
	'expiration_date',
	'csv',
	'created_at',
	'updated_at',
	'deleted_at'
    );

    protected function getIdAttribute($v){ return (int)$v; }
    protected function getAccountIdAttribute($v){ return (int)$v; }
    protected function getCardNumberAttribute($v){ return ($v)?$v:''; }
    protected function getExpirationDateAttribute($v){ return ($v)?$v:''; }
    protected function getCsvAttribute($v){ return ($v)?$v:''; }
    protected function getCreatedAtAttribute($v){ return $v; }
    protected function getUpdatedAtAttribute($v){ return $v; }
    protected function getDeletedAtAttribute($v){ return $v; }

    

}
