<?php
namespace App\Action;

use Slim\Views\Twig;
use Psr\Log\LoggerInterface;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

final class AccountAction
{
    private $view;
    private $logger;
    private $config;
    private $client_session;    

    public function __construct(Twig $view, LoggerInterface $logger, $client_session)
    {
    	$this->view = $view;
        $this->logger = $logger;
        $this->config = $client_session->getConfig();
        $this->client_session = $client_session;
    }

    public function dispatch(Request $request, Response $response, $args)
    {
        $this->logger->debug("Account Dispatched Action");
        
        $user_session = $this->client_session->get();
          
        $meta_data = array(
        	
        	"title" => "Account",
        	"user" =>  $this->client_session->getUserProfile(),
        	"attributes" => $request->getAttributes(),
        	"show_logo" => false,
        	"is_authenticated" => isset($user_session->is_authenticated)?$user_session->is_authenticated:false,
        	"shopping_cart_item_count" => $this->client_session->getUserShoppingCartItemCount(),
        	"shopping_cart" => $this->client_session->getUserShoppingCart()
        );
        
        $this->logger->debug(json_encode($meta_data));
                
        $this->view->render($response, "account.twig", $meta_data);
        return $response;
    }
    
    public function profile_picture(Request $request, Response $response, $args)
    {
    	$this->logger->debug("Account Profile Picture Action");
    	
    	$this->logger->debug("Args [". json_encode($args) ."]");
    	$this->logger->debug("\$_FILES [". json_encode($_FILES) ."]");
    	
    	$response = $response->withHeader('Content-Type', 'application/json');
    	
    	if (!empty($_FILES)) {
    		 
    		$tempFile = $_FILES['file']['tmp_name']; 
    		
    		$this->logger->debug("Temp File [". $tempFile ."]");
    	
    		$targetPath =  $this->config['base_directory'] . $this->config['user_upload_directory'] . DIRECTORY_SEPARATOR . $args['id'] . DIRECTORY_SEPARATOR;
    		 
    		$targetFile =  $targetPath. $_FILES['file']['name'];
    		
    		$this->logger->debug("Target File [". $targetFile ."]");
    	
    		move_uploaded_file($tempFile,$targetFile);
    		 
    	}
    	 
    	echo json_encode(array('status' => 'Successful'));
    	 
    	return $response;
    }
    
    public function address(Request $request, Response $response, $args)
    {
    	$this->logger->debug("Account Address Action");
    	 
    	$this->logger->debug("Method [" . $request->getMethod() . "] Args [". json_encode($args) ."]");
    	
    	$response = $response->withHeader('Content-Type', 'application/json');
    	
    	echo json_encode(array('status' => 'Successful'));
    	
    	return $response;
    }
    
}
