<?php
/**
 * ProductImageBase Model
 * 
 * Provides data from "'product_image'"
 *
 * Author: Johnny Angel <johnnyangelnj@gmail.com>
 *
 */

namespace App\Models\Base;




/**
 * Class ProductImageBase
 *
 * @package App\Models
 *
 * @SWG\Definition(
 *   definition="ProductImage",
 *  required={"id", "product_id", "product_guid", "type", "filename", "backdrop_style"}
 * )
 *
 */

class ProductImageBase extends Model
{
	
	
    // turn on auto timestamps create_at and updated_at
    public $timestamps = false;

    // turn on softe delete_at
    protected $softDelete = false;
    
    // used to hide columns 
    protected $hidden = array();
    
    // used to append columns
    protected $appends = array();
    
    protected $table = 'product_image';
    
    
    


/**
 *
 * @SWG\Property(property="id", type="integer", format="int32")
 * @SWG\Property(property="product_id", type="integer", format="int32")
 * @SWG\Property(property="product_guid", type="string")
 * @SWG\Property(property="type", type="string", description="values ['LARGE', 'MEDIUM', 'SMALL', 'TINY'].")
 * @SWG\Property(property="filename", type="string")
 * @SWG\Property(property="backdrop_style", type="string")

 *
 */
     
     
    protected $fillable = array(
	'id',
	'product_id',
	'product_guid',
	'type',
	'filename',
	'backdrop_style'
    );

    protected function getIdAttribute($v){ return (int)$v; }
    protected function getProductIdAttribute($v){ return (int)$v; }
    protected function getProductGuidAttribute($v){ return self::unpackGUID($v); }
    protected function getFilenameAttribute($v){ return ($v)?$v:''; }
    protected function getBackdropStyleAttribute($v){ return ($v)?$v:''; }

    protected function setProductGuidAttribute($v){ $this->attributes['product_guid'] = self::packGUID($v); }

}
