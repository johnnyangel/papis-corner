<?php
namespace App\Utils;

use Psr\Log\LoggerInterface;
use Psr\Http\Message\ServerRequestInterface as Request;

class Twig extends \Twig_Extension {
	
    private $logger;
    private $config;
    private $csrf;
    private $cookies;
    private $client_session;
    private $product_images;
    
	public function __construct(LoggerInterface $logger, $client_session, $product_images)
	{
		$this->logger = $logger;
		$this->config = $client_session->getConfig();
		$this->csrf   = $client_session->getCsrf();
		$this->cookies = $client_session->getCookies();
		$this->client_session = $client_session;
		$this->product_images = $product_images;
		
	}
	
	public function getGlobals()
	{
		// CSRF token name and value
		$csrf_key_name = $this->csrf->getTokenNameKey();
		$csrf_value_name = $this->csrf->getTokenValueKey();
		
		$kiosk_product_images = $this->product_images->get_kiosk_product_images();
		$featured_product_images = $this->product_images->get_featured_product_images();
		
		$social_media = [];
		$social_media['facebook'] = $this->config['facebook.options'][$this->config['env']];
		$social_media['facebook']['login_url'] = '#';
		$social_media['google'] = $this->config['google.options'];
		$social_media['google']['login_url'] = '#';
		$social_media['twitter'] = $this->config['twitter.options'][$this->config['env']];
		$social_media['twitter']['login_url'] = '#';
		
		return array(
			"developer" => 'Johnny Angel',
			"app_domain" => $this->config['domain'],
			"profile_picture" => array(
				"default_male_profile_picture" => "man-profile-picture.png",
				"default_female_profile_picture" => "woman-profile-picture.png",
				"upload_directory" => $this->config['user_upload_directory']
			),
			"site_name" => "Papi's Corner",
			"social_media" => $social_media,
			"csrf_key_name" => $csrf_key_name,
			"csrf_value_name" => $csrf_value_name,
			"kiosk_product_images" => $kiosk_product_images,
			"featured_product_images" => $featured_product_images,
		);
	}
	
	public function getName()
	{
		return 'twig.extension';
	}
}