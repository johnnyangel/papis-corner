<?php
/**
 *  SearchTagsHasProduct Model
 * 
 * Provides data from "'search_tags_has_product'"
 *
 * Author: Johnny Angel <johnnyangelnj@gmail.com>
 * Generated on Monday 2nd of May 2016 01:58:43 PM
 *
 */
namespace App\Models;

use App\Models\Base\SearchTagsHasProductBase;
 
class SearchTagsHasProduct extends SearchTagsHasProductBase
{
    // used to hide columns
    protected $hidden = array();
	
    // used to append columns
    protected $appends = array();

}
