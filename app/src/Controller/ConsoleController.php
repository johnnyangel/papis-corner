<?php
/**
 * Console Controller
 * 
 * Handles controller actions for "Console" resource. Debugs 
 * server status, deploys a browser-based client, and describes
 * API services available for various clients. 
 */


namespace App\Controller;

use Slim\Views\Twig;
use Psr\Log\LoggerInterface;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

use Swagger\Swagger;
use Loco\Utils\Swizzle\Swizzle;

/**
 * @SWG\Swagger(
 * 		basePath="/api",
 * 		host="dev.papiscorner.com",
 * 		schemes={"http"},
 * 		produces={"application/json"},
 * 		consumes={"application/json"},
 * 		@SWG\Info(
 * 			title="Papi's Corner API Console",
 * 			description="REST Web Services API",
 * 			version="1.0",
 * 			termsOfService="terms",
 * 			@SWG\Contact(name="johnnyangelnj@gmail.com"),
 * 			@SWG\License(name="proprietary")
 * 		),
 *  	@SWG\Definition(
 *         definition="Error",
 *         required={"code", "message"},
 *         @SWG\Property(
 *             property="code",
 *             type="integer",
 *             format="int32"
 *         ),
 *         @SWG\Property(
 *             property="message",
 *             type="string"
 *         )
 *     )
 * )
 */

/**
 *
 * @package App\Controller
 *
 * @SWG\Definition(
 *   definition="Swagger"
 * )
 *
 */
class ConsoleController
{
	private $view;
	private $logger;
	
	public function __construct(Twig $view, LoggerInterface $logger)
	{
		$this->view = $view;
		$this->logger = $logger;
	}
    
	/**
	 * @SWG\Get(
	 *   path="/swagger/json",
	 *   summary="Swagger Console Configuration.",
	 *   operationId="getSwaggerJson",
	 *   produces={"application/json"},
	 *   tags={"Console"},
	 *   @SWG\Response(
	 *   	response=200,
	 *   	description="Papi's Corner Services API Swagger Configuration."
	 *   )
	 * )
	 */	
	public function swagger(Request $request, Response $response, $args){
		
		$response = $response->withHeader('Content-Type', 'application/json');

		$swagger = \Swagger\scan($_SERVER['DOCUMENT_ROOT'] . '/../app');
		
		echo $swagger;
		
		return $response;
		
	}
}
