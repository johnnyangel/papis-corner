<?php 

namespace App\Service;

use \Pimple\Container;


class SmtpProvider implements \Pimple\ServiceProviderInterface
{
    public function register(Container $pimple)
    {
		// Zend Mailer Transport
		$pimple['transport'] = function ($c) {
		
			$config = $c['config'];
			
			$transport = new \Zend\Mail\Transport\Smtp();
			$options = new \Zend\Mail\Transport\SmtpOptions( $config['smtp.options'] );
			
			$transport->setOptions($options);
			
			return $transport;	
		};
    }
}