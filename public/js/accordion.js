/** 
 * Global Accordion Variables
 */
var accordion_count = 0;
var product_details_array = [];

function get_accordion_count()
{
	return accordion_count;
}

function set_accordion_count(count)
{
	accordion_count = count;
}

function reset_accordion(accordion_id)
{
	$( '#accordion_' + accordion_id ).slideUp('fast');
}

function reset_accordions(not_eq_accordion_id)
{
	for (var counter = 0; counter < accordion_count; counter++) {
		if(not_eq_accordion_id != (counter + 1))
			$( '#accordion_' + (counter + 1) ).hide();
	}
}

function get_product_details(product_page, column_count, accordion_id, product)
{
	product = JSON.parse(product);
		
	console.log("On Accordion " + accordion_id + " display product details for " + product.guid + '. Point to top column ' + column_count);
	
	var previously_viewed_product = $("input[name=current_product]").val();
	var previously_on_accordion = $("input[name=current_accordion]").val();
	
	console.log('Previous Guid '+previously_viewed_product+' on Accordion ['+previously_on_accordion+'] Current Guid '+product.guid+' on Accordion ['+accordion_id+']');
	
	var leave_open = false;
	if(previously_on_accordion == accordion_id)
		if(previously_viewed_product != product.guid)
			leave_open = true;
	
	$( '.product-share' ).html('<a id="'+product.key+'" href="#" class="share-button"><i class="fi-share top-bar-icon-style"></i></a>');
	
	$( '.product_details_lg_image' ).attr('href', '/user/product/view/'+product.guid);
	
	$( '#product_modal' ).removeClass().addClass('product-model reveal-modal large-10 small-10 medium-10 columns '+product.product_images[0].backdrop_style);
	$( '#product_modal_view' ).removeClass().addClass('product-model-view effect5 '+product.product_images[0].backdrop_style);
	
	$( '.product-details-image-canvas' ).html(
			'<a id="'+product.guid+'" data-tooltip aria-haspopup="true" title="Click to Enlarge" class="product_details_lg_image has-tip tip-left product_details_image" href="/user/product-bookmark/view/'+product.key+'" title=""> '+
			'<img class="'+product.product_images[0].backdrop_style+'" style="display:inline" name="product_details_image" src="'+product.path+'/'+product.product_images[1].filename+'" alt="'+product.name+'" />' +
			'</a>' +
			'<fieldset>' +
		    '<legend class="product-details-legend-name">'+product.name+'</legend>' +
		    '<b class="product-details-product-name" style="font-size:1.25rem;line-height:1.50;">'+product.name+'</b><br/>'+
		    '<p class="product-details-product-caption">'+product.caption+'<p/>'+
		    '<i class="product-details-product-provided_by" style="margin-top:0rem;margin-left:1rem;font-size:0.75rem;">By The '+product.provided_by+' Collection</i><br/><br/>'+
		    '<p class="product-details-product-description">'+product.description+'</p>'+
		    '<span class="product-details-product-status">Status: '+product.status+'</span><br/>'+
		    //'<span class="product-details-product-price">Price: '+product.price+'</span><br/><br/>'+
		    '<p class="product-details-product-number" style="margin-left:0.25rem;margin-bottom:0rem;font-size:0.70rem;">product #: '+product.catalog_id+' : '+product.id+' : '+product.guid+'</p>'+
		  	//'<div class="product-details-product-buy-now-button"><a id="'+product.guid+'" href="#" class="button small effect1-black right buy-now">Buy</a></div>'+
		  	'<input type="hidden" name="current_product" value="'+product.guid+'">'+
		  	'<input type="hidden" name="current_accordion" value="'+accordion_id+'">'+
		  	'</fieldset>'
	);
	 	
	$(document).foundation('tooltip');
	
	if( accordion_id > 1)
		column_count = column_count - 3;
	
	shift_pointer_on_accordion(product_page, accordion_id, column_count);
	
	toggle_product_details(accordion_id, leave_open);
 		
	return false;
}

function toggle_product_details(accordion_id, leave_open){
	console.log('Leave Accordion ID '+ accordion_id+' '+(leave_open?'Open':'Closed'));
 	if($( '#accordion_' + accordion_id ).is(':hidden')){
 		reset_accordions(accordion_id);
 		$( '#accordion_' + accordion_id ).slideDown('fast');
 		jQuery('html,body').animate({ scrollTop: $( '#accordion_' + accordion_id ).offset().top}, 500);
 	} else {
 		if(! leave_open){
 	 		$( '#accordion_' + accordion_id ).slideUp('fast');
 	 		jQuery('html,body').animate({ scrollTop: $( '#product_accordion_' + accordion_id ).offset().top}, 500); 			
 		} else {
 			jQuery('html,body').animate({ scrollTop: $( '#accordion_' + accordion_id ).offset().top}, 750);
 		}
 	}
}

function shift_pointer_on_accordion(product_page, accordion_id, column_count) {
	console.log('Move Arrow to ' + accordion_id + 'X' + column_count);

	var id = 'accordion_'+ accordion_id +'_arrow_up';
	console.log('Accordion ID '+ id);
	
	if(product_page == 'search'){
		if ( column_count == 3 )  document.getElementById(id).style.left = '45rem';
		else if ( column_count == 2 ) document.getElementById(id).style.left = '27rem';
		else document.getElementById(id).style.left = '8rem';
		

		if ( column_count == 3 ) $( '#accordion_'+ accordion_id +'_arrow_up' ).css({'left':'45rem !important'});
		else if ( column_count == 2 ) $( '#accordion_'+ accordion_id +'_arrow_up' ).css({'left':'27rem !important'});
		else $( '#accordion_'+ accordion_id +'_arrow_up' ).css({'left':'8rem'});		
	} else {
		if ( column_count == 3 )  document.getElementById(id).style.left = '51rem';
		else if ( column_count == 2 ) document.getElementById(id).style.left = '30rem';
		else document.getElementById(id).style.left = '8rem';
		

		if ( column_count == 3 ) $( '#accordion_'+ accordion_id +'_arrow_up' ).css({'left':'51rem !important'});
		else if ( column_count == 2 ) $( '#accordion_'+ accordion_id +'_arrow_up' ).css({'left':'30rem !important'});
		else $( '#accordion_'+ accordion_id +'_arrow_up' ).css({'left':'8rem'});		
	}
}


