<?php
/**
 * OrderItemsBase Model
 * 
 * Provides data from "'order_items'"
 *
 * Author: Johnny Angel <johnnyangelnj@gmail.com>
 *
 */

namespace App\Models\Base;


use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class OrderItemsBase
 *
 * @package App\Models
 *
 * @SWG\Definition(
 *   definition="OrderItems",
 *  required={"id", "product_id", "product_guid", "order_id", "order_guid", "quantity", "price"}
 * )
 *
 */

class OrderItemsBase extends Model
{
	use SoftDeletes;
	
    // turn on auto timestamps create_at and updated_at
    public $timestamps = true;

    // turn on softe delete_at
    protected $softDelete = true;
    
    // used to hide columns 
    protected $hidden = array();
    
    // used to append columns
    protected $appends = array();
    
    protected $table = 'order_items';
    
    protected $dates = ['deleted_at'];
    


/**
 *
 * @SWG\Property(property="id", type="integer", format="int32")
 * @SWG\Property(property="product_id", type="integer", format="int32")
 * @SWG\Property(property="product_guid", type="string")
 * @SWG\Property(property="order_id", type="integer", format="int32")
 * @SWG\Property(property="order_guid", type="string")
 * @SWG\Property(property="quantity", type="integer", format="int32")
 * @SWG\Property(property="price", type="string", description="Two decimal value, 0.00")

 *
 */
     
     
    protected $fillable = array(
	'id',
	'product_id',
	'product_guid',
	'order_id',
	'order_guid',
	'quantity',
	'price',
	'created_at',
	'updated_at',
	'deleted_at'
    );

    protected function getIdAttribute($v){ return (int)$v; }
    protected function getProductIdAttribute($v){ return (int)$v; }
    protected function getProductGuidAttribute($v){ return self::unpackGUID($v); }
    protected function getOrderIdAttribute($v){ return (int)$v; }
    protected function getOrderGuidAttribute($v){ return self::unpackGUID($v); }
    protected function getQuantityAttribute($v){ return (int)$v; }
    protected function getCreatedAtAttribute($v){ return $v; }
    protected function getUpdatedAtAttribute($v){ return $v; }
    protected function getDeletedAtAttribute($v){ return $v; }

    protected function setProductGuidAttribute($v){ $this->attributes['product_guid'] = self::packGUID($v); }
    protected function setOrderGuidAttribute($v){ $this->attributes['order_guid'] = self::packGUID($v); }

}
