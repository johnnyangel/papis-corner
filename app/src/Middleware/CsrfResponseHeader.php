<?php

namespace App\Middleware;

use \Slim\Csrf\Guard;
use Psr\Log\LoggerInterface;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

class CsrfResponseHeader
{
	private $logger;
	private $csrf;
	private $cookies;
	
	public function __construct(LoggerInterface $logger, $cookies)
	{
		$this->logger = $logger;
		$this->csrf = $cookies->getCsrf();
		$this->cookies = $cookies;
	}

	/**
	 * Example middleware invokable class
	 *
	 * @param  \Psr\Http\Message\ServerRequestInterface $request  PSR7 request
	 * @param  \Psr\Http\Message\ResponseInterface      $response PSR7 response
	 * @param  callable                                 $next     Next middleware
	 *
	 * @return \Psr\Http\Message\ResponseInterface
	 */
	public function __invoke($request, $response, $next)
	{
		// Generate new token and update request
		$request = $this->csrf->generateNewToken($request);

		// Build Header Token
		$nameKey = $this->csrf->getTokenNameKey();
		$valueKey = $this->csrf->getTokenValueKey();
		$name = $request->getAttribute($nameKey);
		$value = $request->getAttribute($valueKey);
		$tokenArray = [
				$nameKey => $name,
				$valueKey => $value
		];

		// Update response with added token header
		$response = $response->withAddedHeader('X-CSRF-Token', json_encode($tokenArray));
		
		$headers = $request->getHeaders();
		foreach ($headers as $name => $values) {
			$this->logger->debug("Request Header: Name [" . $name . "] Value [" .  json_encode($values) . "]");
		}
		
		$headers = $response->getHeaders();
		foreach ($headers as $name => $values) {
			$this->logger->debug("Response Header: Name [" . $name . "] Value [" .  json_encode($values) . "]");
		}
		
		return $next($request, $response);
	}
}