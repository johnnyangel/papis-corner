<?php
namespace App\Utils;

use Psr\Log\LoggerInterface;

class ProductImages {
	
	public $logger;
	
	public function __construct(LoggerInterface $logger)
	{
		$this->logger = $logger;
	}
	
	public function get_large_product_images(){
		return \App\Models\Product::with(['product_images' =>
				function($query) {
					$query->where('type', '=' ,'LARGE');
				}])->where('status', '=', 'AVAILABLE')->get();
	}
	
    public function get_kiosk_product_images(){
    	return \App\Models\Product::with(['product_images' =>
    			function($query) {
    				$query->where('type', '=' ,'MEDIUM');
    			}])->where('status', '=', 'AVAILABLE')->get();
    }
    
    public function get_featured_product_images(){
    	return \App\Models\Product::with(['product_images' =>
    			function($query) {
    				$query->where('type', '=' ,'SMALL')->orWhere('type', '=' ,'LARGE')->orWhere('type', '=' ,'TINY');
    			}])->where('status', '=', 'AVAILABLE')->get();
    }
    
    public function get_small_product_images(){
    	return \App\Models\Product::with(['product_images' =>
    			function($query) {
    				$query->where('type', '=' ,'TINY');
    			}])->where('status', '=', 'AVAILABLE')->get();
    }
}