
function facebook_signup(response) {
	  
	console.log(response);
	
	if (response.status === 'connected') {
		console.log('Welcome Back!  Fetching your information.... ');
		FB.api('/me?fields=id,email,name,first_name,last_name,picture', function(response) {
		  console.log('Successful login for: (id:' + response.id + ') ' + response.name);
		  console.log('Facebook Response Object: ' + JSON.stringify(response, null, 2)); 
		  console.log('Facebook Response Granted Scopes: (' + response.grantedScopes  + ')');
		  $.get( "/user/facebook-login?id="+response.id+"&name="+response.name, function( response ) {
			console.log( response );
			if(response.status = 'Successful'){
				// The person is not logged into Facebook, so we're not sure if
				// they are logged into this app or not.
				$( '#login-message-container' ).show();
				$( '#login-message' ).addClass('success label message-box').html('You are login using Facebook.');
				$( '#registration-message-container' ).show();
				$( '#registration-message' ).addClass('success label message-box').html('You are login using Facebook.');
				location.href = response.url;				
			} else {
				// The person is logged into Facebook, but not your app.
				$( '#login-message-container' ).show();
				$( '#login-message' ).addClass('warning label message-box').html("Please Sign up for a Papi's Corner Account.");
				$( '#registration-message-container' ).show();
				$( '#registration-message' ).addClass('warning  label message-box').html("Please Sign up for a Papi's Corner Account.");

			}
		  });
		  facebook_getUserProfilePicture(response.id);
	   });
	} else if (response.status === 'not_authorized') {
		// The person is logged into Facebook, but not your app.
		$( '#login-message-container' ).show();
		$( '#login-message' ).addClass('warning label message-box').html("Please Sign up for a Papi's Corner Account.");
		$( '#registration-message-container' ).show();
		$( '#registration-message' ).addClass('warning  label message-box').html("Please Sign up for a Papi's Corner Account.");
	}
}

function facebook_getUserProfilePicture(id){
	
	FB.api("/"+id+"/picture", function (response) {
    	if (response && !response.error) {
    		/* handle the result */
    		console.log(response);
    	}
	});
}

function facebook_logout(){
	FB.logout(function() { console.log('Logout Facebook!'); });
}

function facebook_checkLoginState() {
	FB.getLoginStatus(function(response) {
		facebook_signup(response);
	});
}
  
function facebook_login () {
	console.log('Facebook Login')
	FB.login(function(response) {
		facebook_signup(response);
	}, {scope: 'email,user_likes'});
	/*
	, {
	    scope: 'publish_actions', 
	    return_scopes: true
	});
	*/
}
  
function facebook_logout () {
	console.log('Facebook Logout')
}
  

window.fbAsyncInit = function() {
	FB.init({
		//appId      : '194606940895616',  www.papiscorner.com
		//appId      : '194635340892776', // dev.papiscorner.com
		appId	   : $( '#facebook_app_id' ).val(),
		cookie     : true,  // enable cookies to allow the server to access to the session
		xfbml      : true,  // parse social plugins on this page
		version    : 'v2.2' // use version 2.2
	});

	/*
	FB.getLoginStatus(function(response) {
		facebook_signup(response);
	});
	*/
	
	/* Facebook Popups */
	/*
	FB.ui({
		method: 'share',
		href: 'http://www.papisconer.com/',
	}, function(response){});
	
	FB.ui({
		method: 'feed',
		link: 'http://www.papisconer.com/',
		caption: 'An example caption',
	}, function(response){});
	FB.ui({
		method: 'send',
		link: 'http://www.papisconer.com/',
	});
	*/
};

// Load the SDK asynchronously
(function(d, s, id) {
	var js, fjs = d.getElementsByTagName(s)[0];
	if (d.getElementById(id)) return;
	js = d.createElement(s); js.id = id;
	js.src = "//connect.facebook.net/en_US/sdk.js";
	fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));
