<?php
/**
 * AddressBase Model
 * 
 * Provides data from "'address'"
 *
 * Author: Johnny Angel <johnnyangelnj@gmail.com>
 *
 */

namespace App\Models\Base;


use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class AddressBase
 *
 * @package App\Models
 *
 * @SWG\Definition(
 *   definition="Address",
 *  required={"id", "account_id", "type", "recipient", "address_1", "address_2", "city", "state", "postal_code", "territory", "country", "lat", "long"}
 * )
 *
 */

class AddressBase extends Model
{
	use SoftDeletes;
	
    // turn on auto timestamps create_at and updated_at
    public $timestamps = true;

    // turn on softe delete_at
    protected $softDelete = true;
    
    // used to hide columns 
    protected $hidden = array();
    
    // used to append columns
    protected $appends = array();
    
    protected $table = 'address';
    
    protected $dates = ['deleted_at'];
    


/**
 *
 * @SWG\Property(property="id", type="integer", format="int32")
 * @SWG\Property(property="account_id", type="integer", format="int32")
 * @SWG\Property(property="type", type="string", description="values ['Billing', 'Shipping'].")
 * @SWG\Property(property="recipient", type="string")
 * @SWG\Property(property="address_1", type="string")
 * @SWG\Property(property="address_2", type="string")
 * @SWG\Property(property="city", type="string")
 * @SWG\Property(property="state", type="string")
 * @SWG\Property(property="postal_code", type="string")
 * @SWG\Property(property="territory", type="string")
 * @SWG\Property(property="country", type="string")
 * @SWG\Property(property="lat", type="string", description="Two decimal value, 0.00")
 * @SWG\Property(property="long", type="string", description="Two decimal value, 0.00")

 *
 */
     
     
    protected $fillable = array(
	'id',
	'account_id',
	'type',
	'recipient',
	'address_1',
	'address_2',
	'city',
	'state',
	'postal_code',
	'territory',
	'country',
	'lat',
	'long',
	'created_at',
	'updated_at',
	'deleted_at'
    );

    protected function getIdAttribute($v){ return (int)$v; }
    protected function getAccountIdAttribute($v){ return (int)$v; }
    protected function getRecipientAttribute($v){ return ($v)?$v:''; }
    protected function getAddress1Attribute($v){ return ($v)?$v:''; }
    protected function getAddress2Attribute($v){ return ($v)?$v:''; }
    protected function getCityAttribute($v){ return ($v)?$v:''; }
    protected function getStateAttribute($v){ return ($v)?$v:''; }
    protected function getPostalCodeAttribute($v){ return ($v)?$v:''; }
    protected function getTerritoryAttribute($v){ return ($v)?$v:''; }
    protected function getCountryAttribute($v){ return ($v)?$v:''; }
    protected function getCreatedAtAttribute($v){ return $v; }
    protected function getUpdatedAtAttribute($v){ return $v; }
    protected function getDeletedAtAttribute($v){ return $v; }

    

}
