<?php
return [
    'settings' => [
    	'displayErrorDetails' => true,
        // View settings
        'view' => [
            'template_path' => __DIR__ . '/templates',
            'twig' => [
                'cache' => __DIR__ . '/../cache/twig',
                'debug' => true,
                'auto_reload' => true,
            ],
        ],

        // monolog settings
        'logger' => [
            'name' => 'app',
            'path' => __DIR__ . '/../log/app.log',
        	//'log_signal' => \Monolog\Logger::INFO
        	'log_signal' => \Monolog\Logger::DEBUG
        ],
    ],
];
