<?php
/**
 * ShoppingCartBase Model
 * 
 * Provides data from "'shopping_cart'"
 *
 * Author: Johnny Angel <johnnyangelnj@gmail.com>
 *
 */

namespace App\Models\Base;


use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class ShoppingCartBase
 *
 * @package App\Models
 *
 * @SWG\Definition(
 *   definition="ShoppingCart",
 *  required={"id", "guid", "session_id"}
 * )
 *
 */

class ShoppingCartBase extends Model
{
	use SoftDeletes;
	
    // turn on auto timestamps create_at and updated_at
    public $timestamps = true;

    // turn on softe delete_at
    protected $softDelete = true;
    
    // used to hide columns 
    protected $hidden = array();
    
    // used to append columns
    protected $appends = array();
    
    protected $table = 'shopping_cart';
    
    protected $dates = ['deleted_at'];
    


/**
 *
 * @SWG\Property(property="id", type="integer", format="int32")
 * @SWG\Property(property="guid", type="string")
 * @SWG\Property(property="session_id", type="integer", format="int32")

 *
 */
     
     
    protected $fillable = array(
	'id',
	'guid',
	'session_id',
	'created_at',
	'updated_at',
	'deleted_at'
    );

    protected function getIdAttribute($v){ return (int)$v; }
    protected function getGuidAttribute($v){ return self::unpackGUID($v); }
    protected function getSessionIdAttribute($v){ return (int)$v; }
    protected function getCreatedAtAttribute($v){ return $v; }
    protected function getUpdatedAtAttribute($v){ return $v; }
    protected function getDeletedAtAttribute($v){ return $v; }

    protected function setGuidAttribute($v){ $this->attributes['guid'] = self::packGUID($v); }

}
